# QA-Services

Project based on ReactJS + Redux + NodeJS technologies.

## Project docs links

* [Environment setup](docs/ENVIRONMENT.md) short instruction for project setup.
* [CoreUI](docs/COREUI.md) Basic Admin UI Kit docs.
* [Lodash](https://habr.com/ru/post/217515/) Very cool explaining of the lodash library.
* [Branching policy](docs/REACT.md) *TODO*
* [Mail box](https://webmail.qa-services.gq) corporate email client

## API docs links

* [Issues & Comments](https://comments.qa-services.gq/swagger)
* [Projects](https://projects.qa-services.gq/swagger/)
* [Mails](https://messenger.qa-services.gq/swagger#/)
* [Auth & Users](https://auth.qa-services.gq/swagger)