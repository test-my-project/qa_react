function replaceHtmlLink (body, host, protocol = 'http') {
  const regexp = new RegExp(
    `(href|src|link)(=['"])(http://${host}|https://${host}|ftp://${host}|/[^/])([^'"]+)(['"])`,
    'gim');

  return body.replace(regexp, function (match, tag, quotes, http, url, quotes2) {
    if (http.startsWith('/')) {
      http = `${protocol}//${host}${http}`
    }
    const replacedUrl = `/proxy?route=${encodeURIComponent(http + url)}`
    return tag + quotes + replacedUrl + quotes2
  });
}

function replaceHtmlForm (body, host, protocol) {
  return body.replace(/(<form.*>)/gim, function (form, tag) {
    const match = tag.match(/method=['"](post|get|POST|get)['"]/);
    if (!match) {
      return tag;
    }
    const [, method = ''] = match;
    const regexp = new RegExp(
      `(action)(=['"])(http://${host}|https://${host}|ftp://${host}|/[^/])([^'"]+)(['"])`, 'gim');
    if (method.toLowerCase() === 'post') {
      tag = tag.replace(regexp, function (match, tag, quotes, http, url, quotes2) {
        if (http.startsWith('/')) {
          http = `${protocol}//${host}${http}`;
        }
        const replacedUrl = `/proxy?route=${encodeURIComponent(http + url)}`;
        return tag + quotes + replacedUrl + quotes2;
      });
    }
    if (method.toLowerCase() === 'get') {
      let replacedUrl;
      tag = tag.replace(regexp, function (match, tag, quotes, http, url, quotes2) {
        if (http.startsWith('/')) {
          http = `${protocol}//${host}${http}`;
        }
        replacedUrl = http + url;
        return tag + quotes + '/proxy' + quotes2;
      });
      tag += `<input type="hidden" name="route" value="${replacedUrl}">`;
    }
    return tag;
  });
}

module.exports = {
  replaceHtmlLink,
  replaceHtmlForm
};