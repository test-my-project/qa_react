const http = require('http');
const express = require('express');
const session = require('express-session');
const sessionFileStore = require('session-file-store');
const cors = require('cors');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const url = require('url');
const querystring = require('querystring');
const config = require('config');

const {
  authController,
  proxyController,
  servicesController,
  inviteToProjectMiddleware
} = require('./controllers');
const FileStore = sessionFileStore(session);

const app = express();
app.use(cors());
app.use(session({
  store: new FileStore,
  secret: 'keyboard cat',
  sameSite: true,
  resave: false,
  proxy: true,
  saveUninitialized: false
}));
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
if (process.env.NODE_ENV && !process.env.NODE_ENV.includes('dev') && process.env.NODE_ENV !== 'localhost') {
  app.use(logger('dev'));
}
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

let proxyRouter = express.Router();
app.use('/proxy', proxyRouter);
new proxyController(proxyRouter);

let authRouter = express.Router();
app.use('/publicMyTestingProject/api/v1/auth', authRouter);
new authController(authRouter);

app.get('/publicMyTestingProject/api/v1/projects/invite/:token', inviteToProjectMiddleware);
app.use('/publicMyTestingProject/api/v1', new servicesController());

app.use(express.static(path.join(__dirname, '..', 'publicMyTestingProject')));

app.use(function (req, res, next) {
  let approvedUrls = [
    'publicMyTestingProject',
    'proxy',
  ];
  // console.log('approvedUrls == ',approvedUrls.includes(req.path.split('/')[1]));
  if (!approvedUrls.includes(req.path.split('/')[1])) {
    let r;
    // console.log(req.session);
    if (req.headers.referer) {
      r = decodeURIComponent(req.headers.referer);
    }
    // console.log(r);
    if (!r) return next();

    let { query } = url.parse(r);
    let { route } = querystring.parse(query);
    if (typeof req.session.activeProjectUrl != 'undefined') {
      route = req.session.activeProjectUrl;
      delete req.session.activeProjectUrl;
      req.session.save();
    }
    if (!route) {
      return next();
    }
    if (route.endsWith('/')) {
      route = route.substr(0, route.length - 1);
    }

    return proxyController.proxyToPath(req, res, next, route);
  } else {
    next();
  }
});

// catch 404 and forward to error handler
/*app.use(function (req, res, next) {
  let err = new Error('Not Found');
  err.status = 404;
  next(err);
});

app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.send({
    message: err.message,
    error: err
  });
});*/

const port = process.env.PORT || config.get('port') || '3030';
app.set('port', port);

/**
 * Event listener for HTTP server "error" event.
 */
function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  let bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      // eslint-disable-next-line
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      // eslint-disable-next-line
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {
  let addr = server.address();
  let bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  console.log('Listening on ' + bind);
}

/**
 * Create HTTP server.
 */
let server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);


module.exports = server;