const { Router } = require('express');
const request = require('request-promise');
const Cryptr = require('cryptr');
const config = require('config');
const get = require('lodash/get');

const { Service } = require('../models/servicesModel');

const cryptr = new Cryptr(config.get('secret'));

class servicesController extends Router {
  constructor () {
    super();

    for (const service in config.get('services')) {
      this.use('/' + service + '/', new Service(config.get(`services.${service}`)));
    }
  }
}

const inviteToProject = ({ id, accessToken }) => {
  const { protocol, host, port, hash } = config.get('services.projects');
  return request({
    method: 'POST',
    headers: {
      'x-access-token': hash,
      'access-token': accessToken,
    },
    url: `${protocol}://${host}:${port}/${id}/member`,
  });
};

const inviteToProjectMiddleware = async (req, res) => {
  const { email, id } = JSON.parse(cryptr.decrypt(req.params.token));
  const user = get(req.session, 'user');
  const token = get(req.session, 'token');
  if (user && token && user.email === email) {
    try {
      await inviteToProject({ id, accessToken: get(req.session, 'token') });
      return res.redirect('/');
    } catch (e) {
      return res.status(e.statusCode).send(e);
    }
  }
  if (user && token) {
    req.session.destroy();
  }
  res.cookie('inviteToProject', JSON.stringify({ email, id }), { maxAge: 900000, httpOnly: true });

  return res.redirect('/#/login');
};

module.exports = {
  servicesController,
  inviteToProject,
  inviteToProjectMiddleware,
};