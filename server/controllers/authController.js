const { authServerModel } = require('../models/authModel');
const { inviteToProject } = require('./servicesController');

class authController extends authServerModel {
  constructor(router) {
    super();
    this.router = router;
    this.registerRoutes();
  }

  registerRoutes() {
    this.router.post('/login', this.loginUser.bind(this));
    this.router.post('/sign_up', this.registerUser.bind(this));
    this.router.get('/check_login', this.checkSession, this.checkLogin.bind(this));
  }

  async loginUser(req, res) {
    let { body } = req;
    if (!!body.email === false || body.password === false) {
      return res.status(400).send({
        errorCode: 400,
        message: 'email or password empty'
      });
    }
    try {
      let user = await this.login(body);
      await this.auhtorize(req, user.payload, user.token);
      const waitInvite = req.cookies.inviteToProject;
      if (waitInvite) {
        const { email, id } = JSON.parse(waitInvite);
        if (user.payload.email === email) {
          inviteToProject({ id, accessToken: user.token });
          res.clearCookie('inviteToProject');
        }
      }
      return res.send(req.session.user);
    } catch (e) {
      return res.status(e.statusCode).send(e);
    }
  }

  async registerUser(req, res) {

    let { body } = req;
    if (!!body.email === false || body.password === false) {
      return res.status(400).send({
        errorCode: 400,
        message: 'email or password empty'
      });
    }
    try {
      let user = await this.signUp(body);
      await this.auhtorize(req, user.payload, user.token);
      const waitInvite = req.cookies.inviteToProject;
      if (waitInvite) {
        const { email, id } = JSON.parse(waitInvite);
        if (user.payload.email === email) {
          inviteToProject({ id, accessToken: user.token });
          res.clearCookie('inviteToProject');
        }
      }
      return res.send(req.session.user);
    } catch (e) {
      return res.status(e.statusCode||500).send(e);
    }
  }

  checkLogin(req, res) {
    res.status(200).send(req.session.user);
  }
}

module.exports = { authController };