const { authController } = require('./authController');
const { proxyController } = require('./proxyController');
const { servicesController, inviteToProjectMiddleware } = require('./servicesController');

module.exports = {
  authController,
  proxyController,
  servicesController,
  inviteToProjectMiddleware
};