const proxy = require('http-proxy-middleware');
const url = require('url');
const querystring = require('querystring');
const zlib = require('zlib');
const iltorb = require('iltorb');

const { replaceHtmlLink, replaceHtmlForm } = require('../helpers/frame_script');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

class proxyController {
  constructor (router) {
    this.router = router;
    this.registerRoutes();
  }

  registerRoutes () {
    this.router.all('/', this.route.bind(this));
  }

  route (req, res, next) {
    return proxyController.proxyToPath(req, res, next);
  }

  static proxyToPath (req, res, next, importantRoute) {
    if (!importantRoute && !req.query.route && req.session.activeProjectUrl) {
      importantRoute = req.session.activeProjectUrl;
    }
    if (importantRoute || req.query.route) {
      const route = decodeURIComponent(importantRoute || req.query.route);
      const parsedRoute = url.parse(route);

      if (!parsedRoute.host) {
        return res.send();
      }
      req.session.activeProjectUrl = parsedRoute.host;
      req.session.save();

      const options = {
        target: `${parsedRoute.protocol}//${parsedRoute.host}`,
        changeOrigin: true,
        selfHandleResponse: true,
        autoRewrite: true,
        cookieDomainRewrite: '',
        cookiePathRewrite: '',
        hostRewrite: true,
        followRedirects: true,
        pathRewrite (path, req) {
          if (!importantRoute) {
            const parsedUrl = url.parse(req.url);
            const query = querystring.parse(parsedUrl.query);
            const route = url.parse(query.route);
            delete query.route;
            route.query = query;
            route.host = null;
            route.hostname = null;
            route.protocol = null;
            let str = url.format(route);
            if (str.startsWith('///')) {
              str = str.substr(2);
            } else if (str.startsWith('//')) {
              str = str.substr(1);
            }
            return str;
          }
          return path;
        },
        onProxyRes (proxyRes, req, res) {
          if (!proxyRes.headers['content-type']) {
            // console.log(proxyRes);
            return res.redirect(`/proxy?route=${encodeURIComponent(proxyRes.responseUrl)}`);
          }
          proxyRes.headers['cache-control'] = 'no-cache';
          let body = Buffer.from('');
          proxyRes.on('data', function (data) {
            body = Buffer.concat([body, data]);
          });
          proxyRes.on('end', function () {
            if (
              proxyRes.headers['content-type'].includes('text/html') ||
              proxyRes.headers['content-type'].includes('application/javascript')
            ) {
              const bodyString = proxyController.parseBody(proxyRes.headers['content-encoding'], body);
              body = replaceHtmlLink(bodyString, parsedRoute.host, parsedRoute.protocol);
              body = replaceHtmlForm(body, parsedRoute.host, parsedRoute.protocol);
              const length = Buffer.from(body).length;
              res.writeHead(proxyRes.statusCode, {
                ...proxyRes.headers,
                'content-length': length
              });
              res.write(proxyController.createBuffer(proxyRes.headers['content-encoding'], body));
            } else {
              res.writeHead(proxyRes.statusCode, proxyRes.headers);
              res.write(body);
            }
            res.end();
          });
        }
      };
      return proxy(options)(req, res, next);
    } else {
      res.send();
    }
  }

  static parseBody(contentEncoding, buffer) {
    switch (contentEncoding) {
      case 'gzip': {
        return zlib.gunzipSync(buffer).toString('utf8');
      }
      case 'br': {
        return iltorb.decompressSync(buffer).toString('utf8');
      }
      default: {
        return buffer.toString();
      }
    }
  }

  static createBuffer (contentEncoding, body) {
    const buffer = Buffer.from(body);
    switch (contentEncoding) {
      case 'gzip': {
        return zlib.gzipSync(buffer);
      }
      case 'br': {
        return iltorb.compressSync(buffer);
      }
      default: {
        return buffer;
      }
    }
  }
}

module.exports = { proxyController };