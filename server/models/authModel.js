const config = require('config');
const request = require('request-promise');
const fs = require('fs');
const path = require('path');
const authChecking = require('qa-auth-checking');

const authservice = new authChecking({
  authServer: config.get('auth'),
  decodeJWTKey: fs.readFileSync(path.resolve(__dirname, '..', 'keys', config.get('auth.keyPath')), 'utf8'),
});

class authServerModel {
  constructor () {
    this.request = request;
    this.uri = `${config.get('auth.protocol')}://${config.get('auth.host')}:${config.get('auth.port')}`;
    this.options = {
      headers: {
        'content-type': 'application/json',
        'x-access-token': config.get('auth.hash')
      },
      json: true
    };
  }

  checkSession (req, res, next) {
    if ('user' in req.session && 'token' in req.session) {
      next();
    } else {
      return res.status(401).send({
        errorCode: 401,
        message: 'User unauthorized'
      });
    }
  }

  async decodeJWT (token) {
    return authservice.decodeUserJWT(token);
  }

  async login (form) {
    try {
      const opts = {
        ...this.options,
        form,
        method: 'POST',
        uri: `${this.uri}/login`,
      };
      const response = await this.request(opts);
      const auth = await this.decodeJWT(response.token);
      return { ...auth, token: response.token };
    } catch (e) {
      console.error(e);
      throw e;
    }
  }

  async signUp (form) {
    try {
      let response = await this.request({
        ...this.options,
        form,
        method: 'POST',
        uri: `${this.uri}/sign_up`
      });
      let auth = await this.decodeJWT(response.token);
      return { ...auth, token: response.token };
    } catch (e) {
      throw e;
    }
  }

  async changeUserInfo (userId, form) {
    try {
      return this.request({
        ...this.options,
        form,
        method: 'PUT',
        uri: `${this.uri}/users/${userId}`
      });
    } catch (e) {
      throw e;
    }
  }

  async auhtorize (req, user, token) {
    req.session.user = user;
    req.session.token = token;
    return req.session.save();
  }
}

module.exports = { authServerModel }