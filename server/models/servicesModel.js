const { Router } = require('express');
const proxy = require('express-http-proxy');
const clone = require('lodash/cloneDeep');

class Service extends Router {
  constructor (serviceConfig) {
    super();
    const config = clone(serviceConfig);
    if (!config.url) {
      config.url = `${config.protocol}://${config.host}:${config.port}`;
    }
    this.use(proxy(config.url, {
      proxyReqOptDecorator: (proxyReqOpts, srcReq) => {
        proxyReqOpts.headers['x-access-token'] = config.hash;
        proxyReqOpts.headers['access-token'] = srcReq.session.token;
        return proxyReqOpts;
      }
    }));
    /*this.all('*', async function (req, res) {
      let urlParams = req.url.split('/').filter(Boolean).join('/');
      let requestUrl = this._config.url + '/' + urlParams;
      try {
        const data = await request({
          url: requestUrl,
          method: req.method,
          headers: {
            'x-access-token': this._config.hash,
            'access-token': req.session.token
          },
          json: true,
          form: req.body
        });
        res.send(data);
      } catch (e) {
        res.status(e.statusCode || 500).send(e.message);
      }
    }.bind(this));*/
  }
}

module.exports = { Service };