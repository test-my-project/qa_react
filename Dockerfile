FROM node:10-alpine

WORKDIR /code

COPY package.json .

RUN apk add --no-cache git

RUN yarn --pure-lockfile && yarn cache clean

COPY . /code

RUN yarn test

RUN yarn build

CMD yarn start