const api = {
  url: '/publicMyTestingProject/api/',
  version: 'v1/',
  routes: {
    registration: 'auth/sign_up',
    login: 'auth/login',
    checkLogin: 'auth/check_login',
    projects: 'projects',
    priorities: 'tasks/priorities',
    statuses: 'tasks/statuses',
    messenger: 'messenger',
    member: 'member',
    issues: 'tasks/tasks'
  },
  secret: 'verySecretKey',
  centrifugoUrl: 'ws://centrifugo.qa-services.gq/connection/websocket',
};

export default api;