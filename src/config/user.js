const user = {
  storageName: 'userData',
  firstLoginStorageName: 'userFirstLogin'
};

export default user;