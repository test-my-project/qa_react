const frame = {
  deviceList: [
    {
      name: 'Responsive',
      width: 414,
      height: 736,
      isRotateEnabled: true,
      isCustomEditEnable: true,
      isZoomEnabled: true
    },
    {
      name: 'Galaxy S5',
      width: 360,
      height: 640,
      isRotateEnabled: true,
      isCustomEditEnable: false,
      isZoomEnabled: true
    },
    {
      name: 'iPhone 5/SE',
      width: 320,
      height: 568,
      isRotateEnabled: true,
      isCustomEditEnable: false,
      isZoomEnabled: true
    },
    {
      name: 'iPhone 6/7/8',
      width: 375,
      height: 667,
      isRotateEnabled: true,
      isCustomEditEnable: false,
      isZoomEnabled: true
    },
    {
      name: 'iPhone 6/7/8 Plus',
      width: 414,
      height: 736,
      isRotateEnabled: true,
      isCustomEditEnable: false,
      isZoomEnabled: true
    },
    {
      name: 'iPhone X',
      width: 375,
      height: 812,
      isRotateEnabled: true,
      isCustomEditEnable: false,
      isZoomEnabled: true
    },
    {
      name: 'iPad',
      width: 768,
      height: 1024,
      isRotateEnabled: true,
      isCustomEditEnable: false,
      isZoomEnabled: true
    },
    {
      name: 'iPad Pro',
      width: 1024,
      height: 1366,
      isRotateEnabled: true,
      isCustomEditEnable: false,
      isZoomEnabled: true
    },
    {
      name: 'Full width',
      width: '100%',
      height: 'max',
      isRotateEnabled: false,
      isZoomEnabled: false,
      withNativeHeight: true
    }
  ],
  deviceListPlaceholder: 'Select the device',

  zoomList: [
    {
      percent: '50%',
      scale: '.5'
    },
    {
      percent: '75%',
      scale: '.75'
    },
    {
      percent: '100%',
      scale: '1'
    },
    {
      percent: '125%',
      scale: '1.25'
    },
    {
      percent: '150%',
      scale: '1.5'
    }
  ],
  zoomListPlaceholder: '100%',

  issueCreateFormConfig: [
    {
      name: 'title',
      value: '',
      placeholder: 'Issue title',
      label: 'Title',
      type: 'text',
      helpMessage: 'Paste here the the title of your issue',
      validate: {
        required: {
          value: true,
          errorMessage: 'Please fill in title field'
        },
        minLength: {
          value: 5,
          errorMessage: 'This field must be between 5 and 80 characters'
        },
        maxLength: {
          value: 80,
          errorMessage: 'This field must be between 5 and 80 characters'
        },
      }
    },
    {
      name: 'description',
      value: '',
      placeholder: 'Issue description',
      label: 'Description',
      type: 'textarea',
      helpMessage: 'Paste here the small details about your issue',
      validate: {
        required: {
          value: true,
          errorMessage: 'Please fill in project description field'
        },
        minLength: {
          value: 20,
          errorMessage: 'This field must be between 20 and 200 characters'
        },
        maxLength: {
          value: 200,
          errorMessage: 'This field must be between 20 and 200 characters'
        },
      }
    }
  ]
};

export default frame;