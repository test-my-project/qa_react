const general = {
  NOTIFICATION: {
    DELAY: 3000
  },
  SITE_LOADER: {
    DELAY: 2000
  },
  SMALL_LOADER: {
    DELAY: 1500
  }
};

export default general;