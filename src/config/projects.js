const projects = {
  fields: {
    projectType: {
      name: 'projectType',
      value: 'developing'
    },
    workType: {
      name: 'workType',
      value: 'Site'
    },
    projectUrl: {
      name: 'projectUrl',
      value: ''
    },
    projectName: {
      name: 'projectName',
      value: ''
    },
    shortDescription: {
      name: 'shortDescription',
      value: ''
    },
  },
  colorPicker: {
    default    : '#f44336',
    selected   : '#f44336',
    width      : 'auto',
    circleSize : 22
  }
};

export default projects;