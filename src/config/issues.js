export const issues = {
  SELECTED_BY_DEFAULT_STATUS_ID: 1,
  SELECTED_BY_DEFAULT_PRIORITY_ID: 2,
  SELECTED_BY_DEFAULT_ASSIGN_USER_ID: 1
};

export const prioritiesIcons = [
  ['arrow-bottom', 'secondary'],
  ['arrow-top', 'primary'],
  ['arrow-top', 'danger'],
  ['ban', 'danger']
];