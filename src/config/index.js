/**
 * Api configuration
 */
import api from './api';

/**
 * General config
 */
import general from './general';

/**
 * App routes configuration
 */
import { routes, pageRoutes } from './routes';

/**
 * User config
 */
import user from './user';

/**
 * ProjectsList config
 */
import projects from './projects';

/**
 * Aside menu navigation config
 */
import navigation from './navigation';

/**
 * iFrame main configuration
 */
import frame from './frame';

/**
 * Issues main configuration
 */
import { issues, prioritiesIcons } from './issues';

export {
  api,
  general,
  routes,
  issues,
  frame,
  prioritiesIcons,
  pageRoutes,
  projects,
  navigation,
  user
};