import React, { Component } from 'react';
import { connect } from 'react-redux';
import { checkLogin } from './redux/actions';

import {
  HashRouter,
  Route,
  Switch
} from 'react-router-dom';

/**
 * Container
 */
import {
  DefaultLayout,
  FrameLayout,
  Login,
  Page404,
  Page500,
  Register
} from './containers';

/**
 * Custom routes
 */
import {
  ProtectedRoute,
  PrivatRoute
} from './routes';

/**
 * Register global styles
 */
import 'react-toastify/dist/ReactToastify.css';
import './App.scss';

class App extends Component {
  componentDidMount() {
    this.props.onCheckLogin();
  }

  render() {
    return (
      <HashRouter>
        <Switch>
          <PrivatRoute exact path="/login" name="Login Page" component={Login} />
          <PrivatRoute exact path="/register" name="Register Page" component={Register} />
          <Route exact path="/404" name="Page 404" component={Page404} />
          <Route exact path="/500" name="Page 500" component={Page500} />
          <ProtectedRoute exact path="/project/:id" name="Frame Layout" component={FrameLayout} />
          <ProtectedRoute path="/" name="Home" component={DefaultLayout} />
        </Switch>
      </HashRouter>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.authReducer.userData
  };
};

const mapDispatchToProps = dispatch => ({
  onCheckLogin: () => {
    dispatch(checkLogin);
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);