import _ from 'lodash';
import { pageRoutes } from './../../config';

export const checkLocation = () => {
  return window.location.pathname;
};

export const checkHash = () => {
  return window.location.hash;
};

export const isDashboard = () => {
  return _.includes(checkLocation(), pageRoutes.dashboard || '')
    || _.includes(checkHash(), pageRoutes.dashboard || '');
};