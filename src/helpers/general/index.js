export const generateRandomNumber = (min = 0, max = 10000) => {
  let num = Math.random() * (max-min) + min;
  return Math.floor(num);
};

export const getFirstLetter = (str) => {
  return str.charAt(0);
};

export const windowSize = () => {
  let width = 0, height = 0;

  if( typeof( window.innerWidth ) == 'number' ) {
    //Non-IE
    width = window.innerWidth;
    height = window.innerHeight;
  } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
    //IE 6+ in 'standards compliant mode'
    width = document.documentElement.clientWidth;
    height = document.documentElement.clientHeight;
  } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
    //IE 4 compatible
    width = document.body.clientWidth;
    height = document.body.clientHeight;
  }

  return { width, height };
};

export const compareHeight = height => {
  const currentHeight = windowSize().height;
  if (height === ('max' || 'auto')) return currentHeight;
  return currentHeight >= height ? height : currentHeight;
};

export const capitalize = s => {
  if (typeof s !== 'string') return '';
  return s.charAt(0).toUpperCase() + s.slice(1);
};