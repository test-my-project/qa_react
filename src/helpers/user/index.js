import { user, pageRoutes } from './../../config';

export const checkUser = () => {
  return localStorage.getItem(user.storageName);
};

export const checkAuth = (path) => {
  if (checkUser()) return true;
  const loginPage = path.indexOf(pageRoutes.login) !== -1;
  const registerPage = path.indexOf(pageRoutes.register) !== -1;
  return !checkUser() && (loginPage || registerPage);
};