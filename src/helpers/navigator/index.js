import { pageRoutes } from './../../config';

class GoTo {
  dashboard = helper => {
    helper.props.history.push(pageRoutes.dashboard);
  };
}

export default new GoTo();