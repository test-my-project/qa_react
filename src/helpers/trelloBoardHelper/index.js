import _ from 'lodash';

export const trelloBoardDataGenerator = (opts) => {
  const { issues, statuses, priorities, members } = opts;

  const lanes = _.map(statuses, s => {
    return {
      id: `${ s.id }`,
      title: s.title,
      cards: []
    };
  });

  _.map(lanes, l => {
    _.map(issues, i => {

      // eslint-disable-next-line eqeqeq
      if (i.task_status_id == l.id) {
        const label = _.compact(_.map(priorities, p => { if (i.task_priority_id === p.id) return p.title; }));
        let assignedUser;
        if (members) {
          assignedUser = _.find(members, ['id', i.assignee_user_id]) || 1;
        }

        l.cards.push({
          title: i.title,
          description: i.description.trim(),
          id: `${ i.id }`,
          label: label[0],
          priorityId: i.task_priority_id,
          assignedUser
        });
      }
    });

    const cardLength = l.cards.length;

    l.label = `${cardLength} issue${ cardLength !== 1 ? 's' : '' }`;

    return l;
  });

  return { lanes: [ ...lanes ] };
};