import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Row, Col } from 'reactstrap';

import { CustomDropdownList } from './../';

class CustomIssuesControlPanel extends Component {
  render() {
    const {
      projects,
      onProjectChange
    } = this.props;

    return (
      <Row>
        <Col>
          <CustomDropdownList
            data        = { _.map(projects, 'name') }
            placeholder = { 'Select project' }
            onChange    = { v => onProjectChange(v) }
          />
        </Col>

        <Col>
          <CustomDropdownList
            placeholder = { 'Created by' }
            onChange    = { value => console.log(value) }
          />
        </Col>

        <Col>
          <CustomDropdownList
            placeholder = { 'Assign to' }
            onChange    = { value => console.log(value) }
          />
        </Col>
      </Row>
    );
  }
}

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

CustomIssuesControlPanel.propTypes = propTypes;
CustomIssuesControlPanel.defaultProps = defaultProps;

export default CustomIssuesControlPanel;