import React, { Component } from 'react';

class Preloader extends Component {
  render() {
    const { relative } = this.props;

    const wrapperClass = `preloader__wrapper ${relative ? 'relative' : ''}`;

    return (
      <div className={ wrapperClass }>
        <div className="preloader">
          <div className="pre-cube1 pre-cube" />
          <div className="pre-cube2 pre-cube" />
          <div className="pre-cube4 pre-cube" />
          <div className="pre-cube3 pre-cube" />
        </div>
        <p>Loading...</p>
      </div>
    );
  }
}

export default Preloader;