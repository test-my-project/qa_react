import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import {
  Card,
  CardBody,
  Col
} from 'reactstrap';

import { pageRoutes } from './../../../config';

class DashboardAddProject extends Component {
  render() {
    return (
      <Col xs="12" sm="6" lg="3" className="project-card add">
        <Card>
          <CardBody>
            <Link to={pageRoutes.projects.add}>
              <div>
                <p className="text-value">Create new project</p>
                Short description bla bla bla
              </div>
            </Link>
          </CardBody>
        </Card>
      </Col>
    );
  }
}

export default DashboardAddProject;