import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  AvForm,
  AvField
} from 'availity-reactstrap-validation';

import {
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  FormGroup,
  Row
} from 'reactstrap';

import { CustomFormAdvancedLayout, CustomSpinnerButton } from './../';
import { projects } from './../../../config';

const FIELDS = projects.fields;

class DefaultFormFieldsToRender extends Component {
  render() {
    const {
      parentState,
      onFieldChange,
      id
    } = this.props;

    const {
      projectUrl,
      projectName,
      shortDescription
    } = parentState.fields;

    const projectType = FIELDS.projectType.name;
    const workType    = FIELDS.workType.name;
    const name        = FIELDS.projectName.name;
    const url         = FIELDS.projectUrl.name;
    const description = FIELDS.shortDescription.name;

    return (
      <React.Fragment>
        <FormGroup row>
          <Col xs="12" md="6">
            <AvField
              type        = "select"
              label       = "Project type"
              data-name   = { projectType }
              name        = { projectType + '-' + id }
              id          = { projectType + '-' + id }
              onChange    = { e => onFieldChange(e, id) }
              helpMessage = "Select the type of your project"
            >
              <option value="developing">Developing</option>
              <option value="supporting">Supporting</option>
            </AvField>
          </Col>
          <Col xs="12" md="6">
            <AvField
              type        = "select"
              label       = "Project view"
              data-name   = { workType }
              name        = { workType }
              id          = { workType + '-' + id }
              onChange    = { e => onFieldChange(e, id) }
              helpMessage = "Select the type of your project"
            >
              <option value="Site">Site</option>
              <option value="Design">Design</option>
              <option value="Prototypes">Prototypes</option>
            </AvField>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Col xs="12" md="6">
            <AvField
              name        = { name + '-' + id }
              data-name   = { name }
              id          = { name + '-' + id }
              value       = { projectName }
              onChange    = { e => onFieldChange(e, id) }
              label       = "Project name"
              placeholder = "My awesome project"
              helpMessage = "The name of your awesome project"
              validate={{
                required: {
                  value: true,
                  errorMessage: 'Please fill in project name filed'
                },
                pattern: {
                  value: '^[A-Za-zА-Яа-я0-9\\s]+$',
                  errorMessage: 'This field must be composed only with letter and numbers'
                },
                minLength: {
                  value: 6,
                  errorMessage: 'Min length 6 symbols'
                },
              }} />
          </Col>
          <Col xs="12" md="6">
            <AvField
              name        = { url + '-' + id }
              data-name   = { url }
              id          = { url + '-' + id }
              value       = { projectUrl }
              onChange    = { e => onFieldChange(e, id) }
              label       = "Project URL"
              placeholder = "https://awesome.com"
              helpMessage = "Paste here an url for your project"
              validate={{
                required: {
                  value: true,
                  errorMessage: 'Please fill in project URL field'
                },
                pattern: {
                  value: 'https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|www\\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\\.[^\\s]{2,}|https?:\\/\\/(?:www\\.|(?!www))[a-zA-Z0-9]\\.[^\\s]{2,}|www\\.[a-zA-Z0-9]\\.[^\\s]{2,}\n',
                  errorMessage: 'This field must contain http or https site links'
                }
              }} />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Col xs="12" md="12">
            <AvField
              type        = "textarea"
              placeholder = "Content..."
              data-name   = { description }
              name        = { description + '-' + id }
              id          = { description + '-' + id }
              value       = { shortDescription }
              onChange    = { e => onFieldChange(e, id) }
              label       = "Project description"
              helpMessage = "Paste here the small details about your project"
              rows        = "5"
              require     = "true"
              validate={{
                required: {
                  value: true,
                  errorMessage: 'Please fill in project description field'
                },
                maxLength: {
                  value: 1000,
                  errorMessage: 'This field must be between 0 and 1000 characters'
                },
              }} />
          </Col>
        </FormGroup>
      </React.Fragment>
    );
  }
}

class DefaultFormFooterToRender extends Component {
  constructor(props) {
    super(props);

    this.state = {
      footerControls: { color: 'primary', text: 'Submit', type: 'validate' }
    };

    this.rowToRender     = this.rowToRender.bind(this);
    this.buttonsToRender = this.buttonsToRender.bind(this);
  }

  buttonsToRender(opts) {
    const { onRemove, isRequestLoading } = this.props;

    if (!opts) {
      return (
        <Col col="3" sm="12" md="2">
          <CustomSpinnerButton
            { ...this.state }
            isRequestLoading = { isRequestLoading }
          />
        </Col>
      );
    } else {
      return _.map(opts, c => {
        const { color, text, type } = c;
        const isValidate = type === 'validate';

        if (isValidate) return (
          <Col key={ type } col="3" sm="12" md="2">
            <CustomSpinnerButton
              { ...c }
              isRequestLoading = { isRequestLoading }
            />
          </Col>
        );

        return (
          <Col key={ type } col="3" sm="12" md="2">
            <Button
              color={ color }
              onClick={ onRemove }
              block
            >
              { text }
            </Button>
          </Col>
        );
      });
    }
  }

  rowToRender() {
    const { customControls } = this.props;

    return (
      <Row className="flex-row-reverse">
        { this.buttonsToRender(customControls) }
      </Row>
    );
  }

  render() {
    const { withoutWrapper } = this.props;

    if (withoutWrapper) return this.rowToRender();

    return (
      <CardFooter>
        { this.rowToRender() }
      </CardFooter>
    );
  }
}

class RemoveFormBtn extends Component {
  render() {
    const { onRemove, id } = this.props;

    return (
      <i className="zmdi zmdi-close sub-project__remove" onClick={ onRemove.bind(this, id) } />
    );
  }
}

class CustomSubFormHeader extends Component {
  render() {
    const { isFirst, index, onRemoveSubProject } = this.props;

    if (isFirst) return null;

    return (
      <Row className="header">
        <Col md="12">
          <RemoveFormBtn id={ index } onRemove={ onRemoveSubProject }/>
          <h4 className="sub-project__title">Sub-Project #{index}</h4>
        </Col>
      </Row>
    );
  }
}

class CustomForm extends Component {
  constructor(props) {
    super(props);
    this.state = {};

    this.formSectionsToRender = this.formSectionsToRender.bind(this);
    this.onValidate           = this.onValidate.bind(this);
  }

  onValidate(e, errors) {
    if (errors && errors.length === 0) {
      this.props.onSubmit();
    }
  }

  formSectionsToRender(props){
    const { parentState, onRemoveSubProject, onFieldChange, advancedFields } = props;

    return parentState.forms.map((p, index) => {
      const isFirst = index === 0;
      const className = !isFirst ? 'sub-project' : '';
      return (
        <CardBody
          id        = { index }
          key       = { index }
          className = { className }
        >
          <CustomSubFormHeader
            isFirst            = { isFirst }
            index              = { index }
            onRemoveSubProject = { onRemoveSubProject }
          />
          <DefaultFormFieldsToRender
            onFieldChange = { onFieldChange }
            parentState   = { p }
            id            = { index }
          />
          { advancedFields &&
            <CustomFormAdvancedLayout
              {...props}
              id          = { index }
              parentState = { p }
            />
          }
        </CardBody>
      );
    });
  }

  render() {
    const { formSectionsToRender, props, onValidate } = this;
    const { isRequestLoading } = props;

    return (
      <React.Fragment>
        <Row>
          <Col xs="12">
            <Card>
              <CardHeader>
                <strong>New project</strong>
              </CardHeader>
              <AvForm
                onSubmit  = { onValidate }
                className = "custom-form form-horizontal"
              >
                { formSectionsToRender(props) }
                <DefaultFormFooterToRender
                  isRequestLoading = { isRequestLoading }
                />
              </AvForm>
            </Card>
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

CustomForm.propTypes = propTypes;
CustomForm.defaultProps = defaultProps;

export {
  DefaultFormFieldsToRender,
  DefaultFormFooterToRender,
  CustomForm
};