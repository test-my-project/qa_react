import React, { Component } from 'react';
import PropTypes from 'prop-types';

class CustomPoint extends Component {
  render() {
    const { data } = this.props;

    if (!data) return '';

    const { x, y } = data.pointer;

    const style = {
      top: `${ y }px`,
      left: `${ x }px`
    };

    return (
      <React.Fragment>
        <div className="animated fadeIn pointer" style={{ ...style }}>
          1
        </div>
      </React.Fragment>
    );
  }
}

const propTypes = {
  x: PropTypes.string,
  y: PropTypes.string
};

const defaultProps = {
  x: '',
  y: ''
};

CustomPoint.propTypes = propTypes;
CustomPoint.defaultProps = defaultProps;

export default CustomPoint;