import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { DelayInput } from 'react-delay-input';

import { Nav, NavItem } from 'reactstrap';

import {
  CustomDropdownList,
  CustomTooltip,
  CustomTooltipBtn
} from './../';

const FRAME_INPUT_SIZE_DIGITS_COUNT = 2;
const FRAME_INPUT_SIZE_DELAY        = 600;

class FrameNavigation extends Component {
  render() {
    const {
      onSelectDevice,
      onProjectChange,
      onZoomChange,
      onInputChange,
      onRotateFrame,
      onEventsEnable,
      isEventsEnabled,
      frameConfig,
      isAsideOpened,
      parentState,
      projectsList
    } = this.props;
    const {
      deviceList,
      deviceListPlaceholder,
      zoomList,
      zoomListPlaceholder
    } = frameConfig;
    const {
      width,
      height,
      url,
      name,
      isCustomEditEnable,
      isZoomEnabled,
      isPointerEnable,
      isRotateEnabled
    } = parentState;

    const asideIssuesBtnClassName = isEventsEnabled && !isAsideOpened ? 'icon-plus opened' : 'icon-plus';

    return (
      <Nav
        className="d-md-down-none f__controls"
        navbar
      >
        { projectsList && name && <NavItem className="px-2">
          <CustomDropdownList
            data        = { _.map(projectsList, 'name') }
            placeholder = { name }
            onChange    = { value => onProjectChange(value) }
            disabled    = { isAsideOpened }
          />
        </NavItem> }
        <NavItem className="px-2">
          <CustomTooltip
            content        = {
              <DelayInput
                className   = "form-control medium"
                value       = { url }
                type        = "text"
                id          = "url"
                name        = "url"
                placeholder = "url"
                disabled
              />
            }
            id             = { 'tooltip-url' }
            tooltipContent = { url }
          />
        </NavItem>
        <NavItem className="px-2">
          <CustomDropdownList
            data        = { _.map(deviceList, 'name') }
            placeholder = { deviceListPlaceholder }
            onChange    = { value => onSelectDevice(value) }
            disabled    = { isAsideOpened }
          />
        </NavItem>
        <NavItem className="px-2">
          <CustomTooltip
            content        = {
              <DelayInput
                className    = "form-control"
                delayTimeout = { FRAME_INPUT_SIZE_DELAY }
                minLength    = { FRAME_INPUT_SIZE_DIGITS_COUNT }
                type         = "number"
                id           = "width"
                placeholder  = "width"
                value        = { width }
                disabled     = { !isCustomEditEnable || isAsideOpened }
                onChange     = { e => onInputChange(e) }
              />
            }
            id             = { 'tooltip-width' }
            tooltipContent = { 'Frame width' }
          />
        </NavItem>
        <NavItem className="px-2">
          <CustomTooltip
            content        = {
              <DelayInput
                className    = "form-control"
                delayTimeout = { FRAME_INPUT_SIZE_DELAY }
                minLength    = { FRAME_INPUT_SIZE_DIGITS_COUNT }
                type         = "number"
                id           = "height"
                placeholder  = "height"
                value        = { height }
                disabled     = { !isCustomEditEnable || isAsideOpened }
                onChange     = { e => onInputChange(e) }
              />
            }
            id             = { 'tooltip-height' }
            tooltipContent = { 'Frame height' }
          />
        </NavItem>
        <NavItem className="px-2">
          <CustomDropdownList
            disabled     = { !isZoomEnabled }
            data         = { _.map(zoomList, 'percent') }
            placeholder  = { zoomListPlaceholder || isAsideOpened }
            onChange     = { value => onZoomChange(value) }
            className    = "tiny"
          />
        </NavItem>
        <NavItem className="px-2">
          <CustomTooltipBtn
            onClick        = { onRotateFrame }
            content        = { <i className="icon-refresh" /> }
            id             = "rotate"
            tooltipContent = "Rotate"
            size           = "lg"
            className      = "btn-link"
            disable        = { !isRotateEnabled || isAsideOpened }
          />
          <CustomTooltipBtn
            onClick        = { onEventsEnable }
            content        = { <i className={ asideIssuesBtnClassName } /> }
            id             = "pointer"
            tooltipContent = "Create task"
            size           = "lg"
            className      = "btn-link"
            disable        = { !isPointerEnable || isAsideOpened }
          />
        </NavItem>
      </Nav>
    );
  }
}

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

FrameNavigation.propTypes = propTypes;
FrameNavigation.defaultProps = defaultProps;

export default FrameNavigation;