import React from 'react';
import PropTypes from 'prop-types';

import WrappedComponent from './../../../containers/View/WrappedComponent';
import {
  CustomDropdownList
} from './../';

class CustomAssignUserDropdown extends WrappedComponent {
  render(){
    const {
      data,
      filter,
      placeholder,
      emptyFilter,
      filterPlaceholder,
      onChange
    } = this.props;

    return <CustomDropdownList
      filter      = { filter }
      placeholder = { placeholder }
      data        = { data }
      messages    = {{ emptyFilter, filterPlaceholder }}
      onChange    = { v => onChange(v) }
    />;
  }
}

const propTypes = {
  filter            : PropTypes.bool,
  filterPlaceholder : PropTypes.string,
  emptyFilter       : PropTypes.string,
  placeholder       : PropTypes.string
};

const defaultProps = {
  filter            : true,
  placeholder       : 'Unassigned',
  emptyFilter       : 'No users found',
  filterPlaceholder : 'Type username'
};

CustomAssignUserDropdown.propTypes = propTypes;
CustomAssignUserDropdown.defaultProps = defaultProps;

export default CustomAssignUserDropdown;