import React, { Component } from 'react';

import {
  Button,
  FormText
} from 'reactstrap';

import { CustomCropperModal } from './../';

class CustomCropperLayout extends Component {
  constructor(props) {
    super(props);

    this.acceptAction = this.acceptAction.bind(this);
  }

  acceptAction(p) {
    const { modalToggle, id } = p;
    modalToggle(id);
  }

  avatarImgToRender = (img, letter, resetHandler, id) => {
    return img ?
      <React.Fragment>
        <img src={ img } alt="Project avatar" />
        <i className="zmdi zmdi-close" onClick={ resetHandler.bind(this, id) } />
        <small>Remove</small>
      </React.Fragment>
      : letter;
  };

  render(){
    const { acceptAction, props, avatarImgToRender } = this;

    const {
      color: previewBg,
      avatarLetter,
      avatarImg,
      modalToggle,
      modalState,
      avatarHandler,
      avatarReset,
      id
    } = props;

    const previewClass = avatarImg ? 'cropper__preview with-avatar' : 'cropper__preview';

    return (
      <React.Fragment>
        <div className={ previewClass } style={{ backgroundColor: previewBg }}>
          { avatarImgToRender(avatarImg, avatarLetter, avatarReset, id) }
        </div>
        <div className="cropper__wrap">
          <Button onClick={ modalToggle.bind(this, id) } color="primary">Select an image</Button>
          <FormText color="muted">You can chose image that you want</FormText>
        </div>
        <CustomCropperModal
          getImageFromCropper = { avatarHandler.bind(this, id) }
          toggle              = { modalToggle.bind(this, id) }
          state               = { modalState }
          acceptAction        = { acceptAction.bind(this, props) } />
      </React.Fragment>
    );
  }
}

export default CustomCropperLayout;