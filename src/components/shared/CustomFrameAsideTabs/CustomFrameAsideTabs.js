import _ from 'lodash';
import React from 'react';
import {
  CustomTabs,
  CustomFrameAsideValidationForm
} from './../';

import {
  ListGroup,
  ListGroupItem,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane
} from 'reactstrap';

import classnames from 'classnames';

class CustomFrameAsideTabs extends CustomTabs {
  constructor(props) {
    super(props);

    this.state = {
      ...this.state
    };

    this.pointerDataToRender = this.pointerDataToRender.bind(this);
    this.issueDataToRender   = this.issueDataToRender.bind(this);
    this.tabsToRender        = this.tabsToRender.bind(this);
  }

  tabsToRender() {
    const { data } = this.props;
    const tabsData = _.map(data, d => d.title);

    return tabsData.map((t, key) => {
      const id = key + 1 + '';

      return (
        <NavItem key={ key }>
          <NavLink
            className={ classnames({ active: this.state.activeTab === id } )}
            onClick={() => { this.toggle(id); }}
          >
            <i className={ `icon-${ key === 0 ? 'pie-chart' : 'layers' }` } />
            <span className={ this.state.activeTab === id ? '' : 'd-none' }> { t }</span>
          </NavLink>
        </NavItem>
      );
    });
  }

  tabsContentToRender() {
    const { data } = this.props;
    const tabsContent = _.map(data, d => d.data);

    return (
      <TabContent
        activeTab = { this.state.activeTab }
        className = { 'animated fadeIn' }
      >
        { tabsContent.map((c, key) => {
          const id = key + 1 + '';

          return (
            <TabPane
              key   = { key }
              tabId = { id }
            >
              { key === 1 ? this.pointerDataToRender(c) : this.issueDataToRender(c) }
            </TabPane>
          );
        })}
      </TabContent>
    );
  }

  issueDataToRender = data => {
    const { statuses, priorities, collectFormData, onRequestStarted, isRequestLoading, members } = this.props;

    return <CustomFrameAsideValidationForm
      members          = { members }
      data             = { data }
      priorities       = { priorities }
      statuses         = { statuses }
      collectFormData  = { collectFormData }
      onRequestStarted = { onRequestStarted }
      isRequestLoading = { isRequestLoading }
    />;
  };

  pointerDataToRender(data) {
    return (
      <ListGroup>
        { Object.keys(data).map((d, key) => {
          return (
            <ListGroupItem key={ key } className={ `list-group-item-accent-${data[d] ? 'primary' : 'danger'} list-group-item-divider` }>
              <div><strong style={{ textTransform: 'capitalize' }}>{ d }</strong>:</div>
              <small className="text-muted mr-3">
                { data[d] ? JSON.stringify(data[d]) : 'Undefined data' }
              </small>
            </ListGroupItem>
          );
        })}
      </ListGroup>
    );
  }

  render() {
    const {
      tabsToRender,
      tabsContentToRender
    } = this;

    return (
      <React.Fragment>
        <Nav tabs>
          { tabsToRender() }
        </Nav>
        { tabsContentToRender() }
      </React.Fragment>
    );
  }
}

export default CustomFrameAsideTabs;