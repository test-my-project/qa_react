import _ from 'lodash';
import React, { Component } from 'react';

import {
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane
} from 'reactstrap';

import classnames from 'classnames';

class CustomTabs extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '1',
    };

    this.tabsToRender        = this.tabsToRender.bind(this);
    this.tabsContentToRender = this.tabsContentToRender.bind(this);
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  tabsToRender() {
    const { data } = this.props;
    const tabsData = _.map(data, d => d.title);

    return tabsData.map((t, key) => {
      const id = key + 1 + '';

      return (
        <NavItem key={ key }>
          <NavLink
            className={ classnames({ active: this.state.activeTab === id } )}
            onClick={() => { this.toggle(id); }}
          >
            <span>{ t }</span>
          </NavLink>
        </NavItem>
      );
    });
  }

  tabsContentToRender() {
    const { data } = this.props;
    const tabsContent = _.map(data, d => d.data);


    return (
      <TabContent
        activeTab = { this.state.activeTab }
      >
        { tabsContent.map((c, key) => {
          const id = key + 1 + '';

          return (
            <TabPane
              key   = { key }
              tabId = { id }
            >
              { c }
            </TabPane>
          );
        }) }
      </TabContent>
    );
  }

  render() {
    const {
      tabsToRender,
      tabsContentToRender
    } = this;

    return (
      <React.Fragment>
        <Nav tabs>
          { tabsToRender() }
        </Nav>
        { tabsContentToRender() }
      </React.Fragment>
    );
  }
}

export default CustomTabs;