import React, { Component } from 'react';

import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from 'reactstrap';

import {
  AvField, AvForm
} from 'availity-reactstrap-validation';

import { CustomSpinnerButton } from './../';

class CustomFormModal extends Component {
  render(){
    const {
      toggle,
      state,
      title,
      acceptBtn,
      cancelBtn,
      onValidate,
      onFieldChange,
      validate,
      className,
      isRequestLoading
    } = this.props;

    return (
      <Modal
        isOpen={ state }
        toggle={ toggle }
        className={ className }>
        <ModalHeader
          toggle={ toggle }>
          { title }
        </ModalHeader>
        <ModalBody className="text-left">
          <AvForm
            onSubmit  = { onValidate }
            className = "custom-form form-horizontal"
          >
            <AvField
              name        = 'member'
              data-name   = 'member'
              id          = 'member'
              onChange    = { e => onFieldChange(e) }
              label       = "Email"
              placeholder = "Email"
              helpMessage = "Please provide an email to invite, for example awesome@gmail.com"
              validate={{
                required: {
                  value: true,
                  errorMessage: 'Please fill in email filed'
                },
                pattern: {
                  value: '[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?',
                  errorMessage: 'This field must be correct email address'
                },
              }} />
            <ModalFooter>
              { validate
                ? <CustomSpinnerButton
                  color            = "primary"
                  text             = { acceptBtn }
                  isRequestLoading = { isRequestLoading }
                />
                : <Button color="primary" >{ acceptBtn }</Button> }
              {' '}
              <Button color={ validate ? 'danger' : 'primary' } onClick={ toggle }>{ cancelBtn }</Button>
            </ModalFooter>
          </AvForm>
        </ModalBody>
      </Modal>
    );
  }
}

export default CustomFormModal;