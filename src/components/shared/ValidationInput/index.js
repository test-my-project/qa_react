import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { AvField } from 'availity-reactstrap-validation';

class ValidationInput extends Component {
  render() {
    const { value, name, type, placeholder, onChange, validate, helpMessage } = this.props;

    return (
      <AvField
        label       = { name }
        helpMessage = { helpMessage }
        value       = { value }
        id          = { name }
        type        = { type }
        placeholder = { placeholder }
        name        = { name }
        onChange    = { (value, e) => { onChange(value, e); } }
        validate    = { validate }
      />
    );
  }
}

const propTypes = {
  id: PropTypes.string,
  name: PropTypes.string.isRequired,
  value: PropTypes.string,
  placeholder: PropTypes.string.isRequired,
  validate: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired
};

const defaultProps = {};

ValidationInput.propTypes = propTypes;
ValidationInput.defaultProps = defaultProps;

export default ValidationInput;