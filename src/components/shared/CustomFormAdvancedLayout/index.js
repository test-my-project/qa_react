import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { CirclePicker } from 'react-color';

import {
  Col,
  FormGroup,
  FormText,
  Label
} from 'reactstrap';

import { CustomCropperLayout } from './../';

class CustomFormAdvancedLayout extends Component {
  render() {
    const {
      avatarHandler,
      avatarReset,
      colorPickerHandler,
      parentState,
      modalToggle,
      id
    } = this.props;

    const { colorPicker, avatar } = parentState;

    /**
     * Avatar cropper state values
     */
    const { firstLetter, modal, img } = avatar;

    /**
     * Color picker state values
     */
    const { selected, width, circleSize } = colorPicker;

    return (
      <FormGroup row>
        <Col md="2">
          <Label htmlFor="avatar-cropper">Project avatar</Label>
        </Col>
        <Col xs="12" md="4">
          <CustomCropperLayout
            id            = { id }
            avatarImg     = { img }
            avatarHandler = { avatarHandler }
            avatarReset   = { avatarReset }
            avatarLetter  = { firstLetter }
            modalToggle   = { modalToggle }
            modalState    = { modal }
            color         = { selected.hex || selected }
            name          = "avatar-cropper" />
        </Col>
        <Col md="2">
          <Label htmlFor="color-picker">Pick your project color</Label>
        </Col>
        <Col xs="12" md="4">
          <CirclePicker
            color            = { selected }
            onChangeComplete = { colorPickerHandler.bind(this, id) }
            width            = { width }
            circleSize       = { circleSize }
            name             = "color-picker" />
          <FormText color="muted">Choose project default color, first is used by default</FormText>
        </Col>
      </FormGroup>
    );
  }
}

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

CustomFormAdvancedLayout.propTypes = propTypes;
CustomFormAdvancedLayout.defaultProps = defaultProps;

export default CustomFormAdvancedLayout;