import React, { Component } from 'react';
import Cropper from 'react-cropper';

import {
  Col,
  Row
} from 'reactstrap';

import { CustomTooltipBtn } from './../';

import DEFAULT_SRC from './../../../assets/img/test/test.png';
import 'cropperjs/dist/cropper.css';

const CONTROLS_DEFAULT_COLOR     = 'ghost-light';
const CONTROLS_DEFAULT_CLASSNAME = 'mr-3';
const CONTROLS_DEFAULT_SIZE      = '';

class CustomCropper extends Component {
  constructor(props){
    super(props);

    this.CONTROLS = [
      {
        className      : CONTROLS_DEFAULT_CLASSNAME,
        color          : CONTROLS_DEFAULT_COLOR,
        size           : CONTROLS_DEFAULT_SIZE,
        content        : this.uploadBtnToRender(),
        tooltipContent : 'Upload an image'
      },
      {
        className      : CONTROLS_DEFAULT_CLASSNAME,
        color          : CONTROLS_DEFAULT_COLOR,
        size           : CONTROLS_DEFAULT_SIZE,
        content        : <i className="zmdi zmdi-zoom-in" />,
        tooltipContent : 'Zoom in',
        onClick        : this.zoomIn
      },
      {
        className      : CONTROLS_DEFAULT_CLASSNAME,
        color          : CONTROLS_DEFAULT_COLOR,
        size           : CONTROLS_DEFAULT_SIZE,
        content        : <i className="zmdi zmdi-zoom-out" />,
        tooltipContent : 'Zoom out',
        onClick        : this.zoomOut
      },
      {
        className      : CONTROLS_DEFAULT_CLASSNAME,
        color          : CONTROLS_DEFAULT_COLOR,
        size           : CONTROLS_DEFAULT_SIZE,
        content        : <i className="zmdi zmdi-rotate-left" />,
        tooltipContent : 'Rotate left',
        onClick        : this.rotateLeft
      },
      {
        className      : CONTROLS_DEFAULT_CLASSNAME,
        color          : CONTROLS_DEFAULT_COLOR,
        size           : CONTROLS_DEFAULT_SIZE,
        content        : <i className="zmdi zmdi-rotate-right" />,
        tooltipContent : 'Rotate right',
        onClick        : this.rotateRight
      },
      {
        className      : CONTROLS_DEFAULT_CLASSNAME,
        color          : CONTROLS_DEFAULT_COLOR,
        size           : CONTROLS_DEFAULT_SIZE,
        content        : <i className="zmdi zmdi-refresh-alt" />,
        tooltipContent : 'Refresh changes',
        onClick        : this.refresh
      },
    ];

    this.cropper= React.createRef();

    this.state = {
      src: DEFAULT_SRC,
      style: {
        height: 270,
        width: '100%',
        overflow: 'hidden'
      }
    };

    this.crop = this.crop.bind(this);
    this.refresh = this.refresh.bind(this);
    this.zoomIn = this.zoomIn.bind(this);
    this.zoomOut = this.zoomOut.bind(this);
    this.rotateLeft = this.rotateLeft.bind(this);
    this.rotateRight = this.rotateRight.bind(this);
    this.uploadFile = this.uploadFile.bind(this);
    this.uploadBtnToRender = this.uploadBtnToRender.bind(this);
  }

  crop(){
    this.props.crop(this.cropper.current.getCroppedCanvas().toDataURL('image/jpeg'));
  }

  refresh = e => {
    e.preventDefault();
    this.cropper.current.reset();
  };

  zoomIn = e => {
    e.preventDefault();
    this.cropper.current.zoom(0.1);
  };

  zoomOut = e => {
    e.preventDefault();
    this.cropper.current.zoom(-0.1);
  };

  rotateLeft = e => {
    e.preventDefault();
    this.cropper.current.rotate(15);
  };

  rotateRight = e => {
    e.preventDefault();
    this.cropper.current.rotate(-15);
  };

  uploadBtnToRender() {
    return (
      <label htmlFor="input-upload" style={{ margin: '0' }} >
        <i className="zmdi zmdi-cloud-upload" />
        <input id="input-upload" type="file" onChange={this.uploadFile.bind(this)} style={{ display: 'none' }} />
      </label>
    );
  }

  uploadFile(e) {
    e.preventDefault();
    let files;
    if (e.dataTransfer) {
      files = e.dataTransfer.files;
    } else if (e.target) {
      files = e.target.files;
    }
    const reader = new window.FileReader();
    reader.onload = () => {
      this.setState({ src: reader.result });
    };
    reader.readAsDataURL(files[0]);
  }

  controlsToRender(controls) {
    return controls.map((c, index) => {
      const { className, color, size, content, tooltipContent, onClick } = c;
      return (
        <CustomTooltipBtn
          key={ index }
          id={ index }
          className={ className }
          content={ content }
          tooltipContent={ tooltipContent }
          size={ size }
          color={ color }
          onClick={ onClick }/>
      );
    });
  }

  render() {
    const { state, crop, cropper, CONTROLS, controlsToRender } = this;
    const { style, src } = state;

    return (
      <React.Fragment>
        <div className="custom-cropper">
          <Row className="mb-3">
            <Col sm="12" md="9">
              <Cropper
                ref={ cropper }
                src={ src }
                style={ style }
                aspectRatio={ 1 }
                crop={ crop }
                preview={ '.img-preview' }/>
            </Col>
            <Col sm="12" md="3">
              <div className="preview text-center mb-3">
                <div>
                  <small>100x100 size</small>
                  <div className="img-preview s100" />
                </div>
              </div>
              <div className="preview text-center mb-3">
                <div>
                  <small>60x60 size</small>
                  <div className="img-preview s60" />
                </div>
              </div>
              <div className="preview text-center mb-3">
                <div>
                  <small>30x30 size</small>
                  <div className="img-preview s30" />
                </div>
              </div>
            </Col>
          </Row>
          <Row>
            <Col sm="12" md="9" className="text-center">
              { controlsToRender(CONTROLS) }
            </Col>
          </Row>
        </div>
      </React.Fragment>
    );
  }
}

export default CustomCropper;