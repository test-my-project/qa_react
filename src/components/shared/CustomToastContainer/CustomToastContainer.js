import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { ToastContainer } from 'react-toastify';
import { general } from './../../../config';

const CloseButton = ({ closeToast }) => (
  <i
    className="zmdi zmdi-close"
    onClick={ closeToast }
  />
);

class CustomToastContainer extends Component {
  render() {
    const { autoClose } = this.props;

    return <ToastContainer
      autoClose   = { autoClose }
      closeButton = { <CloseButton /> }
    />;
  }
}

const propTypes = {
  autoClose: PropTypes.number,
};

const defaultProps = {
  autoClose: general.NOTIFICATION.DELAY
};

CustomToastContainer.propTypes = propTypes;
CustomToastContainer.defaultProps = defaultProps;

export default CustomToastContainer;