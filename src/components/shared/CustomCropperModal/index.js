import React, { Component } from 'react';
import {
  CustomCropper,
  CustomModal
} from './../';

class CustomCropperModal extends Component {
  constructor(props){
    super(props);

    this.state = {
      title: 'Cropper avatar',
      className: 'modal-lg',
      acceptBtn: 'Apply changes',
      cancelBtn: 'Cancel'
    };
  }

  render(){
    const {
      toggle,
      state,
      acceptAction,
      getImageFromCropper
    } = this.props;

    const {
      title,
      acceptBtn,
      cancelBtn,
      className
    } = this.state;

    return (
      <CustomModal
        title={ title }
        acceptAction={ acceptAction }
        state={ state }
        toggle={ toggle }
        className={ className }
        body={ <CustomCropper crop={ getImageFromCropper }/> }
        acceptBtn={ acceptBtn }
        cancelBtn={ cancelBtn } />
    );
  }
}

export default CustomCropperModal;