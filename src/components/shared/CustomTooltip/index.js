import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Tooltip } from 'reactstrap';

class CustomTooltip extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tooltipOpen: false,
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen,
    });
  }

  render() {
    const { toggle, state, props } = this;
    const { id, content, tooltipContent, placement } = props;

    return (
      <React.Fragment>
        <span id={ 'Tooltip-' + id }>
          { content }
        </span>
        <Tooltip
          className = "animated fade"
          placement = { placement }
          isOpen    = { state.tooltipOpen }
          target    = { 'Tooltip-' + id }
          toggle    = { toggle }
        >
          { tooltipContent }
        </Tooltip>
      </React.Fragment>
    );
  }
}

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {
  placement: 'auto'
};

CustomTooltip.propTypes = propTypes;
CustomTooltip.defaultProps = defaultProps;

export default CustomTooltip;