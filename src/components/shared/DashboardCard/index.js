import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import { Col } from 'reactstrap';
import {
  CustomDropdownButton,
  CustomTooltip
} from './../../../components/shared';

import { pageRoutes } from './../../../config';

class DashboardCard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isDropdownOpen: false,
      dropdownOpen: false
    };

    this.dropdownToggle = this.dropdownToggle.bind(this);
  }

  dropdownToggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
      isDropdownOpen: !this.state.isDropdownOpen
    });
  }

  render() {
    const { dropdownToggle } = this;
    const { data } = this.props;
    const { isDropdownOpen, dropdownOpen } = this.state;

    const {
      name,
      id,
      hex,
      description,
      avatar,
      issues,
      inProgress
    } = data;

    return (
      <Col
        xs="6" sm="6" lg="3"
        className = { isDropdownOpen ? 'with-dropdown project-card' : 'project-card' }
      >
        <div className="brand-card">
          <div className="brand-card-header">
            <div className="project-card-header text-value" style={{ backgroundColor: hex }}>
              { name.charAt(0) }
              { avatar && <img src={ avatar } alt={ name } /> }
            </div>
            <div>
              <div className="project-card-title text-value">
                <Link to={ `${ pageRoutes.projects.frame }/${ id }` }>
                  { name }
                </Link>
              </div>
              <div className="project-card-description text-muted">
                { description }
              </div>
            </div>
          </div>
          <div className="brand-card-body">
            <div>
              <div className="text-value">
                { issues }
              </div>
              <div className="text-uppercase text-muted small">issues</div>
            </div>
            <div>
              <div className="text-value">
                { inProgress }
              </div>
              <div className="text-uppercase text-muted small">in progress</div>
            </div>
            <div className="controls">
              <CustomDropdownButton
                customToggle = { dropdownToggle }
                customOpen = { dropdownOpen }
                header = { `${ name } project` }
                content = {
                  <Link to={ `${ pageRoutes.projects.edit }/${ id }` }>
                    { 'Edit' }
                  </Link>
                }
                buttonContent = {
                  <CustomTooltip
                    content        = { <i className="icon-options" /> }
                    id             = { `member-${ id }` }
                    tooltipContent = { 'Settings' }
                  />
                }
                className = "btn-pill btn-sm"
              />
            </div>
          </div>
        </div>
      </Col>
    );
  }
}

const propTypes = {
  data: PropTypes.object,
};

const defaultProps = {
  data: {
    name: 'Project name',
    issues: 11,
    inProgress: 1,
    id: 123,
    hex: '#dadada',
    description: 'Test description',
    avatar: null
  },
};

DashboardCard.propTypes = propTypes;
DashboardCard.defaultProps = defaultProps;

export default DashboardCard;