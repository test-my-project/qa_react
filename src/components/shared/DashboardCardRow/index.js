import React, { Component } from 'react';
import { Row } from 'reactstrap';

import {
  DashboardCard,
  DashboardAddProject,
  DashboardCardRowEmpty,
  Preloader
} from './../';

class DashboardCardRow extends Component {
  constructor(props) {
    super(props);

    this.cardsToRender = this.cardsToRender.bind(this);
  }

  cardsToRender(arr) {
    return arr.map((item, index) => {
      return item ? <DashboardCard key={ index } data={ item } /> : <DashboardCard key={ index } />;
    });
  }

  render() {
    const { cardsToRender, props } = this;
    const { projects } = props;

    if (typeof(projects) === 'undefined') return <Preloader />;

    return (
      <React.Fragment>
        <h2 className="app-title">Dashboard</h2>
        { projects.length === 0
          ? <DashboardCardRowEmpty />
          :
          <Row>
            { cardsToRender(projects) }
            <DashboardAddProject />
          </Row>
        }
      </React.Fragment>
    );
  }
}

export default DashboardCardRow;