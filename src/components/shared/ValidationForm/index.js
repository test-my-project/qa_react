import React, { Component } from 'react';
import { AvForm } from 'availity-reactstrap-validation';

import {
  ValidationInput,
  CustomSpinnerButton
} from './../../../components/shared';

class ValidationForm extends Component {
  fieldValuesGenerator(obj, validate, state) {
    for (const field of obj) {
      field.value = state[field.name];
      field.validate = validate;
    }
    return obj;
  }

  fieldsToRender(arr, onInputChange) {
    return arr.map((f, index) => {
      const { name, type, value, validateOptions, placeholder, helpMessage } = f;

      return (
        <ValidationInput
          key          = { `${name}-${index}` }
          name         = { name }
          type         = { type }
          value        = { value }
          validate     = { validateOptions }
          placeholder  = { placeholder }
          helpMessage  = { helpMessage }
          onChange     = { (v, e) => onInputChange(v, e) }
        />
      );
    });
  }

  onValidate = (e, errors) => {
    if (errors && errors.length === 0) {
      this.props.submitHandler();
    }
  };

  render() {
    const { fieldValuesGenerator, fieldsToRender, onValidate } = this;
    const { state, onInputChange, data, localization, isRequestLoading } = this.props;

    const { title, text, button } = localization;
    const { validate } = state;

    const fieldsObj = fieldValuesGenerator(data, validate, state);

    return (
      <AvForm
        onSubmit  = { onValidate }
        className = "custom-form"
      >
        <h1 className="text-primary">{ title }</h1>
        <p className="text-muted">{ text }</p>
        { fieldsToRender(fieldsObj, onInputChange) }
        <CustomSpinnerButton
          isRequestLoading={ isRequestLoading }
          text={ button }
        />
      </AvForm>
    );
  }
}

export default ValidationForm;