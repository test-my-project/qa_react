import React, { Component } from 'react';

import {
  Button,
  NavLink
} from 'reactstrap';
import { Link } from 'react-router-dom';
import { pageRoutes } from './../../../config';

import { ReactComponent as DefaultListSvg } from './../../../assets/svg/issuesListDefault.svg';
import { ReactComponent as IssuesListNotFoundSVG } from './../../../assets/svg/issuesListNotFound.svg';

class IssuesListCap extends Component {
  render() {
    return (
      <div className="animated fadeIn default-list">
        <div className="content">
          <DefaultListSvg
            className = "default svg-icon"
            alt       = "default-icon"
          />
          <p>Nothing to show, please choose the project.</p>
        </div>
      </div>
    );
  }
}

class IssuesListNoFound extends Component {
  render() {
    const { id } = this.props;

    return (
      <div className="animated fadeIn default-list">
        <div className="content">
          <IssuesListNotFoundSVG
            className = "default svg-icon"
            alt       = "default-icon"
          />
          <p>
            You don`t have any issues in this project.
            <NavLink tag={ Link } to={ `${ pageRoutes.projects.frame }/${ id }` }>
              <Button block color="primary">Create a new one</Button>
            </NavLink>
          </p>
        </div>
      </div>
    );
  }
}

export {
  IssuesListNoFound,
  IssuesListCap
};