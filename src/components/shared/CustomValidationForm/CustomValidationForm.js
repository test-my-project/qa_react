import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  AvForm,
  AvField
} from 'availity-reactstrap-validation';

import {
  Button
} from 'reactstrap';

class CustomValidationForm extends Component {
  constructor(props) {
    super(props);

    this.state = {};

    this.handleSubmit   = this.handleSubmit.bind(this);
    this.fieldsToRender = this.fieldsToRender.bind(this);
  }

  handleSubmit(event, errors, values) {
    this.setState({ errors, values });
  }

  fieldsToRender(data){
    return Object.keys(data).map((d, key) => {
      const {
        type,
        name,
        label,
        helpMessage,
        validate,
        placeholder,
        value
      } = data[key];

      return (
        <AvField
          key         = { key }
          type        = { type }
          placeholder = { placeholder }
          name        = { name }
          id          = { `${ name }+${ key }` }
          value       = { value }
          label       = { label }
          helpMessage = { helpMessage }
          validate    = { validate }
        />
      );
    });
  }

  render() {
    const {
      handleSubmit,
      fieldsToRender,
      props
    } = this;
    const { data } = props;

    if (!data) return (
      <React.Fragment>
        There is no data.
      </React.Fragment>
    );

    return (
      <React.Fragment>
        <AvForm onSubmit={ handleSubmit } className="custom-form">
          { fieldsToRender(data) }
          <Button color="primary">Create</Button>
        </AvForm>
      </React.Fragment>
    );
  }
}

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

CustomValidationForm.propTypes = propTypes;
CustomValidationForm.defaultProps = defaultProps;

export default CustomValidationForm;