import React, { Component } from 'react';

import { general } from './../../../config';
import loader from './../../../assets/img/brand/copper-loader.gif';

class SiteLoader extends Component {

  state = {
    className: 'site-loader__wrap animated hinge'
  };

  componentDidMount() {
    this._isMounted = true;
    this.hideLoader();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  hideLoader = () => {
    setTimeout(() => {
      // setState on unmounted component has no logic
      // so just checking if component mounted first
      this._isMounted && this.setState({ className: 'site-loader__wrap animated hinge fade' });
    }, general.SITE_LOADER.DELAY);
  }

  render() {
    const { className } = this.state;

    return (
      <>
        <div className={ className }>
          <img className="site-loader" src={ loader } alt="Site loader" />
        </div>
      </>
    );
  }
}

export default SiteLoader;