import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';

import { Alert } from 'reactstrap';
import { CustomModal } from './../';

import WrappedComponent from './../../../containers/View/WrappedComponent';

class CustomConfirmModal extends WrappedComponent {
  constructor(props) {
    super(props);

    this.state = {
      ...this.state,
      validate: true
    };

    this.onAcceptAction = this.onAcceptAction.bind(this);
    this.bodyToRender   = this.bodyToRender.bind(this);
  }

  onAcceptAction = () => {
    const { onRequestStarted, props } = this;
    const { acceptAction } = props;

    onRequestStarted();
    acceptAction();
  };

  bodyToRender = opts => {
    const { hasChilds, question } = opts;

    if (!hasChilds) return (
      <React.Fragment>
        { question }
      </React.Fragment>
    );

    const { childNames } = opts;

    return (
      <React.Fragment>
        <Alert color="danger">
          This project contain next sub-projects:
          <div>
            <strong>{ _.join(childNames, ', ') }</strong>
          </div>
        </Alert>
        { question }
      </React.Fragment>
    );
  };

  render(){
    const { props, onAcceptAction, bodyToRender } = this;
    const { toggle, state, title, acceptBtn, cancelBtn, question, className, project } = props;
    const { validate, isRequestLoading } = this.state;

    return (
      <CustomModal
        title            = { title }
        acceptAction     = { onAcceptAction }
        state            = { state }
        toggle           = { toggle }
        className        = { className }
        body             = { bodyToRender({ question, ...project }) }
        acceptBtn        = { acceptBtn }
        cancelBtn        = { cancelBtn }
        validate         = { validate }
        isRequestLoading = { isRequestLoading }
      />
    );
  }
}

const propTypes = {
  title: PropTypes.string,
  className: PropTypes.string,
  question: PropTypes.any,
  acceptBtn: PropTypes.string,
  cancelBtn: PropTypes.string,
  validate: PropTypes.bool
};

const defaultProps = {
  title: 'Confirm window',
  className: '',
  question: 'Are you sure?',
  acceptBtn: 'DELETE',
  cancelBtn: 'Cancel',
  validate: true
};

CustomConfirmModal.propTypes = propTypes;
CustomConfirmModal.defaultProps = defaultProps;

export default CustomConfirmModal;