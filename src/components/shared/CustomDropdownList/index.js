import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { DropdownList } from 'react-widgets';

class CustomDropdownList extends Component {
  render(){
    const {
      data,
      placeholder,
      onChange,
      className,
      filter,
      disabled,
      messages
    } = this.props;

    return (
      <DropdownList
        disabled     = { disabled }
        filter       = { filter }
        data         = { data }
        defaultValue = { placeholder }
        onChange     = { onChange }
        className    = { className }
        messages     = { messages }
      />
    );
  }
}

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {
  data        : ['Must be changed'],
  placeholder : 'Default placeholder',
  className   : '',
  disabled    : false,
  filter      : false,
  messages    : {}
};

CustomDropdownList.propTypes = propTypes;
CustomDropdownList.defaultProps = defaultProps;

export default CustomDropdownList;