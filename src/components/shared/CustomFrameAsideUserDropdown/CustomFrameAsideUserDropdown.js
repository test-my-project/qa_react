import React from 'react';
import PropTypes from 'prop-types';

import { FormGroup } from 'reactstrap';

import WrappedComponent from './../../../containers/View/WrappedComponent';
import {
  CustomAssignUserDropdown
} from './../';

class CustomFrameAsideUserDropdown extends WrappedComponent {
  render(){
    const { data, onChange } = this.props;

    return (
      <FormGroup>
        <label>Assign to</label>
        <CustomAssignUserDropdown
          data = { data }
          onChange = { onChange }
        />
        <small className="text-muted form-text">Assign user</small>
      </FormGroup>
    );
  }
}

const propTypes = {
  data: PropTypes.object,
  onChange: PropTypes.func,
};

const defaultProps = {
  data: ['Must be changed'],

  //eslint-disable-next-line no-restricted-syntax
  onChange: () => { console.warn('CustomFrameAsideUserDropdown should be changed!'); }
};

CustomFrameAsideUserDropdown.propTypes = propTypes;
CustomFrameAsideUserDropdown.defaultProps = defaultProps;

export default CustomFrameAsideUserDropdown;