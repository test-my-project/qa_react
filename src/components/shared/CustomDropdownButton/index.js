import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  ButtonDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from 'reactstrap';

class CustomDropdownButton extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dropdownOpen: false
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  render() {
    const {
      content,
      buttonContent,
      className,
      customToggle,
      header,
      customOpen
    } = this.props;

    const { dropdownOpen } = this.state;
    const { toggle } = this;

    return (
      <ButtonDropdown
        isOpen = { customOpen ? customOpen : dropdownOpen }
        toggle = { customToggle ? customToggle : toggle }
        group  = { false }
        direction = { 'left' }
      >
        <DropdownToggle
          className = { className }
          color = { 'dark' }
        >
          { buttonContent }
        </DropdownToggle>
        <DropdownMenu>
          <DropdownItem header>
            { header }
          </DropdownItem>
          <DropdownItem header>
            { content }
          </DropdownItem>
        </DropdownMenu>
      </ButtonDropdown>
    );
  }
}

const propTypes = {
  header: PropTypes.string,
  content: PropTypes.object,
  buttonContent: PropTypes.object,
  className: PropTypes.string,
};

const defaultProps = {
  header: 'No header',
  content: 'No content',
  buttonContent: 'No button content',
  className: '',
};

CustomDropdownButton.propTypes = propTypes;
CustomDropdownButton.defaultProps = defaultProps;

export default CustomDropdownButton;