import React, { Component } from 'react';
import { Button } from 'reactstrap';
import PropTypes from 'prop-types';

class CustomSpinnerButton extends Component {
  render(){
    const { isRequestLoading, text, clickType, color } = this.props;

    const className = isRequestLoading ? 'loading' : 'loaded';
    const spinner = isRequestLoading && <i className="zmdi zmdi-spinner zmdi-hc-spin" />;

    return (
      <div className="position-relative">
        <Button
          className = { className }
          color={ color }
          onClick={ clickType }
          block
        >
          { text }
        </Button>
        { spinner }
      </div>
    );
  }
}

const propTypes = {
  isRequestLoading: PropTypes.bool,
  className: PropTypes.string,
  text: PropTypes.string,
  color: PropTypes.string,
};

const defaultProps = {
  isRequestLoading: false,
  className: 'loaded',
  text: 'Submit',
  color: 'primary'
};

CustomSpinnerButton.propTypes = propTypes;
CustomSpinnerButton.defaultProps = defaultProps;

export default CustomSpinnerButton;