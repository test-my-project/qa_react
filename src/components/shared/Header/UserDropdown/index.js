import React, { Component } from 'react';

import {
  Badge,
  DropdownItem,
  DropdownMenu,
  DropdownToggle
} from 'reactstrap';

import {
  AppHeaderDropdown
} from '@coreui/react';

class UserDropdown extends Component {
  render() {
    const { onLogout, user } = this.props;
    const data = user.getUserData();
    const { display_name: name, email } = data;

    return (
      <AppHeaderDropdown direction="down" className="user-dropdown">
        <DropdownToggle nav>
          <div className="letter-avatar">
            { user.getFirstLetter() }
          </div>
        </DropdownToggle>
        <DropdownMenu right style={{ left: 'auto' }}>
          <DropdownItem header tag="div">
            Hi, <strong>{ name }</strong>!
            <br />
            <small>{ email }</small>
          </DropdownItem>
          <DropdownItem><i className="fa fa-user" /> Profile</DropdownItem>
          <DropdownItem><i className="fa fa-tasks" /> Issues<Badge color="danger">42</Badge></DropdownItem>
          <DropdownItem><i className="fa fa-comments" /> Comments<Badge color="warning">2</Badge></DropdownItem>
          <DropdownItem divider />
          <DropdownItem onClick={ onLogout }><i className="fa fa-lock" /> Logout</DropdownItem>
        </DropdownMenu>
      </AppHeaderDropdown>
    );
  }
}

export default UserDropdown;