import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import {
  Nav,
  NavItem,
  NavLink
} from 'reactstrap';

import { UserDropdown } from './../../';
import { pageRoutes } from './../../../../config';

class Navigation extends Component {
  render() {
    const { onLogout, user } = this.props;
    const { isLoggedIn } = user;

    return (
      <React.Fragment>
        <Nav className="d-md-down-none" navbar>
          <NavItem className="px-3">
            <NavLink tag={ Link } to={ pageRoutes.projects.main }>Projects</NavLink>
          </NavItem>
          <NavItem className="px-3">
            <NavLink tag={ Link } to={ pageRoutes.issues.main }>Issues</NavLink>
          </NavItem>
        </Nav>
        <Nav className="ml-auto" navbar>
          { isLoggedIn && <UserDropdown onLogout={onLogout} user={user}/> }
        </Nav>
      </React.Fragment>
    );
  }
}

export default Navigation;