import unionBy from 'lodash/unionBy';
import React, { Component } from 'react';

import Board from 'react-trello';

import {
  CustomTooltip,
  Preloader,
  IssuesListNoFound
} from './../';

import { trelloBoardDataGenerator } from './../../../helpers';
import { prioritiesIcons } from './../../../config';

class CardIcon extends Component {
  render() {
    const { content, withUser } = this.props;
    const className = withUser ? 'react-trello-card-icon with-user' : 'react-trello-card-icon';

    return (
      <span className={ className }>
        { content }
      </span>
    );
  }
}

class AssignUserIcon extends Component {
  render() {
    const { user, id } = this.props;

    const isUserNotExist = !user || user === 1;
    const iconContent = isUserNotExist ? <i className={'icons icon-user'} /> : user.display_name.charAt(0);
    const tooltipContent = isUserNotExist ? 'Unassigned' : user.email;

    return (
      <CustomTooltip
        content        = { <CardIcon
          content  = { iconContent }
          withUser = { !isUserNotExist }
        /> }
        id             = { `assign-${id}` }
        tooltipContent = { tooltipContent }
        placemnet      = { 'left' }
      />
    );
  }
}

class PriorityIcon extends Component {
  render() {
    const { id, label, cardId } = this.props;
    const priorityType = prioritiesIcons[id - 1];

    return <CustomTooltip
      content        = {<CardIcon
        content  = { <i className={`icons cui-${ priorityType[0] } text-${ priorityType[1] }`} /> }
        withUser = { false }
      /> }
      id             = { cardId }
      tooltipContent = { label }
      placement      = { 'left' }
    />;
  }
}

class CustomCard extends Component {
  render() {
    const {
      title,
      description,
      label,
      priorityId,
      id,
      assignedUser
    } = this.props;

    return (
      <div className="react-trello-card">
        <header>
          <span className="title">{ title }</span>
          <span className="label">
            <AssignUserIcon
              user = { assignedUser }
              id   = { id }
            />
            <PriorityIcon
              id     = { priorityId }
              label  = { label }
              cardId = { id }
            />
          </span>
        </header>
        <div>{ description }</div>
      </div>
    );
  }
}

class CustomTrelloBoard extends Component {
  render() {
    const {
      statuses,
      priorities,
      isLoading,
      id,
      onUpdateBoard,
      members,
      isBoardLoading,
      updatedIssue
    } = this.props;

    let { issues } = this.props;

    if (isLoading || !issues || !statuses || !priorities) return <Preloader relative = { true } />;

    if (issues.length === 0) return <IssuesListNoFound id = { id }/>;

    if (updatedIssue) {
      issues = unionBy([updatedIssue], issues, 'id');
    }

    const opts = {
      issues       : [ ...issues ],
      statuses     : [ ...statuses ],
      priorities   : [ ...priorities ]
    };

    if (members && members.length > 0) {
      opts.members = [ ...members ];
    }

    const data = trelloBoardDataGenerator(opts);

    /**
     * Boards component - https://github.com/rcdexta/react-trello/blob/master/UPGRADE.md
     */
    const components = {
      Card: CustomCard
    };

    const layoutClassName = isBoardLoading ? 'mt-3 my-board-layout loading' : 'mt-3 my-board-layout';

    return (
      <div className = { layoutClassName }>
        <Board
          data          = { data }
          draggable     = { true }
          cardDraggable = { true }
          laneDraggable = { false }
          className     = "react-trello-board my-board"
          handleDragEnd = { (cardId, sourceLaneId, targetLaneId, position, cardDetails) => onUpdateBoard(cardId, cardDetails) }
          components    = { components }
        />
      </div>
    );
  }
}

//TODO
const propTypes = {};

const defaultProps = {};

CustomTrelloBoard.propTypes = propTypes;
CustomTrelloBoard.defaultProps = defaultProps;

export default CustomTrelloBoard;