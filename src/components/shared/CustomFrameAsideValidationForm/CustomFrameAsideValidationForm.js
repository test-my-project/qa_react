import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  AvForm
} from 'availity-reactstrap-validation';

import {
  FormGroup,
  ListGroupItem,
  ListGroup
} from 'reactstrap';

import {
  CustomDropdownList,
  CustomValidationForm,
  CustomFrameAsideUserDropdown,
  CustomSpinnerButton
} from './../';

const STATUS = 'Status';
const PRIORITY = 'Priority';

class CustomFrameAsideValidationFormWithoutData extends Component {
  render() {
    return(
      <ListGroup className="list-group-accent">
        <ListGroupItem className="list-group-item-accent-warning text-center font-weight-bold text-uppercase small">
          There is no data.
        </ListGroupItem>
      </ListGroup>
    );
  }
}

class CustomFrameAsideValidationForm extends CustomValidationForm {
  constructor(props) {
    super(props);

    this.state = {
      ...this.state,
      status: null,
      priority: null,
      assignTo: null
    };

    this.selectToRender = this.selectToRender.bind(this);
    this.onSelectChange = this.onSelectChange.bind(this);
    this.onAssignMember = this.onAssignMember.bind(this);
  }

  selectToRender(data, opts) {
    return (
      <FormGroup>
        <label>{ opts[1] }</label>
        <CustomDropdownList
          name         = { opts[1] }
          data         = { _.map(data, 'title') }
          placeholder  = { _.find(data, (d) => { return d.isDefaultToSelect; }).title }
          onChange     = { v => this.onSelectChange(v, opts[1]) }
        />
        <small className="text-muted form-text">{ opts[2] }</small>
      </FormGroup>
    );
  }

  onSelectChange(value, name) {
    const { statuses, priorities } = this.props;
    const res = _.find(name === STATUS ? statuses : priorities, [ 'title', value ]);
    this.setState({ [name.toLowerCase()]: { ...res } });
  }

  onAssignMember(value) {
    const { members } = this.props;
    const id = _.find(members, m => (m.name === value)).id;

    this.setState({ assignTo: id });
  }

  handleSubmit(event, errors, values) {
    if (errors.length === 0) {
      this.props.onRequestStarted();
      this.setState({ values });
      this.props.collectFormData(this.state);
    }
  }

  render() {
    const {
      handleSubmit,
      fieldsToRender,
      selectToRender,
      onAssignMember,
      props
    } = this;
    const { data, statuses, priorities, isRequestLoading, members } = props;

    if (!data) return <CustomFrameAsideValidationFormWithoutData />;

    return (
      <React.Fragment>
        <AvForm onSubmit={ handleSubmit } className="custom-form">
          <CustomFrameAsideUserDropdown
            data     = { _.map(members, m => { return m.name;} ) }
            onChange = { onAssignMember }
          />
          { fieldsToRender(data) }
          { statuses && selectToRender(statuses, ['Choose issue status', STATUS, 'Select the status of the issue']) }
          { priorities && selectToRender(priorities, ['Choose issue priority', PRIORITY, 'Select the priority of the issue']) }
          <CustomSpinnerButton
            color            = "primary"
            text             = "Create"
            isRequestLoading = { isRequestLoading }
          />
        </AvForm>
      </React.Fragment>
    );
  }
}

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

CustomFrameAsideValidationForm.propTypes = propTypes;
CustomFrameAsideValidationForm.defaultProps = defaultProps;

export default CustomFrameAsideValidationForm;