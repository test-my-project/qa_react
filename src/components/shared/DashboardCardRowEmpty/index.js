import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import {
  Col,
  Row
} from 'reactstrap';
import { pageRoutes } from './../../../config';

class DashboardCardRowEmpty extends Component {
  render() {
    return (
      <Row>
        <Col xs="6" sm="6" lg="3" className="project-card empty">
          You don`t have any projects <Link to={ pageRoutes.projects.add } color="primary">create a new one</Link>
        </Col>
      </Row>
    );
  }
}

export default DashboardCardRowEmpty;