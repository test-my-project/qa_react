import get from 'lodash/get';
import React, { Component } from 'react';

import {
  CustomPoint,
  Preloader
} from './../';

import { FrameHelper as f } from './../../../services';
import { compareHeight } from './../../../helpers';

class CustomFrame extends Component {
  render(){
    const { props } = this;
    const {
      url,
      id,
      parentState,
      isLoading,
      issue,
      initFrame
    } = props;
    const {
      width,
      height,
      scale
    } = parentState;

    const src = f.generateFrameSrc(url);
    const translateY = f.getGenericStyles(height, scale);
    const wrapperClassName = isLoading ? 'animated fadeIn f__wrapper d-none' : 'animated fadeIn f__wrapper';

    if (issue) {
      f.enableFrameScroll();
    } else {
      f.disableFrameScroll();
    }

    return (
      <React.Fragment>
        { isLoading && <Preloader /> }
        <div
          className = { wrapperClassName }
          style     = {{
            height: compareHeight(height),
            width: width,
            transform: `translateY(${translateY}px) scale(${scale})`
          }}
        >
          <iframe
            id     = { id }
            title  = { `project-frame-${ id }` }
            src    = { src }
            width  = { width }
            onLoad = { initFrame }
            height = { compareHeight(height) }
            style  = {{ transform: `translateY(${translateY}px) scale(${scale})` }}
          />

          <CustomPoint
            data = { get(issue, 'pointer') }
          />
        </div>
      </React.Fragment>
    );
  }
}

export default CustomFrame;