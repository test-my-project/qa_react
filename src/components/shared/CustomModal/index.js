import React, { Component } from 'react';

import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter
} from 'reactstrap';

import { CustomSpinnerButton } from './../';

class CustomModal extends Component {
  render(){
    const {
      toggle,
      state,
      title,
      acceptBtn,
      acceptAction,
      cancelBtn,
      body,
      validate,
      className,
      isRequestLoading
    } = this.props;

    return (
      <Modal
        isOpen={ state }
        toggle={ toggle }
        className={ className }>
        <ModalHeader
          toggle={ toggle }>
          { title }
        </ModalHeader>
        <ModalBody className="text-left">
          { body }
        </ModalBody>
        <ModalFooter>
          { validate
            ? <CustomSpinnerButton
              color            = "danger"
              text             = { acceptBtn }
              clickType        = { acceptAction }
              isRequestLoading = { isRequestLoading }
            />
            : <Button color="primary" onClick={ acceptAction ? acceptAction : toggle }>{ acceptBtn }</Button> }
          {' '}
          <Button color={ validate ? 'primary' : 'danger' } onClick={ toggle }>{ cancelBtn }</Button>
        </ModalFooter>
      </Modal>
    );
  }
}

export default CustomModal;