import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  Button,
  Tooltip
} from 'reactstrap';

class CustomTooltipBtn extends Component {
  constructor(props) {
    super(props);

    this.state = {
      tooltipOpen: false,
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      tooltipOpen: !this.state.tooltipOpen,
    });
  }

  render() {
    const { toggle, state, props } = this;
    const { className, id, color, size, content, tooltipContent, onClick, disable } = props;

    return (
      <React.Fragment>
        <Button
          className = { className }
          color     = { color }
          id        = { 'Tooltip-' + id }
          size      = { size }
          onClick   = { onClick }
          disabled  = { disable }
        >
          { content }
        </Button>
        <Tooltip
          className = "animated fade"
          placement = "auto"
          isOpen    = { state.tooltipOpen }
          target    = { 'Tooltip-' + id }
          toggle    = { toggle }
        >
          { tooltipContent }
        </Tooltip>
      </React.Fragment>
    );
  }
}

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {
  color    : '',
  disabled : false
};

CustomTooltipBtn.propTypes = propTypes;
CustomTooltipBtn.defaultProps = defaultProps;

export default CustomTooltipBtn;