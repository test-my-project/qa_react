import React, { Component } from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultFooter extends Component {
  render() {
    return (
      <React.Fragment>
        <span className="text-muted"><a href="/#/dashboard">TestMyProject</a> &copy; 2018 TestMyTeam.</span>
        <span className="text-muted ml-auto">Powered by <a href="/#/dashboard">TestMyTeam</a></span>
      </React.Fragment>
    );
  }
}

DefaultFooter.propTypes = propTypes;
DefaultFooter.defaultProps = defaultProps;

export default DefaultFooter;