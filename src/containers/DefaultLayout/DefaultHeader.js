import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import {
  AppAsideToggler,
  AppNavbarBrand,
  AppSidebarToggler
} from '@coreui/react';

import { Navigation, SiteLoader } from './../../components/shared';
import { User } from './../../services';
import { logout } from './../../redux/actions';

import logo from '../../assets/img/brand/logo.svg';
import sygnet from '../../assets/img/brand/sygnet.svg';

class DefaultHeader extends Component {
  constructor(props) {
    super(props);

    this.logoutHandler = this.logoutHandler.bind(this);
  }

  logoutHandler() {
    this.props.onLogout();
  }

  render() {
    const { logoutHandler } = this;

    return (
      <React.Fragment>
        <SiteLoader />

        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          href='/#/dashboard'
          full={{ src: logo, width: 89, height: 25, alt: 'CoreUI Logo' }}
          minimized={{ src: sygnet, width: 30, height: 30, alt: 'CoreUI Logo' }}
        />
        <AppSidebarToggler className="d-md-down-none" display="lg" />
        <Navigation
          onLogout={logoutHandler}
          user={User}/>
        <AppAsideToggler className="d-md-down-none" defaultOpen={true}/>
        <AppAsideToggler className="d-lg-none" mobile />
      </React.Fragment>
    );
  }
}

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

const mapStateToProps = state => {
  return {
    user: state.authReducer.userData
  };
};

const mapDispatchToProps = dispatch => ({
  onLogout: () => {
    dispatch(logout);
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DefaultHeader);