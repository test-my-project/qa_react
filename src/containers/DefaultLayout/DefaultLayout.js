import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect, Route, Switch } from 'react-router-dom';
import { Container, Fade } from 'reactstrap';
import { getNavigationProjects } from './../../redux/actions';

import {
  AppAside,
  AppBreadcrumb,
  AppFooter,
  AppHeader,
  AppSidebar,
  AppSidebarFooter,
  AppSidebarForm,
  AppSidebarHeader,
  AppSidebarMinimizer,
  AppSidebarNav,
} from '@coreui/react';

import { CustomToastContainer } from './../../components/shared';

import DefaultAside from './DefaultAside';
import DefaultFooter from './DefaultFooter';
import DefaultHeader from './DefaultHeader';

import { routes } from './../../routes';
import { User, transformers as t } from './../../services';
import { isDashboard } from './../../helpers';

class DefaultLayout extends Component {
  constructor(props) {
    super(props);

    this.updateComponent = this.updateComponent.bind(this);
  }

  componentDidMount() {

    /**
     * Get "My projects url navigation"
     */
    this.updateComponent();
    if (User.checkFirstLoad()) {
      User.welcomeNotify();
    }
  }

  updateComponent() {
    const body = document.getElementsByTagName('body')[0];

    /**
     * Clearing the list after the transition from the project page (frame)
     */
    body.className = body.className.replace('f__main-wrap ', '');
    this.props.onGetNavigationProjects();
  }

  render() {
    const { props, updateComponent } = this;
    const { projects } = props;
    const transformArr = [
      ['Projects', projects],
      ['Issues',  null]
    ];

    const nav = t.navigationTransformer(transformArr);

    return (
      <div className="app">
        <AppHeader fixed>
          <DefaultHeader />
        </AppHeader>
        <div className="app-body">
          <AppSidebar fixed display="lg">
            <AppSidebarHeader />
            <AppSidebarForm />
            { nav && <AppSidebarNav navConfig={nav} {...this.props} /> }
            <AppSidebarFooter />
            <AppSidebarMinimizer />
          </AppSidebar>
          <main className="main">
            { !isDashboard() && <AppBreadcrumb appRoutes={routes} /> }
            <Container>
              <Switch>
                { routes.map((route, idx) => {
                  return route.component ? (<Route key={idx} path={route.path} exact={route.exact} name={route.name} render={props => (
                    <route.component {...props} onLayoutUpdate={ updateComponent } />
                  )} />) : (null); },
                )}
                <Redirect from="/" to="/dashboard" />
              </Switch>
            </Container>
          </main>
          <AppAside fixed>
            <Fade timeout={300} in={true}>
              <DefaultAside />
            </Fade>
          </AppAside>
        </div>
        <AppFooter>
          <DefaultFooter />
        </AppFooter>

        <CustomToastContainer />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    projects: state.navigationReducer.projects
  };
};

const mapDispatchToProps = dispatch => ({
  onGetNavigationProjects: () => {
    dispatch(getNavigationProjects);
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DefaultLayout);