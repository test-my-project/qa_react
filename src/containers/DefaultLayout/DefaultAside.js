import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  ListGroup,
  ListGroupItem
} from 'reactstrap';

class DefaultAside extends Component {
  render() {
    return (
      <React.Fragment>
        <ListGroup className="list-group-accent" tag={'div'}>
          <ListGroupItem className="list-group-item-accent-secondary text-center font-weight-bold text-uppercase small">Today</ListGroupItem>
          <ListGroupItem action tag="a" href="#" className="list-group-item-accent-warning list-group-item-divider">
            <div>Meeting with <strong>Lucas</strong> </div>
            <small className="text-muted mr-3">
              <i className="icon-calendar" />&nbsp; 1 - 3pm
            </small>
            <small className="text-muted">
              <i className="icon-location-pin" /> Palo Alto, CA
            </small>
          </ListGroupItem>
          <ListGroupItem action tag="a" href="#" className="list-group-item-accent-info list-group-item-divider">
            <div>Skype with <strong>Megan</strong></div>
            <small className="text-muted mr-3">
              <i className="icon-calendar" />&nbsp; 4 - 5pm
            </small>
            <small className="text-muted">
              <i className="icon-social-skype" /> On-line
            </small>
          </ListGroupItem>
          <ListGroupItem className="list-group-item-accent-secondary text-center font-weight-bold text-uppercase small">Tomorrow</ListGroupItem>
          <ListGroupItem action tag="a" href="#" className="list-group-item-accent-danger list-group-item-divider">
            <div>New UI Project - <strong>deadline</strong></div>
            <small className="text-muted mr-3"><i className="icon-calendar" />&nbsp; 10 - 11pm</small>
            <small className="text-muted"><i className="icon-home" />&nbsp; creativeLabs HQ</small>
          </ListGroupItem>
          <ListGroupItem action tag="a" href="#" className="list-group-item-accent-success list-group-item-divider">
            <div><strong>#10 Startups.Garden</strong> Meetup</div>
            <small className="text-muted mr-3"><i className="icon-calendar" />&nbsp; 1 - 3pm</small>
            <small className="text-muted"><i className="icon-location-pin" />&nbsp; Palo Alto, CA</small>
          </ListGroupItem>
          <ListGroupItem action tag="a" href="#" className="list-group-item-accent-primary list-group-item-divider">
            <div><strong>Team meeting</strong></div>
            <small className="text-muted mr-3"><i className="icon-calendar" />&nbsp; 4 - 6pm</small>
            <small className="text-muted"><i className="icon-home" />&nbsp; creativeLabs HQ</small>
          </ListGroupItem>
        </ListGroup>
      </React.Fragment>
    );
  }
}

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

DefaultAside.propTypes = propTypes;
DefaultAside.defaultProps = defaultProps;

export default DefaultAside;