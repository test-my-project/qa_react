/**
 * Default layout
 */
import DefaultLayout from './DefaultLayout';

/**
 * iFrame main layout
 */
import FrameLayout from './FrameLayout';

/**
 * Layout pages
 */
import Login from './Pages/Login/';
import Page404 from './Pages/Page404/';
import Page500 from './Pages/Page500/';
import Register from './Pages/Register/';

export {
  DefaultLayout,
  FrameLayout,

  Login,
  Page404,
  Page500,
  Register
};