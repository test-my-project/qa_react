import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';


import {
  Card,
  CardBody,
  CardFooter,
  Col,
  Container,
  Row
} from 'reactstrap';

import {
  ValidationForm,
  SiteLoader,
  CustomToastContainer
} from './../../../components/shared';
import WrappedComponent from './../../View/WrappedComponent';

import { registration } from './../../../redux/actions';
import { pageRoutes } from '../../../config';

class Register extends WrappedComponent {
  constructor(props) {
    super(props);
    this.state = {
      ...this.state,
      username: '',
      email: '',
      password: '',
      isLoading: true
    };

    this.localization = {
      title: 'Register',
      text: 'Create your account',
      button: 'Create new Account'
    };

    this.REGISTER_FIELDS = [
      {
        name: 'username',
        type: 'text',
        placeholder: 'Username',
        helpMessage: 'Please provide your username',
        validateOptions: {
          required: {
            value: true,
            errorMessage: 'Username field cannot be empty'
          },
          minLength: {
            value: 8,
            errorMessage: 'Min length 8 symbols'
          },
        }
      },
      {
        name: 'email',
        type: 'email',
        placeholder: 'Email',
        helpMessage: 'Please provide your email, for example awesome@gmail.com',
        validateOptions: {
          required: {
            value: true,
            errorMessage: 'Email field cannot be empty'
          },
          minLength: {
            value: 6,
            errorMessage: 'Min length 6 symbols'
          },
        }
      },
      {
        name: 'password',
        type: 'password',
        placeholder: 'Password',
        helpMessage: 'Please provide your password',
        validateOptions: {
          required: {
            value: true,
            errorMessage: 'Password field cannot be empty'
          },
          minLength: {
            value: 8,
            errorMessage: 'Min length 8 symbols'
          },
        }
      }
    ];

    this.registrationHandler = this.registrationHandler.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
  }

  onInputChange(e, v) {
    this.setState({ [e.target.name]: v });
  }

  registrationHandler() {
    const { username, email, password } = this.state;
    const res = {
      'email': email,
      'display_name': username,
      'password': password
    };

    this.onRequestStarted();
    this.props.onRegistration(res);
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({ isLoading: false });
    }, 2300);
  }

  render() {
    const { state, REGISTER_FIELDS, onInputChange, registrationHandler, localization } = this;
    const { isLoading, isRequestLoading } = state;

    const cardClass = isLoading ? 'mx-4 hovered' : 'mx-4 hovered loaded';

    return (
      <React.Fragment>
        <SiteLoader />

        <div className="app registration flex-row align-items-center">
          <Container>
            <Row className="justify-content-center">
              <Col md="6">
                <Card className={ cardClass }>
                  <CardBody className="p-4">
                    <ValidationForm
                      localization  = { localization }
                      state         = { state }
                      data          = { REGISTER_FIELDS }
                      onInputChange = { onInputChange }
                      submitHandler = { registrationHandler }
                      isRequestLoading = { isRequestLoading }
                    />
                  </CardBody>
                  <CardFooter className="p-4 text-muted text-center">
                    <Row>
                      <Col xs="12">
                        Have an account? You can <Link to={pageRoutes.login}>login</Link>.
                      </Col>
                    </Row>
                  </CardFooter>
                </Card>
              </Col>
            </Row>
          </Container>

          <CustomToastContainer />
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.authReducer.userData
  };
};

const mapDispatchToProps = (dispatch) => ({
  onRegistration: (res, opts) => {
    dispatch(registration(res, opts));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Register);