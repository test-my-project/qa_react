import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import {
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Container,
  Row
} from 'reactstrap';

import {
  ValidationForm,
  SiteLoader,
  CustomToastContainer
} from './../../../components/shared';

import WrappedComponent from './../../View/WrappedComponent';
import { login } from './../../../redux/actions';
import { pageRoutes } from '../../../config';

class Login extends WrappedComponent {
  constructor(props) {
    super(props);

    this.state = {
      ...this.state,
      email: '',
      password: '',
      isLoading: true
    };

    this.localization = {
      title: 'Login',
      text: 'Sign In to your account',
      button: 'Login'
    };

    this.LOGIN_FIELDS = [
      {
        name: 'email',
        type: 'email',
        placeholder: 'Email',
        helpMessage: 'Please provide your email, for example awesome@gmail.com',
        validateOptions: {
          required: {
            value: true,
            errorMessage: 'Email field cannot be empty'
          },
          minLength: {
            value: 6,
            errorMessage: 'Min length 6 symbols'
          },
        }
      },
      {
        name: 'password',
        type: 'password',
        placeholder: 'Password',
        helpMessage: 'Please provide your password',
        validateOptions: {
          required: {
            value: true,
            errorMessage: 'Password field cannot be empty'
          },
          minLength: {
            value: 8,
            errorMessage: 'Min length 8 symbols'
          },
        }
      }
    ];

    this.loginHandler = this.loginHandler.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({ isLoading: false });
    }, 2300);
  }

  onInputChange(e, v) {
    this.setState({ [e.target.name]: v });
  }

  loginHandler() {
    const { email, password } = this.state;
    const { onLogin } = this.props;
    const res = {
      'email': email,
      'password': password
    };

    this.onRequestStarted();
    onLogin(res, { helper: this });
  }

  render() {
    const { state, LOGIN_FIELDS, onInputChange, loginHandler, localization } = this;
    const { isLoading, isRequestLoading } = state;

    const cardClass = isLoading ? 'hovered' : 'hovered loaded';

    return (
      <React.Fragment>
        <SiteLoader />

        <div className="app login flex-row align-items-center">
          <Container>
            <Row className="justify-content-center">
              <Col md="8">
                <CardGroup className={ cardClass }>
                  <Card className="p-4">
                    <CardBody className="p-4">
                      <ValidationForm
                        localization     = { localization }
                        state            = { state }
                        data             = { LOGIN_FIELDS }
                        onInputChange    = { onInputChange }
                        submitHandler    = { loginHandler }
                        isRequestLoading = { isRequestLoading }
                      />
                    </CardBody>
                  </Card>
                  <Card
                    className="py-5 bg-primary">
                    <CardBody className="text-dark text-center">
                      <div>
                        <h2>Sign up</h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut
                          labore et dolore magna aliqua.</p>
                        <Link to={ pageRoutes.register }>
                          <Button
                            color="dark"
                            className="mt-3"
                            outline
                          >
                            Register Now!
                          </Button>
                        </Link>
                      </div>
                    </CardBody>
                  </Card>
                </CardGroup>
              </Col>
            </Row>
          </Container>

          <CustomToastContainer />
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.authReducer.userData
  };
};

const mapDispatchToProps = dispatch => ({
  onLogin: (res, opts) => {
    dispatch(login(res, opts));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);