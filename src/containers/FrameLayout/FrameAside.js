import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  ListGroup,
  ListGroupItem
} from 'reactstrap';
import { CustomFrameAsideTabs } from './../../components/shared';

class FrameAside extends Component {
  render() {
    const {
      pointerData,
      issueData,
      statuses,
      priorities,
      collectFormData,
      onRequestStarted,
      isRequestLoading,
      issue,
      members
    } = this.props;

    if (issue) {
      return (
        <ListGroup className="list-group-accent">
          <ListGroupItem className="list-group-item-accent-primary text-center font-weight-bold text-uppercase">
            Your issues has been created.
          </ListGroupItem>
        </ListGroup>
      );
    }

    if (!pointerData) {
      return (
        <ListGroup className="list-group-accent">
          <ListGroupItem className="list-group-item-accent-warning text-center font-weight-bold text-uppercase small">
            There is no data, please select a point.
          </ListGroupItem>
        </ListGroup>
      );
    }

    const tabsData = [
      {
        title: 'Create an issue',
        data: issueData ? { ...issueData } : null
      },
      {
        title: 'Pointer data',
        data: pointerData ? { ...pointerData } : null
      }
    ];

    return <CustomFrameAsideTabs
      members          = { members }
      data             = { tabsData }
      statuses         = { statuses }
      priorities       = { priorities }
      collectFormData  = { collectFormData }
      isRequestLoading = { isRequestLoading }
      onRequestStarted = { onRequestStarted }
    />;
  }
}

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

FrameAside.propTypes = propTypes;
FrameAside.defaultProps = defaultProps;

export default FrameAside;