import _ from 'lodash';
import React from 'react';
import { connect } from 'react-redux';

import {
  AppAside,
  AppFooter,
  AppHeader
} from '@coreui/react';
import { Fade } from 'reactstrap';

import FrameAside from './FrameAside';
import FrameFooter from './FrameFooter';
import FrameHeader from './FrameHeader';

import WrappedComponent from './../View/WrappedComponent';

import {
  CustomFrame,
  CustomToastContainer,
  SiteLoader
} from './../../components/shared';

import { FrameHelper } from './../../services';
import { frame as f } from './../../config';

import {
  addIssues,
  clearIssuesData,
  getProjectForFrame,
  getProjectFrameStatus,
  getProjectsForFrame,
  getIssuesPrioritiesProjects,
  getIssuesStatusesProjects
} from './../../redux/actions';

class FrameLayout extends WrappedComponent {
  constructor(props) {
    super(props);

    this.state = {
      ...this.state,

      isLoading: true,
      isAsideOpened: false,
      isEventsEnabled: false,
      projectID: this.props.match.params.id,
      frame: {
        width: '100%',
        height: '100vh',
        scale: '1',
        isRotateEnabled: false,
        isCustomEditEnable: false,
        isPointerEnable: false,
        isZoomEnabled: false
      },
      frameConfig: { ...f },
      issue: null,
      issueFormData: null
    };

    this.updateComponent   = this.updateComponent.bind(this);
    this.onSelectDevice    = this.onSelectDevice.bind(this);
    this.onZoomChange      = this.onZoomChange.bind(this);
    this.onRotateFrame     = this.onRotateFrame.bind(this);
    this.onInputChange     = this.onInputChange.bind(this);
    this.onProjectChange   = this.onProjectChange.bind(this);
    this.urlToState        = this.urlToState.bind(this);
    this.initFrame         = this.initFrame.bind(this);
    this.toggleFrameLoader = this.toggleFrameLoader.bind(this);
    this.onEventsEnable    = this.onEventsEnable.bind(this);
    this.onEventsDisable   = this.onEventsDisable.bind(this);
    this.collectFormData   = this.collectFormData.bind(this);
  }

  /**
   * Issue events
   */
  collectFormData(data) {
    this.props.onAddIssue({
      id: this.state.projectID,
      ...data,
      ...this.state.issue.pointer,
    }, { helper: this });
  }

  /**
   * Events for iFrame header tools
   */
  onProjectChange(val) {
    const { projects, onGetProject } = this.props;
    const nextProject = _.find(projects, ['name', val]);
    const { frameUrl, url, name, id } = nextProject;
    window.location.href = frameUrl;

    /**
     * Update iFrame url
     */
    onGetProject(id, { helper: this });
    this.setState(prevState =>  ({
      frame: { ...prevState.frame, url, name },
      projectID: id,
      isLoading: true
    }));
  }

  onInputChange(e) {
    const { value, id } = e.target;
    this.setState(prevState =>  ({ frame: { ...prevState.frame, [id]: value } }));
  }

  onZoomChange(percent) {
    const obj = _.find(f.zoomList, { percent });
    const { scale } = obj;
    this.setState(prevState =>  ({ frame: { ...prevState.frame, scale } }));
  }

  onSelectDevice(name) {
    const obj = _.find(f.deviceList, { name });

    this.setState(prevState => ({ frame: { ...prevState.frame, ...obj } }));
  }

  onRotateFrame() {
    const { width: height, height: width } = this.state.frame;
    this.setState(prevState =>  ({ frame: { ...prevState.frame, width, height } }));
  }

  urlToState(){
    if (this.props.project) {
      const { name, url } = this.props.project;
      if (name && url) {
        this.setState(prevState =>  ({ frame: { ...prevState.frame, url, name } }));
      }
    }
  }

  /**
   * Events for iFrame
   */
  initFrame() {
    const { toggleFrameLoader } = this;

    /**
     * Hide loader
     */
    toggleFrameLoader();
    this.setState(prevState =>  ({
      frame: {
        ...prevState.frame,
        isPointerEnable: true,
        isNativeHeight: true
      }
    }));
  }

  onEventsEnable() {
    const id = this.state.projectID;

    this.setState({ isEventsEnabled: true });
    FrameHelper.enableFrameEvents(id, { helper: this });
  }

  onEventsDisable() {
    const id = this.state.projectID;

    FrameHelper.disableFrameEvents(id);
    this.setState({ isEventsEnabled: false });

    if (this.state.isAsideOpened) {
      document.getElementById('f_aside_toggle').click();
      this.setState(prevState =>  ({
        frame: { ...prevState.frame, isPointerEnable: true },
        isAsideOpened: false,
        issue: null
      }));
      this.props.onClearIssuesData();
    }
  }

  toggleFrameLoader() {
    this.setState({ isLoading: false });
    this.props.onFrameStatusChanged();
  }

  /**
   * React events
   */
  componentDidMount() {
    FrameHelper.setDefaultStyleToWrapper();

    /**
     * Get project data
     */
    this.updateComponent();

    /**
     * Get all projects
     */
    this.props.onGetProjects();
    this.props.onGetStatuses();
    this.props.onGetPriorities();
  }

  updateComponent() {
    const { projectID: id } = this.state;
    this.props.onGetProject(id, { helper: this });
  }

  render() {
    const {
      state,
      props,
      onSelectDevice,
      onEventsEnable,
      onEventsDisable,
      onZoomChange,
      onInputChange,
      collectFormData,
      initFrame,
      onProjectChange,
      onRequestStarted,
      onRotateFrame
    } = this;
    const {
      projectID: id,
      frame,
      frameConfig,
      isLoading,
      isAsideOpened,
      issue,
      isRequestLoading,
      isEventsEnabled
    } = state;
    const {
      project,
      projects,
      statuses,
      issueData,
      priorities
    } = props;

    if (!project || !projects) return <SiteLoader />;

    const { url, members } = project;

    return (
      <div className="app">
        <AppHeader>
          <FrameHeader
            onProjectChange = { onProjectChange }
            onEventsEnable  = { onEventsEnable }
            onEventsDisable = { onEventsDisable }
            onSelectDevice  = { onSelectDevice }
            onZoomChange    = { onZoomChange }
            onRotateFrame   = { onRotateFrame }
            onInputChange   = { onInputChange }
            frameConfig     = { frameConfig }
            parentState     = { frame }
            projectsList    = { projects }
            isAsideOpened   = { isAsideOpened }
            isEventsEnabled = { isEventsEnabled }
          />
        </AppHeader>
        <div className="app-body">
          <main className="main">
            <CustomFrame
              id          = { id }
              url         = { url }
              parentState = { frame }
              initFrame   = { initFrame }
              isLoading   = { isLoading }
              issue       = { issue }
            />
          </main>
          <AppAside
            className = "f__aside"
          >
            <Fade
              timeout = { 300 }
              in      = { true }
            >
              <FrameAside
                members          = { members }
                pointerData      = { _.get(issue, 'pointer') }
                issueData        = { _.get(issue, 'create') }
                statuses         = { statuses }
                priorities       = { priorities }
                issue            = { issueData }
                collectFormData  = { collectFormData }
                isRequestLoading = { isRequestLoading }
                onRequestStarted = { onRequestStarted }
              />
            </Fade>
          </AppAside>
        </div>
        <AppFooter className="d-none">
          <FrameFooter />
        </AppFooter>

        <CustomToastContainer />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    project: state.frameReducer.project,
    projects: state.frameReducer.projects,

    statuses: state.issuesReducer.statuses,
    priorities: state.issuesReducer.priorities,
    issueData: state.issuesReducer.issue,
  };
};

const mapDispatchToProps = dispatch => ({
  onClearIssuesData: () => {
    dispatch(clearIssuesData);
  },
  onGetProjects: () => {
    dispatch(getProjectsForFrame);
  },
  onGetProject: (id, opts) => {
    dispatch(getProjectForFrame(id, opts));
  },
  onFrameStatusChanged: () => {
    dispatch(getProjectFrameStatus);
  },
  onGetStatuses: () => {
    dispatch(getIssuesStatusesProjects);
  },
  onGetPriorities: () => {
    dispatch(getIssuesPrioritiesProjects);
  },
  onAddIssue: (data, opts) => {
    dispatch(addIssues(data, opts));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FrameLayout);