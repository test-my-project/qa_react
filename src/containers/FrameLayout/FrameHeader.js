import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  AppNavbarBrand,
  AppAsideToggler
} from '@coreui/react';

import {
  CustomTooltipBtn,
  FrameNavigation,
  SiteLoader
} from './../../components/shared';

class FrameHeader extends Component {
  render() {
    const {
      onEventsDisable,
      isAsideOpened
    } = this.props;

    return (
      <React.Fragment>
        <SiteLoader />

        <AppNavbarBrand href='/#/dashboard'>
          <i className="zmdi zmdi-home" />
        </AppNavbarBrand>

        <FrameNavigation { ...this.props } />

        { isAsideOpened && <CustomTooltipBtn
          onClick        = { onEventsDisable }
          content        = { <i className="icon-close text-danger" /> }
          id             = "close-aside"
          tooltipContent = "Close"
          size           = "lg"
          className      = "btn-link"
        /> }

        <AppAsideToggler className="d-none" id="f_aside_toggle" defaultOpen={ false } />
      </React.Fragment>
    );
  }
}

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

FrameHeader.propTypes = propTypes;
FrameHeader.defaultProps = defaultProps;

export default FrameHeader;