import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

import {
  Button,
  Col,
  NavLink,
  Row
} from 'reactstrap';

import WrappedComponent from './../WrappedComponent';

import { CustomForm } from './../../../components/shared';
import { addProject, addProjectUpdate } from './../../../redux/actions';
import { pageRoutes, projects } from './../../../config';

/**
 * Local constants variables
 */
const FIELDS       = projects.fields;
const COLOR_PICKER = projects.colorPicker;

const DEFAULT_FORM = {
  colorPicker: { ...COLOR_PICKER },
  avatar: {
    firstLetter: 'p',
    modal      : false,
    img        : null
  },
  fields: {
    [FIELDS.projectType.name]       : FIELDS.projectType.value,
    [FIELDS.workType.name]          : FIELDS.workType.value,
    [FIELDS.projectUrl.name]        : FIELDS.projectUrl.value,
    [FIELDS.projectName.name]       : FIELDS.projectName.value,
    [FIELDS.shortDescription.name]  : FIELDS.shortDescription.value
  }
};

const INITIAL_STATE = {
  forms: [ DEFAULT_FORM ]
};

class ProjectAddFetchSuccess extends WrappedComponent {
  componentDidMount() {
    this.props.onLayoutUpdate();
  }

  render() {
    const { project, onComponentMount } = this.props;
    const { name } = project;

    return (
      <div className="animated fadeIn project-add success text-center">
        <Row>
          <Col sm="12" >
            <h2>Your project - {name}, has been successfully created!</h2>
            <i className="cui-check text-primary icons font-5xl d-block mt-4 mb-3" />
          </Col>
        </Row>
        <Row>
          <Col sm="12" >
            To edit your project go <Link to={ pageRoutes.root }>Link to - {name}</Link>
          </Col>
          <Col sm="12" className="mt-3 mb-3">
            or
          </Col>
        </Row>
        <Row>
          <Col md="3" />
          <Col col="3" sm="6" md="3" className="mb-3">
            <NavLink tag={ Link } to={ pageRoutes.root }>
              <Button block color="primary">Go to dashboard</Button>
            </NavLink>
          </Col>
          <Col col="3" sm="6" md="3" className="mb-3">
            <NavLink tag={ Link } to={ pageRoutes.projects.add }>
              <Button onClick={ onComponentMount } block color="warning">Create a new one</Button>
            </NavLink>
          </Col>
          <Col md="3" />
        </Row>
      </div>
    );
  }
}

class ProjectAdd extends WrappedComponent {
  constructor(props) {
    super(props);

    this.state = {
      ...this.state,
      ...INITIAL_STATE
    };

    this.toggleAvatarModal               = this.toggleAvatarModal.bind(this);
    this.addSubProjectHandler            = this.addSubProjectHandler.bind(this);
    this.removeSubProject                = this.removeSubProject.bind(this);
    this.handlerResetAvatar              = this.handlerResetAvatar.bind(this);
    this.onFieldChange                   = this.onFieldChange.bind(this);
    this.handleProjectAvatarChange       = this.handleProjectAvatarChange.bind(this);
    this.handleAddProject                = this.handleAddProject.bind(this);
    this.handleColorPickerChangeComplete = this.handleColorPickerChangeComplete.bind(this);
  }

  addSubProjectHandler = () => {
    this.setState(prevState => ({ forms: [ ...prevState.forms, _.cloneDeep(DEFAULT_FORM) ] }));
  };

  removeSubProject = id => {
    const arr = this.state.forms;

    delete arr[id];
    this.setState({ forms: arr.filter( () => { return true; }) });
  };

  handlerResetAvatar = id => {
    const state = _.cloneDeep(this.state);
    state.forms[id].avatar.img = null;
    this.setState({ ...state });
  };

  handleProjectAvatarChange = (id, img) => {
    const state = _.cloneDeep(this.state);
    state.forms[id].avatar.img = img;
    this.setState({ ...state });
  };

  toggleAvatarModal = id => {
    const state = _.cloneDeep(this.state);
    state.forms[id].avatar.modal = !state.forms[id].avatar.modal;
    this.setState({ ...state });
  };

  handleColorPickerChangeComplete = (id, color) => {
    const state = _.cloneDeep(this.state);
    state.forms[id].colorPicker.selected = color;
    this.setState({ ...state });
  };

  onFieldChange = (e, id) => {
    const { dataset, value } = e.target;
    const state = _.cloneDeep(this.state);
    state.forms[id].fields[dataset.name] = value;
    this.setState({ ...state });
  };

  handleAddProject = () => {
    const { state, props, onRequestStarted } = this;
    const { onAddProject } = props;

    onRequestStarted();
    onAddProject(state, { helper: this });
  };

  componentDidMount() {
    const { props } = this;
    const { onComponentMount } = props;

    onComponentMount();
  }

  render() {
    const {
      handleColorPickerChangeComplete,
      handleProjectAvatarChange,
      handlerResetAvatar,
      removeSubProject,
      addSubProjectHandler,
      toggleAvatarModal,
      onFieldChange,
      handleAddProject,
      state
    } = this;

    const { project, onComponentMount, onLayoutUpdate } = this.props;
    const { isRequestLoading } = this.state;

    if (project) {
      return <ProjectAddFetchSuccess
        project={ project }
        onComponentMount={ onComponentMount }
        onLayoutUpdate={ onLayoutUpdate } />;
    }

    return (
      <div className="animated fadeIn project-add">
        <Row>
          <Col col="3" sm="4" md="10" >
            <h2>Create a new project</h2>
          </Col>
        </Row>
        <CustomForm
          advancedFields     = { true }
          avatarHandler      = { handleProjectAvatarChange }
          avatarReset        = { handlerResetAvatar }
          colorPickerHandler = { handleColorPickerChangeComplete }
          modalToggle        = { toggleAvatarModal }
          onRemoveSubProject = { removeSubProject }
          onFieldChange      = { onFieldChange }
          parentState        = { state }
          onSubmit           = { handleAddProject }
          isRequestLoading   = { isRequestLoading }
        />
        <Row className="flex-row-reverse">
          <Col col="3" sm="4" md="2" className="mb-3">
            <Button block color="warning" onClick={ addSubProjectHandler }>Add sub-project</Button>
          </Col>
        </Row>
      </div>
    );
  }
}

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

ProjectAdd.propTypes = propTypes;
ProjectAdd.defaultProps = defaultProps;

const mapStateToProps = state => {
  return {
    project: state.projectsReducer.projectAdd
  };
};

const mapDispatchToProps = dispatch => ({
  onAddProject: (res, opts) => {
    dispatch(addProject(res, opts));
  },
  onComponentMount: () => {
    dispatch(addProjectUpdate);
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProjectAdd);