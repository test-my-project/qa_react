import _ from 'lodash';
import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {
  Row,
  Col
} from 'reactstrap';

import {
  CustomTrelloBoard,
  CustomIssuesControlPanel,
  Preloader,
  IssuesListCap
} from './../../../components/shared';

import {
  getIssuesByProjectID,
  getIssuesPrioritiesProjects,
  getIssuesStatusesProjects,
  updateIssue,
  getProjectsForIssuesList
} from './../../../redux/actions';

class IssuesList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedProject: '',
      isLoading: false,
      isBoardLoading: false
    };

    this.startLoading      = this.startLoading.bind(this);
    this.startBoardLoading = this.startBoardLoading.bind(this);
    this.endLoading        = this.endLoading.bind(this);
    this.endBoardLoading   = this.endBoardLoading.bind(this);
    this.onUpdateBoard     = this.onUpdateBoard.bind(this);
    this.onProjectChange   = this.onProjectChange.bind(this);
  }

  endLoading() {
    this.setState({ isLoading: false });
  }

  startLoading() {
    this.setState({ isLoading: true });
  }

  endBoardLoading() {
    this.setState({ isBoardLoading: false });

    this.props.onGetIssuesListData(this.state.selectedProject, { helper: this });
  }

  startBoardLoading() {
    this.setState({ isBoardLoading: true });
  }

  componentDidMount() {
    this.props.onGetProjects();
  }

  onProjectChange(v) {
    this.startLoading();

    const selectedProject = _.find(this.props.projects, p => { return p.name === v; } );

    const { id, members } = selectedProject;

    this.setState({
      selectedProject: id,
      members
    });

    /**
     * Get new issues data
     */
    this.props.onGetIssuesListData(id, { helper: this });
  }

  onUpdateBoard(id, opts) {
    const { props, startBoardLoading } = this;
    const { onUpdateIssue } = props;

    onUpdateIssue(id, { ...opts, helper: this });
    startBoardLoading();
  }

  render() {
    const {
      state,
      props,
      onProjectChange,
      onUpdateBoard
    } = this;

    const {
      selectedProject,
      isBoardLoading,
      isLoading
    } = state;

    const {
      issues,
      statuses,
      priorities,
      projects,
      updatedIssue
    } = props;

    if (!projects) return <Preloader />;

    const { members } = state;

    return (
      <div className="animated fadeIn issues">
        <Row>
          <Col>
            <h2 className="app-title">My issues</h2>
          </Col>
        </Row>

        <CustomIssuesControlPanel
          projects        = { projects }
          onProjectChange = { onProjectChange }
        />

        { selectedProject ? <Row className="relative">
          <Col>
            <CustomTrelloBoard
              isBoardLoading = { isBoardLoading }
              isLoading      = { isLoading }
              members        = { members }
              issues         = { issues }
              statuses       = { statuses }
              priorities     = { priorities }
              id             = { selectedProject }
              onUpdateBoard  = { onUpdateBoard }
              updatedIssue   = { updatedIssue }
            />
          </Col>
        </Row> : <IssuesListCap /> }
      </div>
    );
  }
}

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

IssuesList.propTypes = propTypes;
IssuesList.defaultProps = defaultProps;

const mapStateToProps = state => {
  return {
    issues       : state.issuesReducer.issues,
    statuses     : state.issuesReducer.statuses,
    priorities   : state.issuesReducer.priorities,
    updatedIssue : state.issuesReducer.updatedIssue,

    projects     : state.projectsReducer.projects
  };
};

const mapDispatchToProps = dispatch => ({
  onGetProjects: () => {
    dispatch(getProjectsForIssuesList);
  },
  onGetIssuesListData: (id, opts) => {

    /**
     * Issues data
     */
    dispatch(getIssuesByProjectID(id, opts));

    /**
     * Issues statuses data
     */
    dispatch(getIssuesStatusesProjects);

    /**
     * Issues priorities data
     */
    dispatch(getIssuesPrioritiesProjects);
  },
  onUpdateIssue: (id, opts) => {
    dispatch(updateIssue(id, opts));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IssuesList);