import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  Col,
  Row
} from 'reactstrap';

import { CustomForm } from './../../../components/shared';

class IssueAdd extends Component {
  constructor(props) {
    super(props);

    this.state = {
      colorPicker: {
        default: '#f44336',
        selected: '#f44336',
        width: '400px'
      },
      avatar: {
        firstLetter: 'p'
      },
      modal: false
    };

    this.toggleAvatarModal = this.toggleAvatarModal.bind(this);
    this.handleColorPickerChangeComplete = this.handleColorPickerChangeComplete.bind(this);
  }

  toggleAvatarModal() {
    this.setState({
      modal: !this.state.modal,
    });
  }

  handleColorPickerChangeComplete = (color) => {
    this.setState(prevState => ({
      colorPicker: {
        ...prevState.colorPicker,
        selected: color.hex
      }
    }));
  };

  render() {
    const {
      handleColorPickerChangeComplete,
      state,
      toggleAvatarModal
    } = this;

    return (
      <div className="animated fadeIn">
        <Row>
          <Col col="3" sm="4" md="10" >
            <h2>Create a new issue</h2>
          </Col>
        </Row>
        <CustomForm
          colorPickerHandler={ handleColorPickerChangeComplete }
          modalToggle={ toggleAvatarModal }
          parentState={ state }
        />
      </div>
    );
  }
}

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

IssueAdd.propTypes = propTypes;
IssueAdd.defaultProps = defaultProps;

export default IssueAdd;