import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import moment from 'moment';
import { Link } from 'react-router-dom';
import {
  ListGroup,
  ListGroupItem,
} from 'reactstrap';
import { getProjectsList } from './../../../redux/actions';
import { Preloader } from './../../../components/shared';

class ProjectsList extends Component {
  componentDidMount() {
    this.props.onGetProjectsList();
  }

  render() {
    const { projectsList } = this.props;
    console.log(projectsList);
    if (!projectsList) return <Preloader/>;

    return (
      <Fragment>
        <h2 className="app-title">Projects</h2>
      <ListGroup className='projects-list animated fadeIn'>
        { projectsList.map(item => {
            const { id, avatar, url, owner, name, updatedAt, description, hex } = item;
            return (
              <ListGroupItem key={ id }>
                <div className="avatar-project">
                  <Link to={ url } style={{ backgroundColor: hex }}>
                    { name.charAt(0) }
                    { avatar && <img src={ avatar } alt={ name } /> }
                  </Link>
                </div>
                <div className="project-details">
                  <Link className='project-title' to={ url }><span>{ owner }</span> / <span
                    className='project-name'>{ name }</span></Link>
                  <p>{ description }</p>
                  <Link to={ url }>{ url }</Link>
                </div>
                <div className='update-project'>
                  <span>Updated { moment(updatedAt, "YYYYMMDD").fromNow() }</span>
                </div>
              </ListGroupItem>
            )
          }
        )}
      </ListGroup>
      </Fragment>
    );
  }
}

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

ProjectsList.propTypes = propTypes;
ProjectsList.defaultProps = defaultProps;

const mapStateToProps = state => {
  return {
    projectsList: state.projectsReducer.projectsList
  };
};

const mapDispatchToProps = dispatch => ({
  onGetProjectsList: () => {
    dispatch(getProjectsList);
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProjectsList);