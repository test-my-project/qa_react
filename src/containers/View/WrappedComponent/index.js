import { Component } from 'react';
import { general } from './../../../config';

class WrappedComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isRequestLoading: false
    };

    this.onRequestStarted  = this.onRequestStarted.bind(this);
    this.onRequestFinished = this.onRequestFinished.bind(this);

    this.hideLoader = this.hideLoader.bind(this);
  }

  onRequestStarted = () => {
    this.setState({ isRequestLoading: true });
  };

  onRequestFinished = () => {
    this.setState({ isRequestLoading: false });
  };

  hideLoader() {
    setTimeout(() => {
      this.setState({
        isLoading: false
      });
    }, general.SMALL_LOADER.DELAY);
  }
}

export default WrappedComponent;