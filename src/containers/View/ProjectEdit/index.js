import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row
} from 'reactstrap';
import { AvForm } from 'availity-reactstrap-validation';

import {
  CustomFormModal,
  CustomConfirmModal,
  CustomTooltip,
  CustomTooltipBtn,
  DefaultFormFieldsToRender,
  DefaultFormFooterToRender,
  Preloader
} from './../../../components/shared';

import WrappedComponent from './../WrappedComponent';

import {
  getProject,
  updateProject,
  deleteProject,
  sendInvite,
  removeFromProject
} from './../../../redux/actions';

import { getFirstLetter } from './../../../helpers';

class ProjectMembers extends WrappedComponent {
  constructor(props) {
    super(props);

    this.state = {
      ...this.state,
      projectID: this.props.helper.props.match.params.id,
      fields: {},
      modal: false
    };

    this.onValidate          = this.onValidate.bind(this);
    this.toggleInviteModal   = this.toggleInviteModal.bind(this);
    this.onInvite            = this.onInvite.bind(this);
    this.onRemoveFromProject = this.onRemoveFromProject.bind(this);
  }

  toggleInviteModal () {
    this.setState({
      modal: !this.state.modal,
    });
  }

  onValidate(e, errors) {
    if (errors && errors.length === 0) {
      const { state, props, onRequestStarted, onInvite } = this;
      const { fields, projectID } = state;
      const parentOpts = props.opts;

      const opts = {
        id: projectID,
        name: parentOpts.fields.projectName,
        helper: this
      };

      onRequestStarted();
      onInvite(fields, opts);
    }
  }

  onFieldChange = e => {
    const { dataset, value } = e.target;
    const state = _.cloneDeep(this.state);
    state.fields[dataset.name] = value;
    this.setState({ ...state });
  };

  onInvite(fields, opts) {
    opts = { fields, ...opts };
    this.props.helper.props.onSendInvite(opts);
  }

  onRemoveFromProject(user){
    const { state, props } = this;
    const { projectID } = state;
    const parentOpts = props.opts;

    const opts = {
      id: projectID,
      name: parentOpts.fields.projectName,
      email: user.email,
      userId: user.id,
      helper: props.helper
    };

    props.helper.props.onRemoveFromProject(opts);
    props.helper.props.onGetProject(projectID);
  }

  render() {
    const {
      toggleInviteModal,
      state,
      props,
      onRemoveFromProject,
      onFieldChange,
      onValidate
    } = this;
    const { members } = props.opts;
    const { modal, isRequestLoading, projectID: id } = state;

    return (
      <React.Fragment>
        <ul className="members-list">
          <li className="members-item add">
            <CustomTooltipBtn
              onClick        = { toggleInviteModal }
              content        = { <i className="icon-plus" /> }
              id             = { id }
              tooltipContent = "Invite new members"
              size           = "lg"
              className      = "btn-link"
            />
          </li>

          { members.map(({ id, email, display_name: name }) =>
            <li
              className = "members-item"
              key       = { id }
            >
              <CustomTooltipBtn
                id             = { id }
                content        = { <i className="icon-close" /> }
                tooltipContent = { `Remove ${ email } from this project` }
                size           = "lg"
                className      = "btn-link text-danger"
                onClick        = { () => onRemoveFromProject({ id, email }) }
              />
              <CustomTooltip
                content        = {
                  <span>
                    { getFirstLetter(name) }
                  </span>
                }
                id             = { `member-${ id }` }
                tooltipContent = { `Member ${ email }` }
              />
            </li>
          )}
        </ul>
        <CustomFormModal
          toggle           = { toggleInviteModal }
          state            = { modal }
          isRequestLoading = { isRequestLoading }
          title            = "Invite email to project"
          acceptBtn        = "Send invite"
          cancelBtn        = "Cancel"
          acceptAction     = { onValidate }
          onValidate       = { onValidate }
          onFieldChange    = { onFieldChange }
          validate         = { true }
        />
      </React.Fragment>
    );
  }
}

class ProjectEdit extends WrappedComponent {
  constructor(props) {
    super(props);

    this.state = {
      ...this.state,
      projectID: this.props.match.params.id,
      isLoading: true,
      fields: {},
      footerControls: [
        { color: 'primary', text: 'Update', type: 'validate' },
        { color: 'danger', text: 'Remove', type: 'remove' },
      ],
      modal: false,
      inviteModal: false,
      confirmRemoveQuestion: 'Are sure that you want to DELETE this project?'
    };

    this.onValidate        = this.onValidate.bind(this);
    this.onRemove          = this.onRemove.bind(this);
    this.toggleRemoveModal = this.toggleRemoveModal.bind(this);
    this.onFieldChange     = this.onFieldChange.bind(this);
  }

  onFieldChange = e => {
    const { dataset, value } = e.target;
    const state = _.cloneDeep(this.state);
    state.fields[dataset.name] = value;
    this.setState({ ...state });
  };

  toggleRemoveModal() {
    this.setState({
      modal: !this.state.modal,
    });
  }

  onValidate(e, errors) {
    if (errors && errors.length === 0) {
      const { state, props, onRequestStarted } = this;
      const { fields, projectID } = state;

      const opts = {
        id: projectID,
        name: props.project.fields.projectName,
        helper: this
      };
      onRequestStarted();
      props.onUpdateProject(fields, opts);
    }
  }

  onRemove() {
    const { state, props } = this;
    const { projectID } = state;

    const opts = {
      id: projectID,
      name: props.project.fields.projectName,
      helper: this
    };

    props.onRemoveProject(opts);
  }

  avatarToRender (opts) {
    const { avatar, hex, id, firstLetter } = opts;
    const childEl = avatar ? <img src={ avatar } alt={ id } /> : firstLetter;

    return <div className="avatar" style={{ backgroundColor: hex }}>{ childEl }</div>;
  }

  componentDidMount () {
    this.props.onGetProject(this.state.projectID);
    this.hideLoader();
  }

  componentDidUpdate(prevProps) {
    if (this.props.match.params.id !== prevProps.match.params.id) {
      this.setState({
        isLoading: true,
        projectID: this.props.match.params.id
      });
      this.props.onGetProject(this.props.match.params.id);
      this.hideLoader();
    }
  }

  render() {
    const {
      state,
      avatarToRender,
      onFieldChange,
      onValidate,
      onRemove,
      props,
      toggleRemoveModal
    } = this;

    const { isLoading, footerControls, modal, confirmRemoveQuestion, isRequestLoading } = state;
    const { project } = props;

    if (isLoading) return <Preloader />;
    if (!project) return <Preloader />;

    const { id, fields } = project;
    const { projectName } = fields;

    return (
      <div className="animated fadeIn project-edit">
        <Row>
          <Col xs="12">
            <Card>
              <CardHeader>
                { avatarToRender(project) }
                <h1 className="title">{ projectName }</h1>
                <span className="badges float-right">
                  <ProjectMembers
                    opts   = { project }
                    helper = { this }
                    id     = { id }
                  />
                </span>
              </CardHeader>
              <CardBody>
                <AvForm
                  onSubmit  = { onValidate }
                  className = "custom-form form-horizontal"
                >
                  <DefaultFormFieldsToRender
                    onFieldChange = { onFieldChange }
                    parentState   = { project }
                    id            = { id }
                  />
                  <DefaultFormFooterToRender
                    withoutWrapper   = { true }
                    customControls   = { footerControls }
                    onRemove         = { toggleRemoveModal }
                    isRequestLoading = { isRequestLoading }
                  />
                </AvForm>
              </CardBody>
            </Card>
            <CustomConfirmModal
              toggle           = { toggleRemoveModal }
              state            = { modal }
              acceptAction     = { onRemove }
              question         = { confirmRemoveQuestion }
              isRequestLoading = { isRequestLoading }
              project          = { project }
            />
          </Col>
        </Row>
      </div>
    );
  }
}

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

ProjectEdit.propTypes = propTypes;
ProjectEdit.defaultProps = defaultProps;

const mapStateToProps = state => {
  return {
    project: state.projectsReducer.project
  };
};

const mapDispatchToProps = dispatch => ({
  onGetProject: id => {
    dispatch(getProject(id));
  },
  onUpdateProject: (req, opts) => {
    dispatch(updateProject(req, opts));
  },
  onRemoveProject: opts => {
    dispatch(deleteProject(opts));
  },
  onSendInvite: opts => {
    dispatch(sendInvite(opts));
  },
  onRemoveFromProject: opts => {
    dispatch(removeFromProject(opts));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProjectEdit);