import { toast } from 'react-toastify';

class Notification {
  call(type, message) {
    switch(type) {
      case 'success':
        toast.success(message, { position: toast.POSITION.BOTTOM_RIGHT });
        break;
      case 'error':
        toast.error(message, { position: toast.POSITION.BOTTOM_RIGHT });
        break;
      case 'warn':
        toast.warn(message, { position: toast.POSITION.BOTTOM_RIGHT });
        break;
      case 'info':
        toast.info(message, { position: toast.POSITION.BOTTOM_RIGHT });
        break;
      default:
        toast('Default notification!');
    }
  }
}

export default new Notification();