import _ from 'lodash';
import { Notification } from './../';

class ErrorHandler {
  call(e) {
    const { response } = e;

    if (!response) {
      Notification.call('error', 'Connection refused');
      return;
    }

    const { data, status } = response;
    const message = _.get(data, 'response.body.message');

    switch (status) {
      case 400:
        Notification.call('error', 'Bad request :(');
        break;
      case 404:
      case 500:
        Notification.call('error', message ? message : 'Unknown error');
        break;
      default:
        Notification.call('error', 'Unknown error');
    }
  }
}

export default new ErrorHandler();