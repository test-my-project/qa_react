import axios from 'axios';

import { api } from '../../config';
import {
  ErrorHandler,
  Notification
} from './../';

/**
 * Add project
 * @param data
 * @returns {Promise<*>}
 */
export async function addProject(data) {
  try {
    const res = await axios(
      {
        method: 'POST',
        baseURL: `${api.url}`,
        url: `${api.version}${api.routes.projects}`,
        headers: {
          'Content-Type': 'application/json'
        },
        data
      }
    );
    if (res.error) throw res;
    Notification.call('success',  `${res.data.name} has been created!`);
    return res.data;
  } catch (e) {
    ErrorHandler.call(e);
  }
}

/**
 * Get projects
 * @returns {Promise<*>}
 */
export async function getProjects() {
  try {
    const res = await axios(
      {
        method: 'GET',
        baseURL: `${api.url}`,
        url: `${api.version}${api.routes.projects}`,
        headers: {
          'Content-Type': 'application/json'
        },
      }
    );
    if (res.error) throw res;
    return res.data;
  } catch (e) {
    return e.response.status;
  }
}

/**
 * Get project by provided ID
 * @param id
 * @returns {Promise<*>}
 */
export async function getProject(id) {
  try {
    const res = await axios(
      {
        method: 'GET',
        baseURL: `${api.url}`,
        url: `${api.version}${api.routes.projects}/${id}`,
        headers: {
          'Content-Type': 'application/json'
        },
      }
    );
    if (res.error) throw res;
    return res.data;
  } catch (e) {
    return e.response.status;
  }
}

/**
 * Update project by provided ID
 * @param data
 * @param opts
 * @returns {Promise<*>}
 */
export async function updateProject(data, opts) {
  try {
    const res = await axios(
      {
        method: 'PUT',
        baseURL: `${api.url}`,
        url: `${api.version}${api.routes.projects}/${opts.id}`,
        headers: {
          'Content-Type': 'application/json'
        },
        data
      }
    );
    if (res.error) throw res;
    Notification.call('success',  `${opts.name} has been updated!`);
    return res.data;
  } catch (e) {
    ErrorHandler.call(e);
  }
}

/**
 * Delete project by provided ID
 * @param opts
 * @returns {Promise<*>}
 */
export async function deleteProject(opts) {
  try {
    const res = await axios(
      {
        method: 'DELETE',
        baseURL: `${api.url}`,
        url: `${api.version}${api.routes.projects}/${opts.id}`,
        headers: {
          'Content-Type': 'application/json'
        },
      }
    );
    if (res.error) throw res;
    Notification.call('success',  `${opts.name} has been deleted!`);
    return res.data;
  } catch (e) {
    return e.response.status;
  }
}

export async function sendInviteToProject (data) {
  try {
    const res = await axios(
      {
        method: 'POST',
        baseURL: `${api.url}`,
        url: `${api.version}${api.routes.messenger}/mail`,
        headers: {
          'Content-Type': 'application/json'
        },
        data
      }
    );
    if (res.error) throw res;
    Notification.call('success',  `${data.emails.join(', ')} has been invited to ${data.params.name}!`);
    return res.data;
  } catch (e) {
    return e.response.status;
  }
}

export async function removeMemberFromProject (data) {
  try {
    const res = await axios(
      {
        method: 'DELETE',
        baseURL: `${api.url}`,
        url: `${api.version}${api.routes.projects}/${data.id}/member/${data.userId}`,
        headers: {
          'Content-Type': 'application/json'
        },
      }
    );
    if (res.error) throw res;
    Notification.call('success',  `${data.email} has been removed from ${data.name}!`);
    return res.data;
  } catch (e) {
    return e.response.status;
  }
}