import axios from 'axios';

import { api } from '../../config';

/**
 * Get members by project id
 * @param id
 * @returns {Promise<*>}
 */
export async function getMembersByProjectId(id) {
  try {
    const res = await axios(
      {
        method: 'GET',
        baseURL: `${api.url}`,
        url: `${api.version}${api.routes.projects}/${id}/${api.routes.member}`,
        headers: {
          'Content-Type': 'application/json'
        },
      }
    );
    if (res.error) throw res;
    return res.data;
  } catch (e) {
    return e.response.status;
  }
}