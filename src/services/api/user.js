import axios from 'axios';

import { api } from '../../config';
import {
  ErrorHandler,
  Notification
} from './../';

/**
 * Register the new user
 * @param data
 * @returns {Promise<*>}
 */
export async function registration (data) {
  try {
    const res = await axios(
      {
        method: 'POST',
        baseURL: `${api.url}`,
        url: `${api.version}${api.routes.registration}`,
        headers: {
          'Content-Type': 'application/json'
        },
        data
      }
    );
    if (res.error) throw res;
    Notification.call('success', `${res.data.display_name} have been created!`);
    return res.data;
  } catch (e) {
    ErrorHandler.call(e);
  }
}

/**
 * User login
 * @param data
 * @returns {Promise<*>}
 */
export async function login(data) {
  try {
    const res = await axios(
      {
        method: 'POST',
        baseURL: `${api.url}`,
        url: `${api.version}${api.routes.login}`,
        headers: {
          'Content-Type': 'application/json'
        },
        data
      }
    );
    if (res.error) throw res;
    return res.data;
  } catch (e) {
    ErrorHandler.call(e);
  }
}

/**
 * Check logged in user session
 * @returns {Promise<*>}
 */
export async function checkLogin() {
  try {
    const res = await axios(
      {
        method: 'GET',
        baseURL: `${api.url}`,
        url: `${api.version}${api.routes.checkLogin}`,
        headers: {
          'Content-Type': 'application/json'
        },
      }
    );
    if (res.error) throw res;
    return res.data;
  } catch (e) {
    return e.response.status;
  }
}