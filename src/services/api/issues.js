import axios from 'axios';

import { api } from '../../config';
import {
  ErrorHandler,
  Notification
} from './../';

/**
 * Get issues statuses
 * @returns {Promise<*>}
 */
export async function getStatuses() {
  try {
    const res = await axios(
      {
        method: 'GET',
        baseURL: `${api.url}`,
        url: `${api.version}${api.routes.statuses}`,
        headers: {
          'Content-Type': 'application/json'
        },
      }
    );
    if (res.error) throw res;
    return res.data;
  } catch (e) {
    return e.response.status;
  }
}

/**
 * Get issues priorities
 * @returns {Promise<*>}
 */
export async function getPriorities() {
  try {
    const res = await axios(
      {
        method: 'GET',
        baseURL: `${api.url}`,
        url: `${api.version}${api.routes.priorities}`,
        headers: {
          'Content-Type': 'application/json'
        },
      }
    );
    if (res.error) throw res;
    return res.data;
  } catch (e) {
    return e.response.status;
  }
}

/**
 * Add issue
 * @param data
 * @returns {Promise<*>}
 */
export async function addIssue(data) {
  try {
    const res = await axios(
      {
        method: 'POST',
        baseURL: `${api.url}`,
        url: `${api.version}${api.routes.issues}`,
        headers: {
          'Content-Type': 'application/json'
        },
        data
      }
    );
    if (res.error) throw res;
    Notification.call('success',  'Issue has been created!');
    return res.data;
  } catch (e) {
    ErrorHandler.call(e);
  }
}

/**
 * Update issue
 * @param id
 * @param data
 * @returns {Promise<*>}
 */
export async function updateIssue(id, data) {
  try {
    const res = await axios(
      {
        method: 'PUT',
        baseURL: `${api.url}`,
        url: `${api.version}${api.routes.issues}/${id}`,
        headers: {
          'Content-Type': 'application/json'
        },
        data
      }
    );
    if (res.error) throw res;
    return res.data;
  } catch (e) {
    ErrorHandler.call(e);
  }
}

/**
 * Get issues data
 * @returns {Promise<*>}
 */
export async function getIssues() {
  try {
    const res = await axios(
      {
        method: 'GET',
        baseURL: `${api.url}`,
        url: `${api.version}${api.routes.issues}`,
        headers: {
          'Content-Type': 'application/json'
        },
      }
    );
    if (res.error) throw res;
    return res.data;
  } catch (e) {
    return e.response.status;
  }
}

/**
 * Get issues data (by query param - project ID)
 * @returns {Promise<*>}
 */
export async function getIssuesByProjectID(id) {
  try {
    const res = await axios(
      {
        method: 'GET',
        baseURL: `${api.url}`,
        url: `${api.version}${api.routes.issues}?project_id=${id}`,
        headers: {
          'Content-Type': 'application/json'
        },
      }
    );
    if (res.error) throw res;
    return res.data;
  } catch (e) {
    return e.response.status;
  }
}