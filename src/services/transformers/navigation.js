import _ from 'lodash';
import { navigation } from './../../config';

/**
 * @param data - Array, this is important
 * @returns {*}
 */
export const navigationTransformer = data => {
  if (!data) return navigation;
  const nav = _.cloneDeep(navigation);
  const navArr = _.cloneDeep(nav.items);
  const navLabels =_.map(navArr, 'name');

  for (const item in data) {
    if (data.hasOwnProperty(item)) {
      const label = data[item][0];
      const value = data[item][1];

      if (value && value.children) {
        if (navLabels.includes(label)) {
          const index = navLabels.indexOf(label);
          if (navArr[index + 1]) {
            navArr[index + 1] = value;
          }
        }
      }
    }
  }

  nav.items = navArr;
  return nav;
};