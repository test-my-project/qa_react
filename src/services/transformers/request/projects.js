/* eslint camelcase: 0 */
import { generateRandomNumber } from './../../../helpers';

export const addProjectRequestTransformer = req => {
  const { forms } = req;
  const parent = forms[0];

  const { fields, avatar, colorPicker } = parent;
  const {
    projectType: type,
    workType,
    projectUrl: url,
    projectName: name,
    shortDescription: description
  } = fields;
  const { img } = avatar;
  const { selected } = colorPicker;
  let res = {
    company: generateRandomNumber(),
    avatar:	img,
    hex:	selected.hex || selected,
    work_type: workType,
    type,
    url,
    description,
    name
  };

  if (forms.length <= 1) return res;

  for (let i = 1; i < forms.length; i++) {
    const { fields, avatar, colorPicker } = forms[i];
    const {
      projectType: type,
      workType,
      projectUrl: url,
      projectName: name,
      shortDescription: description
    } = fields;
    const { img } = avatar;
    const { selected } = colorPicker;

    if (!res.childs) res.childs= [];
    res.childs.push({
      company: generateRandomNumber(),
      avatar:	img,
      hex:	selected.hex || selected,
      work_type: workType,
      type,
      url,
      description,
      name
    });
  }

  return res;
};

export const updateProjectRequestTransformer = req => {
  const {
    projectType: type,
    workType: work_type,
    projectUrl: url,
    projectName: name,
    shortDescription: description
  } = req;

  return { type, work_type, url, name, description };
};