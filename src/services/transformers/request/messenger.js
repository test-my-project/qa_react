/* eslint camelcase: 0 */
import Cryptr from 'cryptr';
import { api } from '../../../config';

const cryptr = new Cryptr(api.secret);

export const inviteMemberRequestTransformer = payload => {
  const email = payload.fields.member;
  const token = cryptr.encrypt(JSON.stringify({
    id: payload.id,
    email,
  }));
  return {
    template: 'invite_to_project',
    params: {
      name: payload.name,
      link: `${window.location.origin}${api.url}${api.version}${api.routes.projects}/invite/${token}`,
    },
    emails: Array.isArray(email) ? email : [email],
  };
};