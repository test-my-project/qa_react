/* eslint camelcase: 0 */
import { issues } from './../../../config';

const {
  SELECTED_BY_DEFAULT_STATUS_ID,
  SELECTED_BY_DEFAULT_PRIORITY_ID,
  SELECTED_BY_DEFAULT_ASSIGN_USER_ID
} = issues;

export const addIssueRequestTransformer = payload => {
  const {
    id,
    values,
    url,
    status,
    page,
    priority,
    assignTo,
    window
  } = payload;
  const { title, description } = values;

  return {
    project_id: +id,
    assignee_user_id: assignTo || SELECTED_BY_DEFAULT_ASSIGN_USER_ID,
    task_status_id: status ? status.id : SELECTED_BY_DEFAULT_STATUS_ID,
    task_priority_id: priority ? priority.id : SELECTED_BY_DEFAULT_PRIORITY_ID,
    window_width: window.width,
    window_height: window.height,
    coordinates_x: page.x,
    coordinates_y: page.y,
    url,
    description,
    title
  };
};

export const updateIssueRequestTransformer = payload => {
  const { laneId } = payload;
  return { task_status_id: laneId };
};