/**
 * Request transformers
 */
import {
  addProjectRequestTransformer,
  addIssueRequestTransformer,
  updateProjectRequestTransformer,
  updateIssueRequestTransformer,
  inviteMemberRequestTransformer
} from './request';

/**
 * Response transformers
 */
import {
  getMembersByProjectId,
  addIssueResponseTransformer,
  getIssueResponseTransformer,
  getProjectResponseTransformer,
  getProjectsForIssuesListResponseTransformer,
  getProjectForFrameResponseTransformer,
  getProjectsForFrameResponseTransformer,
  getProjectsResponseTransformer,
  getProjectsListResponseTransformer,
  getNavigationProjectsResponseTransformer,
  getIssuesStatusesForFrameResponseTransformer,
  getIssuesPrioritiesForFrameResponseTransformer
} from './response';

/**
 * Navigation transformer
 */
import { navigationTransformer } from './navigation';

const transformers = {
  getMembersByProjectId,
  addProjectRequestTransformer,
  updateIssueRequestTransformer,
  addIssueRequestTransformer,
  getIssueResponseTransformer,
  addIssueResponseTransformer,
  getNavigationProjectsResponseTransformer,
  getProjectResponseTransformer,
  getProjectsForIssuesListResponseTransformer,
  getProjectForFrameResponseTransformer,
  getProjectsForFrameResponseTransformer,
  getProjectsResponseTransformer,
  getProjectsListResponseTransformer,
  updateProjectRequestTransformer,
  navigationTransformer,
  inviteMemberRequestTransformer,
  getIssuesStatusesForFrameResponseTransformer,
  getIssuesPrioritiesForFrameResponseTransformer
};

export default transformers;