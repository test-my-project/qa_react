import sortBy from 'lodash/sortBy';
import { pageRoutes } from './../../../config';

export const getNavigationProjectsResponseTransformer = res => {
  if (res.length === 0) return;
  const data = [];
  let icon = 'icon-folder';

  for (let i = 0; i < res.length; i++) {
    const { name, id, childs } = res[i];
    const url = `${pageRoutes.projects.frame}/${id}`;
    const item = { name, url, icon };
    data.push(item);

    if (childs.length !== 0) {
      icon = 'icon-folder-alt';

      for (let i = 0; i < childs.length; i++) {
        const { name, id } = childs[i];
        const url = `${pageRoutes.projects.frame}/${id}`;
        const item = { name, url, icon };

        data.push(item);
      }
    }
  }

  return {
    name: 'My projects',
    icon: 'icon-layers',
    children: sortBy(data, 'name')
  };
};