/* eslint camelcase: 0 */
import _ from 'lodash';
import { getFirstLetter } from './../../../helpers';
import { pageRoutes } from './../../../config';

export const getProjectsResponseTransformer = res => {
  if (res.length === 0) return res;
  const data = [];

  for (let i = 0; i < res.length; i++) {
    let { name, id, hex, avatar, description, childs, members } = res[i];
    description = description.substring(0, 25) + '...';
    data.push({ name, id, hex, description, avatar, members });

    if (childs.length !== 0) {
      for (let i = 0; i < childs.length; i++) {
        let { name, id, hex, avatar, description, members } = childs[i];
        description = description.substring(0, 25) + '...';
        data.push({ name, id, hex, description, avatar, members });
      }
    }
  }

  return _.sortBy(data, 'name');
};

export const getProjectsListResponseTransformer = res => {
  return res;
};

export const getProjectsForIssuesListResponseTransformer = res => {
  if (res.length === 0) return res;
  const data = [];

  for (let i = 0; i < res.length; i++) {
    let { name, id, childs, members } = res[i];
    data.push({ name, id, members });

    if (childs.length !== 0) {
      for (let i = 0; i < childs.length; i++) {
        let { name, id, members } = childs[i];
        data.push({ name, id, members });
      }
    }
  }

  return _.sortBy(data, 'name');
};

const editProjectStateGenerator = (data) => {
  const {
    name: projectName,
    type: projectType,
    work_type: workType,
    description: shortDescription,
    url: projectUrl,
    avatar,
    hex,
    childs,
    members,
  } = data;
  const fields = { projectName, projectType, projectUrl, shortDescription, workType };
  const res = { fields: { ...fields }, avatar, hex, firstLetter: getFirstLetter(projectName), members };

  if (childs.length === 0) {
    return res;
  }

  return { ...res, hasChilds: true, childNames: _.map(childs, c => _.get(c, 'name')) };
};

export const getProjectResponseTransformer = res => {
  return editProjectStateGenerator(res[0]);
};

export const getProjectForFrameResponseTransformer = res => {
  const { url, name, members } = res[0];
  const projectMembers = _.map(members, m => ({
    id: m.id,
    name: m.display_name,
    email: m.email
  }));

  return { url, name, members: projectMembers };
};

export const getProjectsForFrameResponseTransformer = res => {
  const data = [];

  for (let i = 0; i < res.length; i++) {
    let {  name, id, childs, url } = res[i];
    const frameUrl = `${window.location.origin}/#${pageRoutes.projects.frame}/${id}`;
    data.push({ name, url, id, frameUrl });

    if (childs.length !== 0) {
      for (let i = 0; i < childs.length; i++) {
        let { name, id, url } = childs[i];
        const frameUrl = `${window.location.origin}/#${pageRoutes.projects.frame}/${id}`;
        data.push({ name, url, id, frameUrl });
      }
    }
  }

  return _.sortBy(data, 'name');
};