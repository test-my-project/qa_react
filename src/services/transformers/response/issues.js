import map from 'lodash/map';
import { capitalize } from './../../../helpers';
import { issues } from './../../../config';

const {
  SELECTED_BY_DEFAULT_STATUS_ID,
  SELECTED_BY_DEFAULT_PRIORITY_ID
} = issues;

export const getIssuesStatusesForFrameResponseTransformer = res => {
  return map(res, r => {
    const { id, title } = r;
    const res = { title: capitalize(title), id };

    return SELECTED_BY_DEFAULT_STATUS_ID === id ? { ...res, isDefaultToSelect: true } : res;
  });
};

export const getIssuesPrioritiesForFrameResponseTransformer = res => {
  return map(res, r => {
    const { id, title } = r;
    const res = { title: capitalize(title), id };

    return SELECTED_BY_DEFAULT_PRIORITY_ID === id ? { ...res, isDefaultToSelect: true } : res;
  });
};

export const addIssueResponseTransformer = res => {
  return res;
};

export const getIssueResponseTransformer = res => {
  return res;
};