import { compareHeight } from './../../helpers';
import { frame } from './../../config';

class FrameHelper {
  generateFrameSrc(url) {
    return window.location.hostname === 'localhost'
      ? `http://localhost:3030/proxy/?route=${ encodeURIComponent(url) }`
      : `/proxy/?route=${ encodeURIComponent(url) }`;
  }

  getGenericStyles(height, scale) {
    const currentHeight = compareHeight(height);
    switch (scale) {
      case '.5':
      case '.75':
        return  `-${Math.round((currentHeight - Math.round(currentHeight * scale)) / 2)}`;
      case '1.25':
      case '1.5':
        return Math.round((Math.round(currentHeight * scale) - currentHeight ) / 2);
      case '1':
      default:
        return 0;
    }
  }

  constructor() {
    this.classNames = {
      body: 'aside-menu-off-canvas header-fixed f__main-wrap'
    };

    this.eventsList = [
      [ 'click', this.eventListener ],
      [ 'keypress', this.eventListener ],
      [ 'mouseover', this.eventListenerHover ],
      [ 'mouseout', this.eventListenerHoverOff ],
    ];
  }

  getFrame() {
    return document.querySelector('.f__wrapper > iframe');
  }

  getFrameHeight() {
    const iframe = this.getFrame();
    if (!iframe) return '';
    return iframe.contentDocument.body.scrollHeight;
  }

  getContentWindow() {
    const iframe = this.getFrame();
    if (!iframe) return;
    return iframe.contentWindow;
  }

  getFrameHtml() {
    this.frameHtml = this.fWindow.document.getElementsByTagName('html')[0];
  }

  getFrameBody() {
    this.frameBody = this.fWindow.document.getElementsByTagName('body')[0];
  }

  getFrameWindow(id) {
    this.f = document.getElementById(id);
    this.fWindow = this.f.contentWindow;
  }

  /**
   * Need to refresh body tag in ComponentDidMount
   */
  setDefaultStyleToWrapper() {
    const { classNames } = this;
    const body = document.getElementsByTagName('body')[0];
    body.className = classNames.body;
  }

  getFrameData(id) {
    this.id = id;
    this.f = document.getElementById(id);
    this.fDocument = this.f.contentDocument || this.f.contentWindow.document;
    this.fWindow = this.f.contentWindow;
    return { ...this.fDocument, ...this.fWindow };
  }

  elementToString = element => {
    const attributes = Object.values(element.attributes).map(attr => (attr.localName + '="' + attr.value + '"'));
    return `<${element.tagName.toLowerCase()} ${attributes.join(' ')}>`;
  };

  urlFromFrameToUsual = href => {
    const res = window.location.hostname === 'localhost'
      ? href.replace('http://localhost:3030/proxy/?route=', '')
      : href.replace('/proxy/?route=', '');
    return decodeURIComponent(res);
  };

  /**
   * Events listeners
   */
  enableFrameScroll() {
    if (!this.fWindow) return '';

    this.getFrameHtml();
    this.getFrameBody();

    this.frameHtml.style.overflow = 'hidden';
    this.frameBody.style.overflow = 'hidden';
  }

  disableFrameScroll() {
    if (!this.fWindow) return '';

    this.getFrameHtml();
    this.getFrameBody();

    this.frameHtml.style = '';
    this.frameBody.style = '';
  }

  eventListenerHover = e => e.target.style.opacity = '.5';

  eventListenerHoverOff = e => e.target.style.opacity = '';

  eventListener = e => {
    e.preventDefault();
    e.stopPropagation();

    let element;

    if (e.clientX && e.clientY) {
      element = e.target;
    }

    this.logData = {
      date: new Date(),
      url: this.urlFromFrameToUsual(this.f.contentWindow.location.href)
    };

    if (this.f.contentWindow.document) {
      this.logData = {
        ...this.logData,
        window: {
          width: this.f.contentWindow.document.body.clientWidth,
          height: this.f.contentWindow.document.body.clientHeight,
        }
      };
    }

    if (e.keyCode) {
      this.logData = {
        ...this.logData,
        keyCode: e.keyCode,
        char: String.fromCharCode(e.keyCode || e.charCode)
      };
    }

    if (element) {
      this.logData = {
        ...this.logData,
        screen: {
          x: e.screenX,
          y: e.screenY
        },
        page: {
          x: e.pageX,
          y: e.pageY
        },
        pointer: {
          x: e.clientX,
          y: e.clientY
        },
        element: this.elementToString(element),
        position: {
          x: element.offsetLeft,
          y: element.offsetTop
        },
        parent: this.elementToString(element.parentElement),
      };
    }

    this.setState();

    if (!this.helper.state.isAsideOpened) {
      document.getElementById('f_aside_toggle').click();
      this.helper.setState(prevState => ({
        frame: { ...prevState.frame, isPointerEnable: false },
        isAsideOpened: true
      }));
    }
  };

  enableFrameEvents(id, opts) {
    this.getFrameWindow(id);
    this.getFrameHtml();
    this.getFrameBody();

    this.f.closest('.f__wrapper').classList.add('f__pointer--enabled');
    this.frameHtml.style.cursor = 'crosshair';
    this.frameBody.style.cursor = 'crosshair';

    this.eventsList.map(e => this.fWindow.addEventListener(e[0], e[1], true));

    this.helper = opts.helper;
    this.setState = () => opts.helper.setState(prevState =>  ({
      issue: {
        ...prevState.frame,
        pointer: { ...this.logData },
        create: { ...frame.issueCreateFormConfig }
      }
    }));
  }

  disableFrameEvents(id) {
    this.getFrameWindow(id);
    this.getFrameHtml();
    this.getFrameBody();

    this.helper = null;

    this.f.closest('.f__wrapper').classList.remove('f__pointer--enabled');
    this.frameHtml.style = '';
    this.frameBody.style = '';

    this.eventsList.map(e => this.fWindow.removeEventListener(e[0], e[1], true));
  }
}

export default new FrameHelper();