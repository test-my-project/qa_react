import Centrifuge from 'centrifuge';

import User from '../user';
import { api } from '../../config';

let socket;
export const connectCentrifuge = async () => {
  const { token } = User.getUserData();
  socket = new Centrifuge(api.centrifugoUrl);
  socket.setToken(token);
  socket.connect();
  socket.on('error', error => {
    // handle error in a way you want, here we just log it into browser console.
    console.error(error);
  });
};

export const on = (channel, cb) => {
  if (!socket) {
    connectCentrifuge().then(() => on(channel, cb));
    return;
  }
  socket.subscribe(channel, cb);
};