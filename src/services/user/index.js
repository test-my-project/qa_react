import { pageRoutes, user, general } from './../../config';
import { getFirstLetter } from './../../helpers';
import { Notification } from './../';

class User {
  constructor(){
    this.isLoggedIn = false;
    this.data = this.setUserData();
  }

  /**
   * Set user data
   */
  setUserData() {
    if (localStorage.getItem(user.storageName)) this.isLoggedIn = true;
    return JSON.parse(localStorage.getItem(user.storageName)) || null;
  }

  /**
   * Set user data after login to the storage
   */
  setDataFromResponse(data) {
    this.isLoggedIn = true;
    this.data = data;
    localStorage.setItem(user.storageName, JSON.stringify(data));
    localStorage.setItem(user.firstLoginStorageName, 'true');
    window.location.href = window.location.origin + pageRoutes.root;
  }

  /**
   * Remove user data
   */
  removeUserData() {
    if (!this.isLoggedIn) return;
    this.isLoggedIn = false;
    this.data = null;
    localStorage.clear();
    window.location.href = window.location.origin + pageRoutes.root;
  }

  /**
   * Get user data
   */
  getUserData() {
    return this.data;
  }

  /**
   * Get first letter
   */
  getFirstLetter() {
    return getFirstLetter(this.data.display_name);
  }

  /**
   * Check first load
   */
  checkFirstLoad() {
    return localStorage.getItem(user.firstLoginStorageName);
  }

  /**
   * First login user notification
   */
  welcomeNotify() {
    setTimeout(() => {
      Notification.call('success', `Welcome back ${this.data.display_name}!`);
      localStorage.removeItem(user.firstLoginStorageName);
    }, general.SITE_LOADER.DELAY);
  }
}

export default new User();