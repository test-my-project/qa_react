import React, { Component } from 'react';
import { connect } from 'react-redux';
import { DashboardCardRow } from './../../components/shared';
import { getProjects } from './../../redux/actions';

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dropdownOpen: false,
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }

  componentDidMount() {
    const { props } = this;
    const { onGetProjects } = props;

    onGetProjects();
  }

  render() {
    const { props } = this;
    const { projects } = props;

    return (
      <div className="animated fadeIn">
        <DashboardCardRow projects={ projects }/>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    projects: state.projectsReducer.projects
  };
};

const mapDispatchToProps = dispatch => ({
  onGetProjects: () => {
    dispatch(getProjects);
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard);