import React from 'react';
import Loadable from 'react-loadable';

import DefaultLayout from './../../containers/DefaultLayout';
import FrameLayout from './../../containers/FrameLayout';
import { Preloader } from './../../components/shared';

import {
  routes as appRoutes,
  pageRoutes
}  from './../../config';

function loading() {
  return <Preloader />;
}

const Breadcrumbs = Loadable({
  loader: () => import('./../../views/Base/Breadcrumbs'),
  loading,
});

const Cards = Loadable({
  loader: () => import('./../../views/Base/Cards'),
  loading,
});

const Carousels = Loadable({
  loader: () => import('./../../views/Base/Carousels'),
  loading,
});

const Collapses = Loadable({
  loader: () => import('./../../views/Base/Collapses'),
  loading,
});

const Dropdowns = Loadable({
  loader: () => import('./../../views/Base/Dropdowns'),
  loading,
});

const Forms = Loadable({
  loader: () => import('./../../views/Base/Forms'),
  loading,
});

const Jumbotrons = Loadable({
  loader: () => import('./../../views/Base/Jumbotrons'),
  loading,
});

const ListGroups = Loadable({
  loader: () => import('./../../views/Base/ListGroups'),
  loading,
});

const Navbars = Loadable({
  loader: () => import('./../../views/Base/Navbars'),
  loading,
});

const Navs = Loadable({
  loader: () => import('./../../views/Base/Navs'),
  loading,
});

const Paginations = Loadable({
  loader: () => import('./../../views/Base/Paginations'),
  loading,
});

const Popovers = Loadable({
  loader: () => import('./../../views/Base/Popovers'),
  loading,
});

const ProgressBar = Loadable({
  loader: () => import('./../../views/Base/ProgressBar'),
  loading,
});

const Switches = Loadable({
  loader: () => import('./../../views/Base/Switches'),
  loading,
});

const Tables = Loadable({
  loader: () => import('./../../views/Base/Tables'),
  loading,
});

const Tabs = Loadable({
  loader: () => import('./../../views/Base/Tabs'),
  loading,
});

const Tooltips = Loadable({
  loader: () => import('./../../views/Base/Tooltips'),
  loading,
});

const BrandButtons = Loadable({
  loader: () => import('./../../views/Buttons/BrandButtons'),
  loading,
});

const ButtonDropdowns = Loadable({
  loader: () => import('./../../views/Buttons/ButtonDropdowns'),
  loading,
});

const ButtonGroups = Loadable({
  loader: () => import('./../../views/Buttons/ButtonGroups'),
  loading,
});

const Buttons = Loadable({
  loader: () => import('./../../views/Buttons/Buttons'),
  loading,
});

const Charts = Loadable({
  loader: () => import('./../../views/Charts'),
  loading,
});

const Dashboard = Loadable({
  loader: () => import('./../../views/Dashboard'),
  loading,
});

const CoreUIIcons = Loadable({
  loader: () => import('./../../views/Icons/CoreUIIcons'),
  loading,
});

const Flags = Loadable({
  loader: () => import('./../../views/Icons/Flags'),
  loading,
});

const FontAwesome = Loadable({
  loader: () => import('./../../views/Icons/FontAwesome'),
  loading,
});

const SimpleLineIcons = Loadable({
  loader: () => import('./../../views/Icons/SimpleLineIcons'),
  loading,
});

const Alerts = Loadable({
  loader: () => import('./../../views/Notifications/Alerts'),
  loading,
});

const Badges = Loadable({
  loader: () => import('./../../views/Notifications/Badges'),
  loading,
});

const Modals = Loadable({
  loader: () => import('./../../views/Notifications/Modals'),
  loading,
});

const Colors = Loadable({
  loader: () => import('./../../views/Theme/Colors'),
  loading,
});

const Typography = Loadable({
  loader: () => import('./../../views/Theme/Typography'),
  loading,
});

const Widgets = Loadable({
  loader: () => import('./../../views/Widgets/Widgets'),
  loading,
});

const Users = Loadable({
  loader: () => import('./../../views/Users/Users'),
  loading,
});

const User = Loadable({
  loader: () => import('./../../views/Users/User'),
  loading,
});

/**
 * Custom components
 */
const ProjectsList = Loadable({
  loader: () => import('../../containers/View/ProjectsList/ProjectsList.js'),
  loading,
});

const ProjectEdit = Loadable({
  loader: () => import('./../../containers/View/ProjectEdit'),
  loading,
});

const ProjectAdd = Loadable({
  loader: () => import('./../../containers/View/ProjectAdd'),
  loading,
});

const IssueAdd = Loadable({
  loader: () => import('./../../containers/View/IssueAdd'),
  loading,
});

const IssuesList = Loadable({
  loader: () => import('./../../containers/View/IssuesList/IssuesList'),
  loading,
});

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: appRoutes.main, exact: true, name: 'Home', component: DefaultLayout },
  { path: pageRoutes.projects.frameID, exact: true, name: 'Frame', component: FrameLayout },
  { path: appRoutes.dashboard, name: 'Dashboard', component: Dashboard },
  { path: appRoutes.theme, exact: true, name: 'Theme', component: Colors },
  { path: appRoutes.themeColors, name: 'Colors', component: Colors },
  { path: appRoutes.themeTypography, name: 'Typography', component: Typography },
  { path: appRoutes.base, exact: true, name: 'Base', component: Cards },
  { path: appRoutes.baseCards, name: 'Cards', component: Cards },
  { path: appRoutes.baseForms, name: 'Forms', component: Forms },
  { path: appRoutes.baseSwitches, name: 'Switches', component: Switches },
  { path: appRoutes.baseTables, name: 'Tables', component: Tables },
  { path: appRoutes.baseTabs, name: 'Tabs', component: Tabs },
  { path: appRoutes.baseBreadcrumbs, name: 'Breadcrumbs', component: Breadcrumbs },
  { path: appRoutes.baseCarousels, name: 'Carousel', component: Carousels },
  { path: appRoutes.baseCollapses, name: 'Collapse', component: Collapses },
  { path: appRoutes.baseDropdowns, name: 'Dropdowns', component: Dropdowns },
  { path: appRoutes.baseJumbotrons, name: 'Jumbotrons', component: Jumbotrons },
  { path: appRoutes.baseListGroups, name: 'List Groups', component: ListGroups },
  { path: appRoutes.baseNavbars, name: 'Navbars', component: Navbars },
  { path: appRoutes.baseNavs, name: 'Navs', component: Navs },
  { path: appRoutes.basePaginations, name: 'Paginations', component: Paginations },
  { path: appRoutes.basePopovers, name: 'Popovers', component: Popovers },
  { path: appRoutes.baseProgressBar, name: 'Progress Bar', component: ProgressBar },
  { path: appRoutes.baseTooltips, name: 'Tooltips', component: Tooltips },
  { path: appRoutes.buttons, exact: true, name: 'Buttons', component: Buttons },
  { path: appRoutes.buttonsButtons, name: 'Buttons', component: Buttons },
  { path: appRoutes.buttonsButtonDropdowns, name: 'Button Dropdowns', component: ButtonDropdowns },
  { path: appRoutes.buttonsButtonGroups, name: 'Button Groups', component: ButtonGroups },
  { path: appRoutes.buttonsBrandButtons, name: 'Brand Buttons', component: BrandButtons },
  { path: appRoutes.icons, exact: true, name: 'Icons', component: CoreUIIcons },
  { path: appRoutes.iconsCoreUI, name: 'CoreUI Icons', component: CoreUIIcons },
  { path: appRoutes.iconsFlags, name: 'Flags', component: Flags },
  { path: appRoutes.iconsFontAwesom, name: 'Font Awesome', component: FontAwesome },
  { path: appRoutes.iconsSimpleLineIcons, name: 'Simple Line Icons', component: SimpleLineIcons },
  { path: appRoutes.notifications, exact: true, name: 'Notifications', component: Alerts },
  { path: appRoutes.notificationsAlerts, name: 'Alerts', component: Alerts },
  { path: appRoutes.notificationsBadges, name: 'Badges', component: Badges },
  { path: appRoutes.notificationsModals, name: 'Modals', component: Modals },
  { path: appRoutes.widgets, name: 'Widgets', component: Widgets },
  { path: appRoutes.charts, name: 'Charts', component: Charts },
  { path: appRoutes.users, exact: true,  name: 'Users', component: Users },
  { path: appRoutes.usersID, exact: true, name: 'User Details', component: User },

  /**
   * Custom components
   */
  { path: pageRoutes.projects.main, name: 'Projects', component: ProjectsList },
  { path: pageRoutes.projects.add, exact: true, name: 'New project', component: ProjectAdd },
  { path: pageRoutes.projects.editID, name: 'Edit your project', component: ProjectEdit },
  { path: pageRoutes.issues.add, exact: true, name: 'New issue', component: IssueAdd },
  { path: pageRoutes.issues.main, exact: true, name: 'Issues', component: IssuesList },
];

export default routes;