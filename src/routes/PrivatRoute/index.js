import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';

import { checkUser } from './../../helpers';
import { pageRoutes } from './../../config';

class PrivatRoute extends Component {
  render() {
    const { props } = this;
    return (
      !checkUser() ? <Route {...props} /> : <Redirect to={pageRoutes.root} />
    );
  }
}

export default PrivatRoute;