/**
 * Main routes
 */
import routes from './routes/';

/**
 * Custom routes
 */
import ProtectedRoute from './ProtectedRoute';
import PrivatRoute from './PrivatRoute';

export {
  routes,
  ProtectedRoute,
  PrivatRoute
};