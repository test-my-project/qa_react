import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';

import { checkAuth } from './../../helpers';
import { pageRoutes } from './../../config';

class ProtectedRoute extends Component {
  render() {
    const { props } = this;

    return (
      checkAuth(window.location.pathname) ? <Route { ...props } /> : <Redirect to={ pageRoutes.login } />
    );
  }
}

export default ProtectedRoute;