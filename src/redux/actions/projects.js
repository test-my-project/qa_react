import {
  FETCH_PROJECT_ADD_REQUEST,
  FETCH_PROJECT_ADD_SUCCESS,
  FETCH_PROJECT_ADD_FAILURE,

  FETCH_PROJECT_GET_REQUEST,
  FETCH_PROJECT_GET_SUCCESS,
  FETCH_PROJECT_GET_FAILURE,

  FETCH_PROJECT_UPDATE_REQUEST,
  FETCH_PROJECT_UPDATE_SUCCESS,
  FETCH_PROJECT_UPDATE_FAILURE,

  FETCH_PROJECT_DELETE_REQUEST,
  FETCH_PROJECT_DELETE_SUCCESS,
  FETCH_PROJECT_DELETE_FAILURE,

  FETCH_PROJECTS_GET_REQUEST,
  FETCH_PROJECTS_GET_SUCCESS,
  FETCH_PROJECTS_GET_FAILURE,

  FETCH_PROJECTS_LIST_GET_REQUEST,
  FETCH_PROJECTS_LIST_GET_SUCCESS,
  FETCH_PROJECTS_LIST_GET_FAILURE,

  FETCH_PROJECTS_GET_FOR_ISSUES_LIST_REQUEST,
  FETCH_PROJECTS_GET_FOR_ISSUES_LIST_SUCCESS,
  FETCH_PROJECTS_GET_FOR_ISSUES_LIST_FAILURE,

  PROJECT_ADD_UPDATE_STORE,

  INVITE_PROJECT_POST_REQUEST,
  INVITE_PROJECT_POST_SUCCESS,
  INVITE_PROJECT_POST_FAILURE,

  INVITE_PROJECT_DELETE_REQUEST,
  INVITE_PROJECT_DELETE_SUCCESS,
  INVITE_PROJECT_DELETE_FAILURE,
} from './types';

import {
  ApiService,
  Notification,
  transformers as t
} from './../../services';

import { GoTo } from './../../helpers';

/**
 * Project add actions
 */
const addProjectRequest = () => {
  return {
    type: FETCH_PROJECT_ADD_REQUEST
  };
};

const addProjectSuccess = payload => {
  return {
    type: FETCH_PROJECT_ADD_SUCCESS,
    payload
  };
};

const addProjectFailure = error => {
  return {
    type: FETCH_PROJECT_ADD_FAILURE,
    error
  };
};

const addProject = (req, opts = {}) => async dispatch => {
  dispatch(addProjectRequest());
  try {
    const payload = await ApiService.addProject(t.addProjectRequestTransformer(req));
    if (!payload) return;
    dispatch(addProjectSuccess(payload));
  } catch (e) {
    dispatch(addProjectFailure(e));
  } finally {
    opts.helper.onRequestFinished();
  }
};

/**
 * Project get actions
 */
const getProjectRequest = () => {
  return {
    type: FETCH_PROJECT_GET_REQUEST
  };
};

const getProjectSuccess = payload => {
  return {
    type: FETCH_PROJECT_GET_SUCCESS,
    payload
  };
};

const getProjectFailure = error => {
  return {
    type: FETCH_PROJECT_GET_FAILURE,
    error
  };
};

const getProject = id => async dispatch => {
  dispatch(getProjectRequest());
  try {
    const payload = await ApiService.getProject(id);
    if (!payload) return;
    dispatch(getProjectSuccess(t.getProjectResponseTransformer(payload)));
  } catch (e) {
    dispatch(getProjectFailure(e));
  }
};

/**
 * Project update actions
 */
const updateProjectRequest = () => {
  return {
    type: FETCH_PROJECT_UPDATE_REQUEST
  };
};

const updateProjectSuccess = payload => {
  return {
    type: FETCH_PROJECT_UPDATE_SUCCESS,
    payload
  };
};

const updateProjectFailure = error => {
  return {
    type: FETCH_PROJECT_UPDATE_FAILURE,
    error
  };
};

const updateProject = (req, opts = {}) => async dispatch => {
  dispatch(updateProjectRequest());
  try {
    const payload = await ApiService.updateProject(t.updateProjectRequestTransformer(req), opts);
    if (!payload) return;
    dispatch(updateProjectSuccess(payload));
  } catch (e) {
    dispatch(updateProjectFailure(e));
  } finally {
    opts.helper.onRequestFinished();
    opts.helper.props.onLayoutUpdate();
  }
};

/**
 * Project delete actions
 */
const deleteProjectRequest = () => {
  return {
    type: FETCH_PROJECT_DELETE_REQUEST
  };
};

const deleteProjectSuccess = payload => {
  return {
    type: FETCH_PROJECT_DELETE_SUCCESS,
    payload
  };
};

const deleteProjectFailure = error => {
  return {
    type: FETCH_PROJECT_DELETE_FAILURE,
    error
  };
};

const deleteProject = (opts = {}) => async dispatch => {
  dispatch(deleteProjectRequest());
  try {
    const payload = await ApiService.deleteProject(opts);
    if (!payload || payload === 500) return;
    dispatch(deleteProjectSuccess(payload));
    opts.helper.toggleRemoveModal();
    GoTo.dashboard(opts.helper);
  } catch (e) {
    dispatch(deleteProjectFailure(e));
  } finally {
    opts.helper.onRequestFinished();
    opts.helper.props.onLayoutUpdate();
  }
};

/**
 * Project component update
 */
const addProjectUpdateAction = () => {
  return {
    type: PROJECT_ADD_UPDATE_STORE
  };
};

const addProjectUpdate = async dispatch => {
  dispatch(addProjectUpdateAction());
};

/**
 * Projects get actions
 */
const getProjectsRequest = () => {
  return {
    type: FETCH_PROJECTS_GET_REQUEST
  };
};

const getProjectsSuccess = payload => {
  return {
    type: FETCH_PROJECTS_GET_SUCCESS,
    payload
  };
};

const getProjectsFailure = error => {
  return {
    type: FETCH_PROJECTS_GET_FAILURE,
    error
  };
};

const getProjects = async dispatch => {
  dispatch(getProjectsRequest());
  try {
    const payload = await ApiService.getProjects();
    if (!payload) return;
    dispatch(getProjectsSuccess(t.getProjectsResponseTransformer(payload)));
  } catch (e) {
    dispatch(getProjectsFailure(e));
  }
};

/**
 * Get projects list actions
 */
const getProjectsListRequest = () => {
  return {
    type: FETCH_PROJECTS_LIST_GET_REQUEST
  };
};

const getProjectsListSuccess = payload => {
  return {
    type: FETCH_PROJECTS_LIST_GET_SUCCESS,
    payload
  };
};

const getProjectsListFailure = error => {
  return {
    type: FETCH_PROJECTS_LIST_GET_FAILURE,
    error
  };
};

const getProjectsList = async dispatch => {
  dispatch(getProjectsListRequest());
  try {
    const payload = await ApiService.getProjects();
    if (!payload) return;
    dispatch(getProjectsListSuccess(t.getProjectsListResponseTransformer(payload)));
  } catch (e) {
    dispatch(getProjectsListFailure(e));
  }
};


/**
 * Get project for issues list
 */
const getProjectsForIssuesListRequest = () => {
  return {
    type: FETCH_PROJECTS_GET_FOR_ISSUES_LIST_REQUEST
  };
};

const getProjectsForIssuesListSuccess = payload => {
  return {
    type: FETCH_PROJECTS_GET_FOR_ISSUES_LIST_SUCCESS,
    payload
  };
};

const getProjectsForIssuesListFailure = error => {
  return {
    type: FETCH_PROJECTS_GET_FOR_ISSUES_LIST_FAILURE,
    error
  };
};

const getProjectsForIssuesList = async dispatch => {
  dispatch(getProjectsForIssuesListRequest());
  try {
    const payload = await ApiService.getProjects();
    if (!payload) return;
    dispatch(getProjectsForIssuesListSuccess(t.getProjectsForIssuesListResponseTransformer(payload)));
  } catch (e) {
    dispatch(getProjectsForIssuesListFailure(e));
  }
};

/**
 * Invite to the project action
 */
const inviteToProjectRequest = () => {
  return {
    type: INVITE_PROJECT_POST_REQUEST
  };
};

const inviteToProjectSuccess = payload => {
  return {
    type: INVITE_PROJECT_POST_SUCCESS,
    payload
  };
};

const inviteToProjectFailure = error => {
  return {
    type: INVITE_PROJECT_POST_FAILURE,
    error
  };
};

const sendInvite = (opts = {}) => async dispatch => {
  dispatch(inviteToProjectRequest());
  try {
    const payload = await ApiService.sendInviteToProject(t.inviteMemberRequestTransformer(opts));
    if (!payload) return;
    dispatch(inviteToProjectSuccess(payload));
    opts.helper.toggleInviteModal();
  } catch (e) {
    dispatch(inviteToProjectFailure(e));
  } finally {
    opts.helper.onRequestFinished();
  }
};

/**
 * Remove from the project action
 */
const removeFromProjectRequest = () => {
  return {
    type: INVITE_PROJECT_DELETE_REQUEST
  };
};

const removeFromProjectSuccess = payload => {
  return {
    type: INVITE_PROJECT_DELETE_SUCCESS,
    payload
  };
};

const removeFromProjectFailure = error => {
  return {
    type: INVITE_PROJECT_DELETE_FAILURE,
    error
  };
};

const removeFromProject = (opts = {}) => async dispatch => {
  dispatch(removeFromProjectRequest());
  try {
    const payload = await ApiService.removeMemberFromProject(opts);
    if (!payload) return;
    dispatch(removeFromProjectSuccess(payload));
    Notification.call('warn', `User with ${opts.email} email, has been removed from this project.`);
    opts.helper.props.onGetProject(opts.id);
  } catch (e) {
    dispatch(removeFromProjectFailure(e));
  }
};

export {
  addProject,
  getProject,
  getProjects,
  getProjectsForIssuesList,
  addProjectUpdate,
  updateProject,
  deleteProject,
  sendInvite,
  removeFromProject,
  getProjectsList
};