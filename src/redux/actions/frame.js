import {
  PROJECT_FRAME_LOAD_SUCCESS,
  PROJECT_FRAME_LOAD_FAILURE,

  FETCH_FRAME_PROJECT_GET_REQUEST,
  FETCH_FRAME_PROJECT_GET_SUCCESS,
  FETCH_FRAME_PROJECT_GET_FAILURE,

  FETCH_FRAME_PROJECTS_GET_REQUEST,
  FETCH_FRAME_PROJECTS_GET_SUCCESS,
  FETCH_FRAME_PROJECTS_GET_FAILURE,
} from './types';

import {
  ApiService,
  transformers as t
} from './../../services';

/**
 * Frame actions
 */
const projectFrameLoadSuccess = () => {
  return {
    type: PROJECT_FRAME_LOAD_SUCCESS
  };
};

const projectFrameLoadFailure  = () => {
  return {
    type: PROJECT_FRAME_LOAD_FAILURE
  };
};

const getProjectFrameStatus = async dispatch => {
  try {
    dispatch(projectFrameLoadSuccess());
  } catch (e) {
    dispatch(projectFrameLoadFailure());
  }
};

const getProjectRequest = () => {
  return {
    type: FETCH_FRAME_PROJECT_GET_REQUEST
  };
};

const getProjectSuccess = payload => {
  return {
    type: FETCH_FRAME_PROJECT_GET_SUCCESS,
    payload
  };
};

const getProjectFailure = error => {
  return {
    type: FETCH_FRAME_PROJECT_GET_FAILURE,
    error
  };
};

const getProjectForFrame = (id, opts) => async dispatch => {
  dispatch(getProjectRequest());
  try {
    const payload = await ApiService.getProject(id);
    if (!payload) return;
    const data = await t.getProjectForFrameResponseTransformer(payload);
    dispatch(getProjectSuccess(data));
    opts.helper.urlToState();
  } catch (e) {
    dispatch(getProjectFailure(e));
  }
};

/**
 * ProjectsList get actions
 */
const getProjectsRequest = () => {
  return {
    type: FETCH_FRAME_PROJECTS_GET_REQUEST
  };
};

const getProjectsSuccess = payload => {
  return {
    type: FETCH_FRAME_PROJECTS_GET_SUCCESS,
    payload
  };
};

const getProjectsFailure = error => {
  return {
    type: FETCH_FRAME_PROJECTS_GET_FAILURE,
    error
  };
};

const getProjectsForFrame = async dispatch => {
  dispatch(getProjectsRequest());
  try {
    const payload = await ApiService.getProjects();
    if (!payload) return;
    dispatch(getProjectsSuccess(t.getProjectsForFrameResponseTransformer(payload)));
  } catch (e) {
    dispatch(getProjectsFailure(e));
  }
};

export {
  getProjectForFrame,
  getProjectsForFrame,
  getProjectFrameStatus
};