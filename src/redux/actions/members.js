import {
  FETCH_GET_MEMBERS_BY_PROJECT_ID_FAILURE,
  FETCH_GET_MEMBERS_BY_PROJECT_ID_REQUEST,
  FETCH_GET_MEMBERS_BY_PROJECT_ID_SUCCESS
} from './types';
import { ApiService, transformers as t } from './../../services';

/**
 * Get members by project id
 */
const getMembersByProjectIdRequest = () => {
  return {
    type: FETCH_GET_MEMBERS_BY_PROJECT_ID_REQUEST
  };
};

const getMembersByProjectIdRequestSuccess = payload => {
  return {
    type: FETCH_GET_MEMBERS_BY_PROJECT_ID_SUCCESS,
    payload
  };
};

const getMembersByProjectIdRequestFailure = error => {
  return {
    type: FETCH_GET_MEMBERS_BY_PROJECT_ID_FAILURE,
    error
  };
};

const getMembersByProjectId = id => async dispatch => {
  dispatch(getMembersByProjectIdRequest());
  try {
    const payload = await ApiService.getMembersByProjectId(id);
    if (!payload) return;
    dispatch(getMembersByProjectIdRequestSuccess(t.getMembersByProjectId(payload)));
  } catch (e) {
    dispatch(getMembersByProjectIdRequestFailure(e));
  }
};

export {
  getMembersByProjectId
};
