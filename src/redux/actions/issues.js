import {
  FETCH_ISSUES_STATUSES_GET_REQUEST,
  FETCH_ISSUES_STATUSES_GET_SUCCESS,
  FETCH_ISSUES_STATUSES_GET_FAILURE,

  FETCH_ISSUES_PRIORITIES_GET_REQUEST,
  FETCH_ISSUES_PRIORITIES_GET_SUCCESS,
  FETCH_ISSUES_PRIORITIES_GET_FAILURE,

  FETCH_ISSUES_ADD_REQUEST,
  FETCH_ISSUES_ADD_SUCCESS,
  FETCH_ISSUES_ADD_FAILURE,

  FETCH_ISSUES_GET_REQUEST,
  FETCH_ISSUES_GET_SUCCESS,
  FETCH_ISSUES_GET_FAILURE,

  FETCH_ISSUE_UPDATE_REQUEST,
  FETCH_ISSUE_UPDATE_SUCCESS,
  FETCH_ISSUE_UPDATE_FAILURE,

  FETCH_ISSUES_BY_PROJECT_ID_GET_REQUEST,
  FETCH_ISSUES_BY_PROJECT_ID_GET_SUCCESS,
  FETCH_ISSUES_BY_PROJECT_ID_GET_FAILURE,

  CLEAR_ISSUES_DATA
} from './types';

import {
  ApiService,
  transformers as t
} from './../../services';

/**
 * Issues get statuses
 */
const getIssuesStatusesRequest = () => {
  return {
    type: FETCH_ISSUES_STATUSES_GET_REQUEST
  };
};

const getIssuesStatusesSuccess = payload => {
  return {
    type: FETCH_ISSUES_STATUSES_GET_SUCCESS,
    payload
  };
};

const getIssuesStatusesFailure = error => {
  return {
    type: FETCH_ISSUES_STATUSES_GET_FAILURE,
    error
  };
};

const getIssuesStatusesProjects = async dispatch => {
  dispatch(getIssuesStatusesRequest());
  try {
    const payload = await ApiService.getStatuses();
    if (!payload) return;
    dispatch(getIssuesStatusesSuccess(t.getIssuesStatusesForFrameResponseTransformer(payload)));
  } catch (e) {
    dispatch(getIssuesStatusesFailure(e));
  }
};

/**
 * Issues get priorities
 */
const getIssuesPrioritiesRequest = () => {
  return {
    type: FETCH_ISSUES_PRIORITIES_GET_REQUEST
  };
};

const getIssuesPrioritiesSuccess = payload => {
  return {
    type: FETCH_ISSUES_PRIORITIES_GET_SUCCESS,
    payload
  };
};

const getIssuesPrioritiesFailure = error => {
  return {
    type: FETCH_ISSUES_PRIORITIES_GET_FAILURE,
    error
  };
};

const getIssuesPrioritiesProjects = async dispatch => {
  dispatch(getIssuesPrioritiesRequest());
  try {
    const payload = await ApiService.getPriorities();
    if (!payload) return;
    dispatch(getIssuesPrioritiesSuccess(t.getIssuesPrioritiesForFrameResponseTransformer(payload)));
  } catch (e) {
    dispatch(getIssuesPrioritiesFailure(e));
  }
};

/**
 * Update issue status
 */
const updateIssueRequest = () => {
  return {
    type: FETCH_ISSUE_UPDATE_REQUEST
  };
};

const updateIssueSuccess = payload => {
  return {
    type: FETCH_ISSUE_UPDATE_SUCCESS,
    payload
  };
};

const updateIssueFailure = error => {
  return {
    type: FETCH_ISSUE_UPDATE_FAILURE,
    error
  };
};

const updateIssue = (id, opts) => async dispatch => {
  dispatch(updateIssueRequest());
  try {
    const payload = await ApiService.updateIssue(id, t.updateIssueRequestTransformer(opts));
    if (!payload) return;
    dispatch(updateIssueSuccess(payload));
  } catch (e) {
    dispatch(updateIssueFailure(e));
  } finally {
    opts.helper.endBoardLoading();
  }
};


/**
 * Get issues
 */
const getIssuesRequest = () => {
  return {
    type: FETCH_ISSUES_GET_REQUEST
  };
};

const getIssuesRequestSuccess = payload => {
  return {
    type: FETCH_ISSUES_GET_SUCCESS,
    payload
  };
};

const getIssuesRequestFailure = error => {
  return {
    type: FETCH_ISSUES_GET_FAILURE,
    error
  };
};

const getIssues = async dispatch => {
  dispatch(getIssuesRequest());
  try {
    const payload = await ApiService.getIssuesByParams(); //TODO
    if (!payload) return;
    dispatch(getIssuesRequestSuccess(t.getIssueResponseTransformer(payload)));
  } catch (e) {
    dispatch(getIssuesRequestFailure(e));
  }
};

/**
 * Get issues
 */
const getIssuesByProjectIDRequest = () => {
  return {
    type: FETCH_ISSUES_BY_PROJECT_ID_GET_REQUEST
  };
};

const getIssuesByProjectIDRequestSuccess = payload => {
  return {
    type: FETCH_ISSUES_BY_PROJECT_ID_GET_SUCCESS,
    payload
  };
};

const getIssuesByProjectIDRequestFailure = error => {
  return {
    type: FETCH_ISSUES_BY_PROJECT_ID_GET_FAILURE,
    error
  };
};

const getIssuesByProjectID = (id, opts) => async dispatch => {
  dispatch(getIssuesByProjectIDRequest());
  try {
    const payload = await ApiService.getIssuesByProjectID(id);
    if (!payload) return;
    dispatch(getIssuesByProjectIDRequestSuccess(t.getIssueResponseTransformer(payload)));
  } catch (e) {
    dispatch(getIssuesByProjectIDRequestFailure(e));
  } finally {
    opts.helper.endLoading();
  }
};

/**
 * Issue add
 */
const addIssuesRequest = () => {
  return {
    type: FETCH_ISSUES_ADD_REQUEST
  };
};

const addIssuesRequestSuccess = payload => {
  return {
    type: FETCH_ISSUES_ADD_SUCCESS,
    payload
  };
};

const addIssuesRequestFailure = error => {
  return {
    type: FETCH_ISSUES_ADD_FAILURE,
    error
  };
};

const addIssues = (req, opts) => async dispatch => {
  dispatch(addIssuesRequest());
  try {
    const payload = await ApiService.addIssue(t.addIssueRequestTransformer(req));
    if (!payload) return;
    dispatch(addIssuesRequestSuccess(t.addIssueResponseTransformer(payload)));
  } catch (e) {
    dispatch(addIssuesRequestFailure(e));
  } finally {
    opts.helper.onRequestFinished();
  }
};

/**
 * Clear issues data
 */
const clearIssuesDataAction = () => {
  return {
    type: CLEAR_ISSUES_DATA
  };
};

const clearIssuesData = async dispatch => {
  dispatch(clearIssuesDataAction());
};

export {
  addIssues,
  updateIssue,
  getIssues,
  getIssuesByProjectID,
  clearIssuesData,
  getIssuesStatusesProjects,
  getIssuesPrioritiesProjects
};