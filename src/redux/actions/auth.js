import {
  FETCH_REGISTRATION_REQUEST,
  FETCH_REGISTRATION_SUCCESS,
  FETCH_REGISTRATION_ERROR,
  FETCH_LOGIN_REQUEST,
  FETCH_LOGIN_SUCCESS,
  FETCH_LOGIN_ERROR,
  FETCH_CHECK_LOGIN_REQUEST,
  FETCH_CHECK_LOGIN_SUCCESS,
  FETCH_CHECK_LOGIN_ERROR,
  LOGOUT_ACTION
} from './types';

import {
  ApiService,
  User
} from './../../services';

/**
 * Registration actions
 */
const registrationRequest = () => {
  return {
    type: FETCH_REGISTRATION_REQUEST
  };
};

const registrationSuccess = payload => {
  return {
    type: FETCH_REGISTRATION_SUCCESS,
    payload
  };
};

const registrationFailure = error => {
  return {
    type: FETCH_REGISTRATION_ERROR,
    error
  };
};

const registration = (res, opts = {}) => async dispatch => {
  dispatch(registrationRequest());
  try {
    const payload = await ApiService.registration(res);
    if (!payload) return;
    dispatch(registrationSuccess(payload));
    return User.setDataFromResponse(payload);
  } catch (e) {
    dispatch(registrationFailure(e));
  } finally {
    opts.helper.onRequestFinished();
  }
};

/**
 * Login actions
 */
const loginRequest = () => {
  return {
    type: FETCH_LOGIN_REQUEST
  };
};

const loginSuccess = payload => {
  return {
    type: FETCH_LOGIN_SUCCESS,
    payload
  };
};

const loginFailure = error => {
  return {
    type: FETCH_LOGIN_ERROR,
    error
  };
};

const login = (res, opts = {}) => async dispatch => {
  dispatch(loginRequest());
  try {
    const payload = await ApiService.login(res);
    if (!payload) throw payload;
    dispatch(loginSuccess(payload));
    return User.setDataFromResponse(payload);
  } catch (e) {
    dispatch(loginFailure(e));
  } finally {
    opts.helper.onRequestFinished();
  }
};

/**
 * Check login actions
 */
const checkLoginRequest = () => {
  return {
    type: FETCH_CHECK_LOGIN_REQUEST
  };
};

const checkLoginSuccess = payload => {
  return {
    type: FETCH_CHECK_LOGIN_SUCCESS,
    payload
  };
};

const checkLoginFailure = error => {
  return {
    type: FETCH_CHECK_LOGIN_ERROR,
    error
  };
};

const checkLogin = async dispatch => {
  dispatch(checkLoginRequest());
  try {
    const payload = await ApiService.checkLogin();
    if (!payload || payload === 401 || payload === 500) throw payload;
    dispatch(checkLoginSuccess(payload));
  } catch (e) {
    dispatch(checkLoginFailure(e));
    if (e === 401) {
      User.removeUserData();
    }
  }
};

/**
 * Logout actions
 */
const logoutAction = () => {
  return {
    type: LOGOUT_ACTION,
    user: false
  };
};

const logout = dispatch => {
  dispatch(logoutAction());
  localStorage.clear();
  User.removeUserData();
};

export { registration, login, logout, checkLogin };