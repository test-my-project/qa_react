import {
  FETCH_NAVIGATION_PROJECTS_GET_REQUEST,
  FETCH_NAVIGATION_PROJECTS_GET_SUCCESS,
  FETCH_NAVIGATION_PROJECTS_GET_FAILURE
} from './types';

import {
  ApiService,
  transformers as t
} from './../../services';

/**
 * ProjectsList get actions
 */
const getNavigationProjectsRequest = () => {
  return {
    type: FETCH_NAVIGATION_PROJECTS_GET_REQUEST
  };
};

const getNavigationProjectsSuccess = payload => {
  return {
    type: FETCH_NAVIGATION_PROJECTS_GET_SUCCESS,
    payload
  };
};

const getNavigationProjectsFailure = error => {
  return {
    type: FETCH_NAVIGATION_PROJECTS_GET_FAILURE,
    error
  };
};

const getNavigationProjects = async dispatch => {
  dispatch(getNavigationProjectsRequest());
  try {
    const payload = await ApiService.getProjects();
    if (!payload) return;
    dispatch(getNavigationProjectsSuccess(t.getNavigationProjectsResponseTransformer(payload)));
  } catch (e) {
    dispatch(getNavigationProjectsFailure(e));
  }
};

export { getNavigationProjects };