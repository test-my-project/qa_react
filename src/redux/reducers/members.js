import {
  INITIAL_STATE,

  FETCH_GET_MEMBERS_BY_PROJECT_ID_FAILURE,
  FETCH_GET_MEMBERS_BY_PROJECT_ID_REQUEST,
  FETCH_GET_MEMBERS_BY_PROJECT_ID_SUCCESS
} from './../actions/types';

export default function membersReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case FETCH_GET_MEMBERS_BY_PROJECT_ID_SUCCESS:
      return { ...state, members: action.payload };
    case FETCH_GET_MEMBERS_BY_PROJECT_ID_REQUEST:
    case FETCH_GET_MEMBERS_BY_PROJECT_ID_FAILURE:
    default:
      return state;
  }
}