import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import authReducer from './auth';
import projectsReducer from './projects';
import navigationReducer from './navigation';
import issuesReducer from './issues';
import frameReducer from './frame';
import membersReducer from './members';

export default combineReducers({
  routing: routerReducer,
  navigationReducer,
  projectsReducer,
  frameReducer,
  membersReducer,
  issuesReducer,
  authReducer
});