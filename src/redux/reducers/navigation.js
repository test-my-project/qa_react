import {
  INITIAL_STATE,
  FETCH_NAVIGATION_PROJECTS_GET_REQUEST,
  FETCH_NAVIGATION_PROJECTS_GET_SUCCESS,
  FETCH_NAVIGATION_PROJECTS_GET_FAILURE
} from './../actions/types';

export default function navigationReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case FETCH_NAVIGATION_PROJECTS_GET_SUCCESS:
      return { ...state, projects: action.payload };
    case FETCH_NAVIGATION_PROJECTS_GET_REQUEST:
    case FETCH_NAVIGATION_PROJECTS_GET_FAILURE:
    default:
      return state;
  }
}