import {
  INITIAL_STATE,
  FETCH_LOGIN_REQUEST,
  FETCH_LOGIN_SUCCESS,
  FETCH_LOGIN_ERROR,
  FETCH_CHECK_LOGIN_REQUEST,
  FETCH_CHECK_LOGIN_SUCCESS,
  FETCH_CHECK_LOGIN_ERROR,
  FETCH_REGISTRATION_REQUEST,
  FETCH_REGISTRATION_SUCCESS,
  FETCH_REGISTRATION_ERROR,
  LOGOUT_ACTION
} from './../actions/types';

export default function authReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case LOGOUT_ACTION:
      return { ...state, userData: action.user };
    case FETCH_LOGIN_SUCCESS:
    case FETCH_CHECK_LOGIN_SUCCESS:
    case FETCH_REGISTRATION_SUCCESS:
      return { ...state, userData: action.payload };
    case FETCH_LOGIN_REQUEST:
    case FETCH_CHECK_LOGIN_REQUEST:
    case FETCH_REGISTRATION_REQUEST:
    case FETCH_REGISTRATION_ERROR:
    case FETCH_CHECK_LOGIN_ERROR:
    case FETCH_LOGIN_ERROR:
    default:
      return state;
  }
}