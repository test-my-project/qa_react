import {
  INITIAL_STATE,

  PROJECT_FRAME_LOAD_SUCCESS,
  PROJECT_FRAME_LOAD_FAILURE,

  FETCH_FRAME_PROJECT_GET_REQUEST,
  FETCH_FRAME_PROJECT_GET_SUCCESS,
  FETCH_FRAME_PROJECT_GET_FAILURE,

  FETCH_FRAME_PROJECTS_GET_REQUEST,
  FETCH_FRAME_PROJECTS_GET_SUCCESS,
  FETCH_FRAME_PROJECTS_GET_FAILURE,
} from './../actions/types';

export default function frameReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case FETCH_FRAME_PROJECT_GET_SUCCESS:
      return { ...state, project: action.payload };
    case FETCH_FRAME_PROJECTS_GET_SUCCESS:
      return { ...state, projects: action.payload };
    case PROJECT_FRAME_LOAD_SUCCESS:
    case PROJECT_FRAME_LOAD_FAILURE:
    case FETCH_FRAME_PROJECT_GET_REQUEST:
    case FETCH_FRAME_PROJECT_GET_FAILURE:
    case FETCH_FRAME_PROJECTS_GET_REQUEST:
    case FETCH_FRAME_PROJECTS_GET_FAILURE:
    default:
      return state;
  }
}