## Project setup 

This project are based on [**NodeJS**](https://nodejs.org/). If you don`t have installed **NodeJS**, please do it first.

Dependencies are handled by [**npm**](https://www.npmjs.com/) and [**yarn**](https://yarnpkg.com/en/). 
(Better to use **yarn**)

## Directories
```
QA-Services Structure
│
├── public/                 (static files)
│   ├── assets/             (assets)
│   ├── favicon.ico  
│   └── index.html          (html temlpate)
│
├── docs/                   (project documents)
│
Client part (Front-End)
│
├── src/                    (client root)
│   ├── assets/             (assets source)
│   ├── components/         (components source)
│   ├── config/             (client configuration source)
│   ├── containers/         (containers source)
│   ├── helpers/            (different project helpers source)
│   ├── redux/              (redux (actions/reducers) source)
│   ├── routes/             (routes source)
│   ├── scss/               (scss/css source)
│   ├── services/           (client services source)
│   ├── views/              (views source)
│   ├── App.js
│   ├── App.scss
│   ├── index.css
│   ├── Projects.js
│   ├── polyfill.js
│   └── serviceWorker.js 
│
Server part (Back-End)
├── config/                 (server config)
├── server/                 (server root)
│
└── package.json
```

## Usage
Clone this repo.
`npm i` or `yarn` - to install dependencies

**important!** Do not forget include **public ssh-key** from your local machine in your gitlab profile.

## Sctipts 
`yarn start-serv` to run a NodeJS server. 

For both of the next commands you need to run the NodeJs server. Without this server you won`t be able to use API and work in a proper way.

If you are working on the issue without iframe usage you can use the next command to start the local client.

`yarn start-with-proxy` for developing (it runs webpack-dev-server)  
It runs on [http://localhost:3003/](http://localhost:3003/)

If you are working on issue with iframe you should use next command to start the local client.

`yarn start-with-build` to run a watcher and local build tool.
It runs on [http://localhost:3030/](http://localhost:3030/)
